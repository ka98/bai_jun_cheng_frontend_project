const path = require('path');
const webpack = require("webpack");
const uglify = require('uglifyjs-webpack-plugin');
const htmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
module.exports = {
    devtool: "eval-source-map",
    entry: {
        app: path.resolve(__dirname, 'src', 'main.js'), // 入口文件路径
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        // publicPath: '/',
        filename: './js/[name]-[chunkhash].js',

    },
    mode: "development",
    devServer: {
        contentBase: path.join(__dirname, "build"),
        // contentBase: './src',
        inline: true,
        port: 3331,
        proxy: {
            // '/snail/*': {
            //                 target: 'http://10.251.251.42:81',
            //                 host: '10.251.251.42:81',
            //                 secure: false
            //             }
            // '/snail/*': {
            //     target: 'http://10.251.251.79:8080',
            //     host: '10.251.251.79:8080',
            //     secure: false
            // }
            '/snail/*': {
                target: 'http://10.251.251.66:8080',
                host: '10.251.251.66:8080',
                secure: false
            }
        },
        // hot: true
    },
    module: {
        rules: [
            {
                //ES6、JSX处理
                test: /(\.jsx|\.js)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        limit: 8192,
                        name: 'img/[name].[hash:4].[ext]'
                    }
                }
            },
            {
                test: require.resolve('jquery'),
                use: [{
                    loader: 'expose-loader',
                    options: 'jQuery'
                },
                    {
                        loader: 'expose-loader',
                        options: '$'
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    // "style-loader",
                    "css-loader",
                    "less-loader"
                ],
            },
            {
                test: /\.css$/,
                // 使用 'style-loader','css-loader'
                use: ['style-loader', 'css-loader']
            },
        ]
    },
    resolve: {
        alias: {
            util: path.resolve(__dirname, 'src/util'),
            test: path.resolve(__dirname, 'src/test')
        }
    },
    plugins: [
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         NODE_ENV: JSON.stringify('production')
        //     }
        // }),
        new uglify({
            extractComments: true
        }),
        new htmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: 'body',
            title: '佰钧成人资管理系统',
            favicon: './img/bill-jc.ico'
        }),
        // new MiniCssExtractPlugin({
        //     filename: "[name].[chunkhash:8].css",
        //     chunkFilename: "[id].css"
        // })
        // new webpack.HotModuleReplacementPlugin()
    ]
}
// module.exports.plugins = [

// new webpack.optimize.UglifyJsPlugin({
//     compress: {
//         warnings: false
//     }
// }),
// new webpack.optimize.OccurenceOrderPlugin()
// ]
