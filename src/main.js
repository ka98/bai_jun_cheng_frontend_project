import React from 'react';
import ReactDOM from 'react-dom';
import GlobalBox from "./global/globalBox";
// 引入主体样式文件
import './main.css';

class App extends React.Component {
    render(){
        return(
          <GlobalBox/>
        );
    }
}
ReactDOM.render(<App/>, document.getElementById('app'));
