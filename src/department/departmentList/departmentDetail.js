import React, { Component } from 'react'
import { Link } from 'react-router-dom'
// import { withRouter } from 'react-router-dom'
import { Table, Input, Row, Col, AutoComplete, LocaleProvider } from 'antd'
import zh_CN from 'antd/lib/locale-provider/zh_CN';
const columns = [
    {
        title: '序号',
        key: 'order',
        render: (text, record, index) => {
            return index + 1
        }
    },
    {
        title: '姓名',
        dataIndex: 'name',
        key: 'name',
        render: (text, record, index) => {
            return <a href={`${window.location.href.split('#')[0]}#/person/${record.empId}/false`} target="_blank">{text}</a>
            // return <span onClick={() => {this.props.history.push(`/person/${record.empId}/false`)}}>{text}</span>
        }
    },{
        title: '工号',
        dataIndex: 'workNum',
        key: 'workNum'
    },{
        title: '电话',
        dataIndex: 'phone',
        key: 'phone'
    }]
class DepartmentDetail extends Component{
    constructor(props) {
        super(props)
        this.handleSearch = this.handleSearch.bind(this)
        this.onSelect = this.onSelect.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.state = {
            dataSource: [],
            data: []
        }
        console.log(this.props)
    }
    handleSearch(value) {
        const { data } = this.props
        const dataSource = value ? data.filter((v) => {
            return v.name.startsWith(value)
        }).map((v) => {
            return v.name
        }) : []
        this.setState({dataSource})
        // console.log(value, data, dataSource, this.props)
    }
    onSelect(value) {
        const data = this.state.data.filter((item) => {
            return value === item.name
        })
        this.setState({data})
    }
    handleChange(value) {
        const data = this.props.data.filter((v) => {
            return v.name.startsWith(value)
        })
        this.setState({
            data
        })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            data: nextProps.data
        })
    }
    render() {
        const { dataSource } = this.state
        return (
            <div className="departmentDetail">
                <Row type="flex" justify="end">
                    <Col span={12} style={{textAlign: 'right', paddingBottom: 6}}>
                        <AutoComplete
                            dataSource={dataSource}
                            style={{ width: 200 }}
                            onSelect={this.onSelect}
                            onSearch={this.handleSearch}
                            onChange={this.handleChange}
                            placeholder="按姓名搜索"
                        />
                    </Col>
                </Row>
                <LocaleProvider locale={zh_CN}>
                    <Table columns={columns} dataSource={this.state.data} pagination={{showSizeChanger: true, showTotal:total => `共 ${total} 条` }}/>
                </LocaleProvider>
            </div>
        )
    }
}
export default DepartmentDetail
