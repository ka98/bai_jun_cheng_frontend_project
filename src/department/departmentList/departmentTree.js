import React, { Component } from 'react'
import { Tree, Input } from 'antd';

const TreeNode = Tree.TreeNode;
const Search = Input.Search;
const getParentKey = (id, tree) => {
    let parentKey;
    for (let i = 0; i < tree.length; i++) {
        const node = tree[i];
        if (node.children) {
            if (node.children.some(item => item.id === id)) {
                parentKey = node.id;
            } else if (getParentKey(id, node.children)) {
                parentKey = getParentKey(id, node.children);
            }
        }
    }
    return parentKey;
};
const getFlatData = (data, arr) => {
    data.forEach((v) => {
        if (v.children) {
            getFlatData(v.children, arr)
        } else {
            arr.push(v)
        }
    })
}
class departmentTree extends React.Component {
    state = {
        expandedKeys: [],
        searchValue: '',
        autoExpandParent: true,
        data: [],
        flatData: []
    }

    onExpand = (expandedKeys) => {
        this.setState({
            expandedKeys,
            autoExpandParent: false,
        });
    }

    onChange = (e) => {
        const value = e.target.value;
        if (!value) {
            this.setState({
                autoExpandParent: false,
                expandedKeys: []
            })
            return
        }
        let parentArr = []
        this.state.flatData.filter((v) => {
            return v.name.indexOf(value) + 1
        }).forEach((v) => {
            parentArr.push(...v.parentsList)
        })
        const expandedKeys = [...new Set(parentArr)].filter((v) => {
            return v!=="25393462"
        })
        console.log(expandedKeys)
        this.setState({
            expandedKeys,
            searchValue: value,
            autoExpandParent: true,
        });
    }
    handleSelect = (selectedKeys, e) => {
        console.log(selectedKeys, e, e.node.getNodeChildren())
        if (e.node.getNodeChildren().length === 0){
            this.props.handleSearch( +selectedKeys[0] )
        }
        // this.props.handleSearch
    }
    componentDidMount() {
        let flatData = []
        fetch('/snail/department/getTreeNode', {
            method: "get",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(
            res => res.json()
        ).then((data) => {
            console.log(data)
            this.setState({ data: data.children }, () => {
                getFlatData(this.state.data, flatData)
                console.log(flatData)
                this.setState({ flatData })
            })
        })
    }
    render() {
        const { searchValue, expandedKeys, autoExpandParent } = this.state;
        const loop = data => data.map((item) => {
            const index = item.name.indexOf(searchValue);
            const beforeStr = item.name.substr(0, index);
            const afterStr = item.name.substr(index + searchValue.length);
            const title = index > -1 ? (
                <span>
          {beforeStr}
                    <span style={{ color: '#f50' }}>{searchValue}</span>
                    {afterStr}
        </span>
            ) : <span>{item.name}</span>;
            if (item.children) {
                return (
                    <TreeNode key={item.id} title={title}>
                        {loop(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode key={item.id} title={title} />;
        });
        return (
            <div className="departmentTree">
                <Search style={{ marginBottom: 8 }} placeholder="Search" onChange={this.onChange} />
                <Tree
                    onExpand={this.onExpand}
                    expandedKeys={expandedKeys}
                    autoExpandParent={autoExpandParent}
                    onSelect={this.handleSelect}
                >
                    {loop(this.state.data)}
                </Tree>
            </div>
        );
    }
}
export default departmentTree