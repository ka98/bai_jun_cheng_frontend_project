import React, { Component } from 'react'
import DepartMentTree from './departmentList/departmentTree'
import DepartMentDetail from './departmentList/departmentDetail'
import './css/departmentList.css'
class DepartmentList extends Component{
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            id: ''
        }
        this.handleSearch = this.handleSearch.bind(this)
    }
    handleSearch(deptId) {
        fetch('/snail/employees/findEmpByEmpCondition', {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({deptId})
        }).then(
            res => res.json()
        ).then((data) => {
            console.log(data.data.pageInfo.list)
            this.setState({ data: data.data.pageInfo.list })
        })
    }
    render() {
        return (
            <div className="departmentList">
                <DepartMentTree handleSearch = {this.handleSearch}/>
                <DepartMentDetail data={this.state.data}/>
            </div>
        )
    }
}
export default DepartmentList