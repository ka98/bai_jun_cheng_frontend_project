import React from 'react';
import {message,Upload,Button,Icon,Table,Popconfirm,Input} from "antd";
import HTTPUtil from "../../util/HTTPUtil";
import "./po.css";
import jQuery from "jquery";
import Param from "util/transParam";
const Search = Input.Search;
const columns = [
    {
        title: '序号',
        render: (text, record, index) => {
            return <span>{index + 1}</span>
        }
    },{
        title: '客户',
        dataIndex: 'customer',
    },{
        title:'部门',
        dataIndex:'department'
    },{
        title:'客户主体',
        dataIndex:'customerSubject'
    },{
        title:'框架协议编号',
        dataIndex:'frameworkProtocolNo'
    },{
        title:'PR',
        dataIndex:'prNo'
    },{
        title:'项目简称',
        dataIndex:'projectAbbreviation'
    },{
        title:'PO项目名称',
        dataIndex:'poProjectName'
    },{
        title:'结算',
        dataIndex:'settlement'
    },{
        title:'区域&报价',
        dataIndex:'regionalQuotation'
    },{
        title:'PO号',
        dataIndex:'poNo'
    },{
        title:'PO金额',
        dataIndex:'poAmount'
    },{
        title:'项目开始时间',
        dataIndex:'projectStartTime'
    },{
        title:'项目结束时间',
        dataIndex:'crojectClosingTime'
    },{
        title:'团队规模',
        dataIndex:'teamSize'
    },{
        title:'团队结构',
        dataIndex:'teamStructure'
    }
    // ,{
    //     title:'服务地点',
    //     dataIndex:'serviceLocation'
    // },{
    //     title:'文档情况',
    //     dataIndex:'documentStatus'
    // },{
    //     title:'电子PO下发时间',
    //     dataIndex:'electronicDocumentTime'
    // },{
    //     title:'采购下发时间',
    //     dataIndex:'purchaseDeliveryTime'
    // },{
    //     title:'所属BU',
    //     dataIndex:'belongsBU'
    // },{
    //     title:'采购',
    //     dataIndex:'procurement'
    // },{
    //     title:'项目经理',
    //     dataIndex:'projectManager'
    // },{
    //     title:'PO状态',
    //     dataIndex:'poStatus'
    // }
];
export default class Po extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],   //显示table的数据
            loading:false,    //是否正在加载中
            onUpload:false,    //是否执行获取table数据的方法
            pagination:[],     //分页
            poId:[],    //勾选选中的数据
            selectedRowKeys:[],   //选框选中的
        };
        this.handleChange=this.handleChange.bind(this);
        this.poExhibition=this.poExhibition.bind(this);
        this.deleteEmp=this.deleteEmp.bind(this);
        this.onHandleSearch=this.onHandleSearch.bind(this);
    }
    componentDidMount(){
        this.poExhibition();
    }
    //获取table的数据
    poExhibition () {
        let me = this;
        let param = {};
        param.pagination=this.state.pagination;
        this.setState({
            loading:true
        });
        fetch("/snail/po/poExhibition",{
            headers: {
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(param)
        }).then((response) => {
            if(response.status !== 200) {
                message.error("获取数据失败！");
                return;
            }
            //检查响应文本
            response.json().then((data) => {
                me.setState({
                    dataSource : data.data.pageInfo.list
                });
                me.setState({loading:false});
            })
        }).catch((e) => {
            message.error("获取数据失败！");
            console.log("e:",e)
        });
    }
    //触发onChange事件
    handleChange = ( info , fileList ) => {
        this.setState({
            loading:true
        });
        fileList:[{ fileList:false }];
        console.log( fileList );
        if (info.file.status === 'done') {
            message.success("上传成功！");
            this.setState({
                loading:false,
            });
            this.poExhibition();
        } else if (info.file.status === "error") {
            message.error("上传失败！");
        }
    };
    //删除
    deleteEmp() {
        console.log("11:",this.state.poId);
        console.log("new:",this.state.selectedRowKeys);
        let param = {};
        param.poId=this.state.poId;
        const aa=this.state.poId;
        console.log(param);
        console.log(aa);
        this.fetchDeletePoById("/snail/po/deletePoById",param);
    }
    fetchDeletePoById (url,param) {
        fetch(url,{
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(param)
        }).then((response) => {
            return response.json()
        }).then((data) => {
            const me=this;
            console.log("data:",data);
            this.poExhibition();
            me.setState({
                value:data,
                // poId:[],
                selectedRowKeys:[]
            })
        }).catch((error) => {
            message.error("删除失败！");
            console.log("error:",error)
        })
    }
    //查询
    onHandleSearch (state) {
        const param = {};
        param.conditionName = state;
        console.log(param);
        this.setState({loading:true});
        fetch("/snail/po/poExhibition",{
            headers: {
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(param)
        }).then((response) => {
            if(response.status !== 200) {
                message.error("获取数据失败！");
                return;
            }
            //检查响应文本
            response.json().then((data) => {
                console.log(data);
                this.setState({
                    dataSource : data.data.pageInfo.list
                });
                this.setState({loading:false});
            })
        }).catch((e) => {
            message.error("获取数据失败！");
            console.log("e:",e)
        });
    }

    //选中框
    onSelectChange = (selectedRowKeys,selectedRows) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys,selectedRows);
        this.setState({ selectedRowKeys });
        let poId = [];
        for(var i=0;i<selectedRows.length;i++){
            poId.push(selectedRows[i]["id"]);
        }
        console.log("poId:",poId);
        this.setState({poId})
    };
    render(){
        //上传
        const props = {
            name: 'file',
            action: '/snail/po/poExcelImport',
            headers: {
                authorization: 'authorization-text',
                "Access-Control-Allow-Origin":"*"
            },
            onChange:this.handleChange,
            // multiple: true
        };
        //勾选框
        // const { selectedRowKeys } = this.state;
        // const rowSelection = {
        //     // selectedRowKeys,
        //     onChange:(selectedRowKeys, selectedRows) => {
        //         console.log("selectedRowKeys:",selectedRowKeys);
        //         console.log("selectedRows:",selectedRows);
        //         let poId = [];
        //         for(var i=0;i<selectedRows.length;i++){
        //             poId.push(selectedRows[i]["id"]);
        //         }
        //         console.log("poId:",poId);
        //         this.setState({
        //             poId
        //         })
        //     },
        //     //用户手动选择/取消选择某列的回调
        //     onSelect:(record,selected,selectedRows) => {
        //         console.log("rss:",record,selected,selectedRows);
        //     },
        //     //用户手动选择/取消选择所有列的回调
        //     onSelectAll: (selected, selectedRows, changeRows) => {
        //         console.log("ssc:",selected, selectedRows, changeRows);
        //     },
        // };

        const {selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        return(
            <div>
                <Upload {...props} className="upl">
                    <Button type="primary"><Icon type="upload"/>导入</Button>
                </Upload>
                <Popconfirm title="确定要删除吗？" onConfirm={this.deleteEmp}>
                    <Button type="danger">删除</Button>
                </Popconfirm>
                <Search
                    placeholder="可输入客户/部门/PO号搜索"
                    onSearch={this.onHandleSearch}
                    className="search"
                />
                <Table
                    dataSource={this.state.dataSource}
                    columns={columns}
                    loading={this.state.loading}
                    rowSelection={rowSelection}
                    pagination={this.state.pagination}
                />
            </div>
        )
    }
}
