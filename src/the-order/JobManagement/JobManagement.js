import React from "react";
import {Form,Select,Input,message,Button} from "antd";
import "./JobManagement.css";
import Jquery from "jquery"
const Option = Select.Option;
const FormItem=Form.Item;
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 4 },
};
class JobManagement extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isDisabled:true,
            jobName:[],     //岗位
            hierarchy:[],    //层级
            offer:[],     //报价
        };
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleChangeJob=this.handleChangeJob.bind(this);
        this.handleSearchJob=this.handleSearchJob.bind(this);
        this.handleChangeHierarchy=this.handleChangeHierarchy.bind(this);
        this.handleSearchHierarchy=this.handleSearchHierarchy.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        let obj = this.props.form.getFieldsValue();
        console.log("obj:",obj);
        fetch('/snail/.....', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        }).then(response=>{
            return response.json()
        }).then(function (data) {
            if (data.code == 200) {
                message.info(data.data);
            }
        }).catch(error=>{
            console.log(error);
            message.error("提交失败！");
        })
    }
    handleChangeJob(value) {
        var er=this;
        console.log(`selected ${value}`);
        if (value!==null){
            er.setState({
                isDisabled:false,
                value
            })
        }
    }
    handleSearchJob(){
        let param=[];
        param.jobName=this.state.jobName;
        this.fetchJob("/snail/jobs/findAllJob",Jquery.param(param));
        console.log("param:",param);
    }
    fetchJob=(url,param)=>{
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:JSON.stringify(this.state.jobName)
        }).then((response)=>{
            console.log("response:",response);
            return response.json()
        }).then((data)=>{
            console.log("data:",data);
            this.state({
                value:data,
            })
        }).catch(error=>{
            message.error("获取数据失败！");
            console.log("error:",error)
        })
    };
    handleChangeHierarchy(a){
        console.log(a)
    }
    handleSearchHierarchy(aa){
        console.log(aa)
    }
    render(){
        const { getFieldDecorator } = this.props.form;
        let isDisabled=this.state.isDisabled;
        const options=this.state.jobName.map(d=><Option key={d.jobName}>{d.text}</Option>);
        const children=this.state.hierarchy.map(a=><Option key={a.hierarchy}>{a.text}</Option>);
        return(
            <Form onSubmit={this.handleSubmit}>
                <FormItem label="岗位" {...formItemLayout}>
                    {getFieldDecorator("jobName")(
                        <Select
                            // className="se"
                            showSearch
                            placeholder="请输入.."
                            showArrow={false}
                            filterOption={false}
                            onChange={this.handleChangeJob}      //选中 option，或 input 的 value 变化（combobox 模式下）时，调用此函数
                            onSearch={this.handleSearchJob}      //文本框值变化时回调
                            notFoundContent={null}
                        >
                            {options}
                        </Select>
                    )}
                </FormItem>
                <FormItem label="层级" {...formItemLayout}>
                    {getFieldDecorator("hierarchy")(
                        <Select
                            // className="se"
                            showSearch
                            placeholder="请输入.."
                            onChange={this.handleChangeHierarchy}
                            onSearch={this.handleSearchHierarchy}
                        >
                            {children}
                        </Select>
                    )}
                </FormItem>
                <FormItem label="报价" {...formItemLayout}>
                    {getFieldDecorator("offer")(
                        <Input className="se" value={this.state.offer}
                               // disabled={isDisabled}
                        />
                    )}
                </FormItem>
                <FormItem
                    wrapperCol={{span:14, offset: 6}}
                >
                    <Button type="primary" htmlType="submit">提交</Button>
                </FormItem>
            </Form>
        )
    }
}
export default Form.create()(JobManagement)
