import React, {Component} from "react";
import { Form, Input, InputNumber } from "antd";
const FormItem = Form.Item;
const EditableContext = React.createContext();
class EditableCell extends Component {
    constructor(props){
        super(props)
    }
    componentDidMount(){
        console.log(this.props);
    }
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />;
        }
        return <Input />;
    }
    render(){
        const { editing, dataIndex, title, inputType, record, index, ...restProps } = this.props;
        console.log(this.props);
        const { getFieldDecorator } = this.props.form;
        return(
            <EditableContext.Consumer>
                {(form) => {
                    // const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [{
                                            required: true,
                                            message: `Please Input ${title}!`,
                                        }],
                                        initialValue: record[dataIndex],
                                    })(this.getInput())}
                                </FormItem>
                            ) : restProps.children}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        )
    }
}
export default Form.create()(EditableCell)