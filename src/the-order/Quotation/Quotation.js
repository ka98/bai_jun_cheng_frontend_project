import React, {Component} from "react";
import { Table, Input, Popconfirm, LocaleProvider, Form } from "antd";
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import EditableCell from './EditableCell/EditableCell'
const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);
const dataSource = [
    {
    key: '1',
    name: '林俊杰',    //姓名
    years: 2018,     //年限
    level: '高级',   //等级
    position: 'java',    //职位
    startTime: '2017.1.1',   //开始时间
    endTime: '2018.2.2'     //结束时间
}, {
    key: '2',
    name: '田馥甄',
    years: 2018,
    level: '中级',
    position: '前端',
    startTime: '2017.2.2',
    endTime: '2018.3.3'
}, {
    key: '3',
    name: '林俊杰11',
    years: 2018,
    level: '中级',
    position: 'java',
    startTime: '2017.3.3',
    endTime: '2018.4.4'
}, {
    key: '4',
    name: '林俊杰222',
    years: 2018,
    level: '低级',
    position: 'java',
    startTime: '2017.7.1',
    endTime: '2018.6.6'
}, {
    key: '5',
    name: '林俊杰333',
    years: 2018,
    level: '技术专家',
    position: '前端',
    startTime: '2017.8.8',
    endTime: '2018.3.15'
}];
class Quotation extends Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],
            key:''
        };
        this.save = this.save.bind(this);
        this.cancel = this.cancel.bind(this);
    }
    isEditing = (record) => {
        console.log(record);
        return record.key === this.state.key;
    };
    save = (form,key) => {
        console.log(form,key);
    };
    edit (key) {
        console.log(key)
        this.setState({ key: key });
    }
    cancel = () => {
        this.setState({ key: '' });
    };
    render(){
        const components = {
            body:{
                row: EditableFormRow,
                cell: EditableCell,
            }
        }
        const columns = [
            {
                title: '序号',
                dataIndex: 'number',
                key: 'number',
                // width: 100,
                // align:"center",
                render:(text, record, index)=> {
                    return index+1;
                }
            },{
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
                editable: true
            },{
                title: '年限',
                dataIndex: 'years',
                key: 'years',
                editable: true,
            },{
                title: '等级',
                dataIndex: 'level',
                key: 'level',
                editable: true,
            },{
                title: '职位',
                dataIndex: 'position',
                key: 'position',
                editable: true,
            },{
                title: '开始时间',
                dataIndex: 'startTime',
                key: 'startTime',
                editable: true,
            },{
                title: '结束时间',
                dataIndex: 'endTime',
                key: 'endTime',
                editable: true,
            },{
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                    const editable = this.isEditing(record);
                    return (
                        <div>
                            {editable ? (
                                <span>
                                    <EditableContext.Consumer>
                                        {form => (
                                            <a
                                                href="javascript:;"
                                                onClick={() => this.save(form, record.key)}
                                                style={{ marginRight: 8 }}
                                            >
                                                确定
                                            </a>
                                        )}
                                    </EditableContext.Consumer>
                                    <LocaleProvider locale={zh_CN}>
                                        <Popconfirm
                                            title="确定取消吗？"
                                            onConfirm={() => this.cancel(record.key)}
                                        >
                                            <a>取消</a>
                                        </Popconfirm>
                                    </LocaleProvider>
                                </span>
                            ) : (
                                <a onClick={() => this.edit(record.key)}>编辑</a>
                            )}
                        </div>
                    )
                }
            }
        ];
        return(
            <Table
                columns={columns}
                dataSource={dataSource}
                rowClassName="editable-row"
                components={components}
                onRow={(record)=>{console.log(record)}}
            />
        )
    }
}
export default Quotation