import React from "react";
import {Button,message} from "antd";

export default class TimerManagement extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            iconLoading:false,   //按钮是否处于旋转状态
            // uLoading:false,       //同步按钮是否正在执行中
        };
        this.handleTimeChange=this.handleTimeChange.bind(this);
        this.handleSyncGroup=this.handleSyncGroup.bind(this);
    }
    //定时器发送
    handleTimeChange = () => {
        this.fetchFn('/snail/quartz/dingGroupQuartz');
        this.setState({
            iconLoading: true
        });
    };
    fetchFn = (url,groudId) => {
        fetch(url,{
            method: 'POST',
            headers: {
                "Content-Type":"application/x-www-form-urlencoded",
                'Accept': 'application/json',
            },
            body:"groupId="
        }).then(response => {
            return response.json()
        }).then(function(json){
            console.log(json);
            this.setState({
                iconLoading: false
            });
            if(json.code == 200) {
                message.success("发送成功！");
            }
        }).catch((e) => {
            message.error("发送失败！");
            this.setState({
                iconLoading: false
            });
            console.log("e:",e);
        })
    };
    //同步群
    handleSyncGroup = () => {
        this.fetchFn("/snail/quartz/test")
    };

    render(){
        return(
            <div>
                <Button type="primary" icon="poweroff" className="baocun" loading={this.state.iconLoading} onClick={this.handleTimeChange}>定时器发送</Button>
                <Button type="primary" loading={this.state.iconLoading} onClick={this.handleSyncGroup}>同步群</Button>
            </div>
        )
    }
};
