import React from 'react'
import { Card } from 'antd';

// 引入标准Fetch及IE兼容依赖
import 'whatwg-fetch'
//ceshi
export default class MyCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lists: []
        }
    }
    render() {
        return (
            <Card title="资源导航" style={{ width: "800px", margin: "0 auto" }} className="animated zoomIn">,
                {
                    this.state.lists.map((e) => {
                        return (
                            <p className="doclist" key={e.title}><a href={ e.url } target="_blank">hello world</a></p>
                        )
                    })
                }
            </Card>
        )
    }
}



