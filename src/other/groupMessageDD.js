import React from 'react';
import {Button, Card, Form, Input, Select, Modal, Checkbox, message,Icon } from 'antd';
import jQuery from "jquery";
import GroupMessageFileUpload from "./fileUpload/groupFileUpload";

const CheckboxGroup = Checkbox.Group;
const Option = Select.Option;
const FormItem = Form.Item;

const sendTitleText = "主题发送";
const sendTitle = 1;
const sendLinkText = "链接发送";
const sendLink = 2;
const sendTypeIdx={
    "主题发送":sendTitle,
    "链接发送":sendLink
};

// 结尾组件
class GroupMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalVisible: false,
            showSendLink: false,
            GroupCheckedList: [],
            GroupAllList: [],             //不是模态框中的所有群
            ModalGroupAllList:[],        //模态框中的所有所有群
            GroupClassificationMap:[],  //保存分组的下拉选对象以及 每个分组有哪些group
            GroupClassification:[],     //保存分组的下拉选对象只有分组
            ModalGroupNameVisible:true //是否显示模态框中的分组名称
        }
    }

    componentDidMount() {
        this.findAllGroup(false);
        this.groupClassification();
    }

    //处理群分类
    groupClassification=()=>{
        let me = this;
        fetch('/snail/group/findGroupClassification', {
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function (response) {
            response.json().then(function (data) {
                let children = [];
                data = data.data;
                me.setState({
                    GroupClassificationMap:data
                });
                // children.push(<Option key="全部">全部</Option>);
                for (let i = 0; i < data.length; i++) {
                    children.push(<Option key={data[i].name}>{data[i].name}</Option>);
                }
                me.setState({
                    GroupClassification:children
                });
                //设置默认的分组
                me.props.form.setFieldsValue({
                    groupType: children[0].key,
                })
            })
        }, function (error) {
            message.error("提交数据失败！");
        })
    };
    /*
    *   isModal 是否为模态框
    * */
    //处理返回所有群
    findAllGroup=(isModal)=>{
        let me = this;
        //获取所有群
        fetch('/snail/group/findAllGroup', {
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function (response) {
            response.json().then(function (data) {
                data = data.data.pageInfo.list;
                let groupList = [];
                let checkList = [];
                for (var i = 0; i < data.length; i++) {
                    if(data[i].groupType==2) {
                        groupList.push({
                            label: data[i].groupName,
                            value: data[i].groupName + "," + data[i].chatId
                        });
                        checkList.push(data[i].groupName + "," + data[i].chatId)
                    }
                }
                //设置模态框中的所有群
                if(isModal){
                    me.setState({
                        ModalGroupAllList:groupList
                    })
                }else{
                    me.setState({
                        GroupAllList: groupList,
                        GroupCheckedList: checkList
                    });
                }
                me.props.form.setFieldsValue({
                    sendType: sendTitleText,
                })
            })
        }, function (error) {
            message.error("获取数据失败！");
        })
    };
    //发送群消息
    handleSubmit = (e) => {
        e.preventDefault();
        let me = this;
        let data = this.props.form.getFieldsValue();
        let urlArr = [];
        data["sendType"] = sendTypeIdx[data["sendType"]];
        //添加群到分组的时候无法获取label的值把label和url拼接在一起
        for(let i = data.chatId.length-1;i>=0;i--){
            if(data["chatId"][i]!=undefined){
                urlArr.push(data["chatId"][i].split(",")[1]);
            }
        }
        data.chatId = urlArr;
        fetch('/snail/attendence/groupDepartmentNotification', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            response.json().then(function (data) {
                if (data.code == 200) {
                    message.info(data.data);
                }
                me.groupClassification();
                me.findAllGroup();
                me.props.form.setFieldsValue({
                    chatId: []
                });
            })
        }).catch((e)=>{
            message.info("发送失败");
            me.groupClassification();
            me.findAllGroup();
            me.props.form.setFieldsValue({
                chatId: []
            });
        })
    };

    //控制复选框是否全选
    checkAll = (checkedValues) => {
        let checked = checkedValues.target.checked;
        if (checked) {
            this.props.form.setFieldsValue({
                chatId: this.state.GroupCheckedList
            })
        } else {
            this.props.form.setFieldsValue({
                chatId: []
            })
        }
    };

    //下拉选 发送类型
    handleSelectChange = (value) => {
        if (value == sendTitle) {
            this.setState({
                showSendLink: false
            });
            this.props.form.setFieldsValue({
                sendType: sendTitleText,
            })
        } else if (value == sendLink) {
            this.setState({
                showSendLink: true
            });
            this.props.form.setFieldsValue({
                sendType: sendLinkText,
            })
        }
    };
    //选择群分类显示群信息
    handleGroupTypeChange = (value) => {
        //如何是选择全部 请求findAllGroup
        if(value=="全部"){
            this.findAllGroup(false);
        }
        this.props.form.setFieldsValue({
            groupType: value,
        });
        let groupMap = this.state.GroupClassificationMap;
        let groupList = [];
        let checkList = [];
        for (let i = 0; i < groupMap.length; i++) {
            if(groupMap[i].name==value) {
                for(let j=0;j<groupMap[i].group.length;j++) {
                    let group = groupMap[i]["group"][j];
                    groupList.push({
                        label: group.groupName,
                        value: group.groupName+","+group.chatId
                    });
                    checkList.push(group.groupName+","+group.chatId)
                }
            }
        }
        this.setState({
            GroupAllList: groupList,
            GroupCheckedList: checkList
        });
    };
    //点击添加弹出新增的模态框
    handleAddGroupModal=()=>{
        this.setState({
            ModalVisible:true,
            ModalGroupNameVisible:true
        });
        this.findAllGroup(true);
    };
    //删除分组
    handleMinusClassification=()=>{
        let formData = this.props.form.getFieldsValue();
        let param = {};
        let me = this;
        param.classificationName = formData.groupType;
        fetch('/snail/group/deleteClassification', {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: jQuery.param(param)
        }).then(function (response) {
            response.json().then(function (data) {
                if (data.code == 200) {
                    me.groupClassification();
                    me.findAllGroup();
                    me.props.form.setFieldsValue({
                        chatId: []
                    });
                    message.info(data.message);
                }
            })
        }, function (error) {

        })
    };

    //点击 + 弹出新增模态框
    handlePlusGroupModal=()=>{
        this.setState({
            ModalVisible:true,
            ModalGroupNameVisible:false
        });
        this.findAllGroup(true);
    };
    //删除分组中的群
    handleMinusGroup=()=>{
        let me = this;
        let param=[];
        let data = this.props.form.getFieldsValue();
        for(let i=0;i<data.chatId.length;i++){
            let item = data["chatId"][i];
            let obj = {};
            obj.groupName=item.split(",")[0];
            obj.chatId=item.split(",")[1];
            obj.groupClassificationName=data.groupType;
            param.push(obj);
        }
        fetch('/snail/group/deleteGroupClassification', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        }).then(function (response) {
            response.json().then(function (data) {
                if (data.code == 200) {
                    me.groupClassification();
                    me.findAllGroup();
                    me.props.form.setFieldsValue({
                        chatId: []
                    });
                    message.info(data.message);
                }
            })
        }, function (error) {
        })

    };

    //保存群到指定的分组当中
    handleAddGroupSubmit=()=>{
        /**
         *  请求参数
         * [{"chatId":"addGro","groupName":"ceshifenzy","groupClassificationName":"分组一"},{"chatId":"addGroup2Url","groupName":"addGroup2","groupClassificationName":"分组五"}]
         */
        let me = this;
        let formData = this.props.form.getFieldsValue();
        let param = [];
        for(let i=0;i<formData.allGroup.length;i++){
            let groupObj = {};
            let item = formData["allGroup"][i];
            groupObj.groupName = item.split(",")[0];
            groupObj.chatId = item.split(",")[1];
            if(formData.groupClassificationName){
                groupObj.groupClassificationName=formData.groupClassificationName;
            }else {
                groupObj.groupClassificationName = formData.groupType;
            }
            param.push(groupObj);
        }
        fetch('/snail/group/saveGroupClassification', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        }).then(function (response) {
            response.json().then(function (data) {
                if (data.code == 200) {
                    me.setState({
                        ModalVisible: false
                    });
                    me.groupClassification();
                    me.findAllGroup();
                    me.props.form.setFieldsValue({
                        chatId: []
                    });
                    me.props.form.setFieldsValue({
                        allGroup:[]
                    });
                    message.info(data.message);
                }
            })
        }, function (error) {
        });
    };

    //隐藏模态框
    handleCancel=()=>{
        this.setState({
            ModalVisible: false
        });
    };

    render() {
        const {getFieldProps} = this.props.form;
        let me = this;

        //选择器（从已有数据中输入选择）
        const children = [];
        for (let i = 10; i < this.state.GroupAllList.length; i++) {
            children.push(<Option key={i.toString(36)+i}>{i.toString(36)+i}</Option>);
        }
        function handleChange(value) {
            console.log("value:",value);
        }
        return (
            <div>
                <Form horizontal onSubmit={this.handleSubmit}>
                    <Card>
                        <Form.Item
                            label="发送类型"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                            style={{marginTop:30}}
                        >
                            <Select id="select" size="large" defaultValue="2"
                                    style={{width: 200}} {...getFieldProps('sendType')}
                                    onChange={me.handleSelectChange}>
                                <Option value="1">主题发送</Option>
                                <Option value="2">链接发送</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label="群分类"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                        >
                            <Select id="select" size="large" defaultValue="2"
                                    style={{width: 200}} {...getFieldProps('groupType')}
                                    onChange={me.handleGroupTypeChange}>
                                {this.state.GroupClassification}
                            </Select>
                            <Button type="primary" onClick={me.handleAddGroupModal} style={{margin:"0 10px"}}>新建</Button>
                            <Button type="primary" onClick={me.handleMinusClassification}>删除</Button>
                        </Form.Item>
                        <div>
                            <Checkbox onChange={me.checkAll} style={{position:"absolute",marginTop:"10px",marginLeft:"20%"}}/>
                            <Form.Item
                                label="群组"
                                labelCol={{span: 6}}
                                wrapperCol={{span: 14}}
                            >

                                {/*选择器（从已有数据中输入选择）*/}
                                <Select
                                    mode="multiple"
                                    style={{ width: '100%' }}
                                    placeholder="Please select"
                                    // defaultValue={['a10', 'c12']}
                                    labelInValue
                                    onChange={handleChange}
                                >
                                    {children}
                                </Select>


                                <CheckboxGroup options={this.state.GroupAllList}
                                               defaultValue={this.state.GroupCheckedList} {...getFieldProps('chatId')} />
                                <Icon type="plus" onClick={me.handlePlusGroupModal}/>
                                {/*<Icon type="minus" onClick={me.handleMinusGroup} style={{marginLeft:30}} />*/}
                            </Form.Item>
                        </div>

                        <Form.Item
                            label="正文"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                        >
                            <Input type="textarea"   {...getFieldProps('text')} style={{width: 300}}
                            />
                        </Form.Item>
                        <Form.Item
                            label="文件"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                            style={{display: this.state.showSendLink ? "block" : "none"}}
                        >
                            <GroupMessageFileUpload />
                        </Form.Item>
                        <Form.Item
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14,offset:6}}
                        >
                            <div><Button type="primary" htmlType="submit">发送</Button></div>
                        </Form.Item>
                    </Card>
                </Form>
                <Modal title="请选择要添加的群" visible={this.state.ModalVisible}
                       onOk={this.handleAddGroupSubmit} onCancel={this.handleCancel}
                >
                    <Form horizontal onSubmit={this.handleAddGroupSubmit}>
                        <Form.Item
                            label="分类名称"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                            style={{display:this.state.ModalGroupNameVisible?"block":"none"}}
                        >
                            <Input {...getFieldProps('groupClassificationName')} />
                        </Form.Item>
                        <Form.Item
                            label="群组"
                            labelCol={{span: 6}}
                            wrapperCol={{span: 14}}
                        >
                            <CheckboxGroup options={me.state.ModalGroupAllList}
                                           {...getFieldProps('allGroup')} />
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
        )
    }
}

const SendGroupMessageDD = Form.create()(GroupMessage);
export default SendGroupMessageDD;
