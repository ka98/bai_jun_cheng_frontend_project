import React from 'react';
import {Link} from 'react-router-dom';
import { Row,Col,Button,Input,Form,message } from 'antd';
import jQuery from 'jquery';
//父组件
import groupManagement from "../GroupManagement";
const FormItem=Form.Item;
export default class groupDetailsLeft extends React.Component{
    constructor(props){
        super(props);
        this.state={
            initData:[],
            empDetail:{},    //总数据
            Data:[]
        }
    }
    componentDidMount(){
        console.log(this.props)
    }
    InitLoad = (empDetail) =>{
        console.log(empDetail)
        this.setState({
            empDetail: empDetail
        });
    };
    componentWillReceiveProps(nextProps) {
        console.log(nextProps)
        if(this.state.initData) {
            this.props.form.setFieldsValue(nextProps.empDetail);
        }
        this.setState({
            initData:false
        });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        //表单的数据
        const formData = this.props.form.getFieldsValue();
        let param={};
        param.id=this.state.empDetail.id;
        param.groupName=formData.groupName;
        param.hookUrl=formData.hookUrl;
        fetch('/snail/group/saveGroup ', {
            method: "POST",
            headers:{
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body:jQuery.param(param)
        }).then(function(response) {
            message.success("保存成功！");
            return response.json();
        },function(error){
            message.error("保存失败！");
            // console.log("error:",error);
        })
    };
    render(){
        const { getFieldDecorator } = this.props.form;
        return(
            <form onSubmit={this.handleSubmit}>
                <Row style={{paddingBottom:'10px',borderBottom:'1px solid #ccc'}}>
                    <Col>
                        <Button style={{display: this.state.empDetail.groupType==2 ? "none" : "left"}} className="baocun" htmlType="submit" onClick={this.handleSaveClick}>保存</Button>
                        <Button><Link to={{pathname:'/others/groupManagement'}} >返回</Link></Button>
                    </Col>
                </Row>
                <div style={{marginTop:'20px'}}>
                    <Form.Item
                        label="部门名称"
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 12 }}
                        style={{marginTop:'10px'}}
                        key="groupName"
                    >
                        {getFieldDecorator('groupName',{
                            initialValue:this.state.empDetail.groupName
                        })(
                            <Input disabled={this.state.empDetail.groupType==2?true:false}/>
                        )}
                    </Form.Item>
                    <Form.Item
                        label={this.state.empDetail.groupType==2?"群主（ownerName）":"hookUrl"}
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 12 }}
                        key={this.state.empDetail.groupType==2?"群主（ownerName）":"hookUrl"}
                    >
                        {getFieldDecorator('hookUrl',{initialValue:this.state.empDetail.groupType==2?this.state.empDetail.ownerName:this.state.empDetail.hookUrl})
                        (
                            <Input disabled={this.state.empDetail.groupType==2?true:false}/>
                        )}
                    </Form.Item>
                </div>
            </form>
        )
    }
}
