import React from 'react';
import {Form,Tabs,message } from 'antd';
//子组件
import groupDetailsLeft from "./groupDetailsLeft";
import WrappedgroupDetailsRight from "./groupDetailsRight";
const TabPane=Tabs.TabPane;
const WrappedgroupDetailsLeft = Form.create () (groupDetailsLeft);
export default class groupDetails extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            empDetail:[],       //总数据
            isDisabled:true,
            tDate:[],        //钉钉内部群（groupId=2）时，存储的数据
            groupType:[],      //群组类型(为false时，groupType等于2，true是1或3)
            activeKey:[],      //面板key
        }
    }
    componentDidMount () {
        console.log(this.props);
        const groupType=this.props.match.params.groupType;
        this.setState({
            groupType:this.props.match.params.groupType
        }, () => {
            const param = [];
            param.groupId = this.props.match.params.id;
            this.fetchFn(param);
            console.log(param)
        });
    }
    fetchFn = (param) => {
        const groupType=this.state.groupType;
        fetch('/snail/group/findGroupById', {
            method: 'POST',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                'Accept': 'application/json',
            },
            body: "groupId=" + param.groupId
        }).then(response => {
            var per = this;
            response.json().then(function (data) {
                if (data.code == 200) {
                    per.setState({
                        empDetail: data.data
                    });
                    console.log(groupType)
                    // if (groupType==false){
                    //     per.setState({
                    //         activeKey:"2"
                    //     })
                    // }else {
                    //     per.setState({
                    //         activeKey:"1"
                    //     });
                    //     per.formRef.InitLoad(per.state.empDetail);
                    // }
                    const ab = groupType == false ? per.setState({activeKey:"2"}) : per.setState({activeKey:"1"});per.formRef.InitLoad(per.state.empDetail);
                    return ab
                }
            });
        }).catch((e) => {
            message.error("请求错误！");
            console.log("e:", e);
        })
    };

    componentWillReceiveProps(nextProps) {
        const per=this;
        per.setState({
            empDetail: nextProps.empDetail,
        });
        per.formRef.InitLoad(nextProps.empDetail);
    }

    // changeTabPosition=(activeKey)=>{
    //     console.log("activeKey",activeKey);
    //     activeKey
    // };
    render(){
        let empDetail = this.state.empDetail;
        return(
            <div style={{background:"#fff",height:"100%"}}>
                <Form>
                    <div className="card-container">
                        <Tabs
                            type="card"
                            // onChange={this.changeTabPosition}
                            defaultActiveKey={this.state.activeKey}
                            // defaultActiveKey="2"
                        >
                            <TabPane tab="基本信息" key="1">
                                <WrappedgroupDetailsLeft wrappedComponentRef = {(inst) => this.formRef = inst}/>
                            </TabPane>
                            <TabPane tab="员工" key="2" >
                                <WrappedgroupDetailsRight empDetail={empDetail}/>
                            </TabPane>
                        </Tabs>
                    </div>
                </Form>
            </div>
        )
    }
}
