import React from "react";
import {Form,Select,Input,Button} from "antd";
const Option = Select.Option;

const sendSuperAdminText="钉钉群发组";
const sendSuperAdmin = 1;
const sendOperationText="钉钉内部群";
const sendOperation = 2;
const sendDingText="运营组";
const sendDing = 3;
const sendTypeIdx={
    "钉钉群发组":sendSuperAdmin,
    "钉钉内部群":sendOperation,
    "运营组":sendDing,
};
export default class FirstStep extends React.Component{
    constructor(props){
        super(props);
        this.state={
            groupType:[],    //新建群类型
            name:[],
            sendType:[],
            form:[],
            isDisabled:false,    //是否编辑
            display:"block",     //按钮的显示隐藏
        }
    }
    //定义新建群类型
    handleSelectChange = (value) => {
        const groupType=`${value}`;
        this.setState({
            groupType:`${value}`
        });
        if (value == sendDing) {
            const groupType=`${value}`;
            this.setState({
                groupType:`${value}`
            });
            this.setState({
                name:this.state.groupType
            });
            this.props.form.setFieldsValue({
                sendType: sendDingText,
            });
            let d = this.state.form;
            d.push(name);
            this.setState({
                form:d
            });
            this.setState({
                groupType:`${value}`
            });
        }
    };
    //表单信息
    check = () => {
        this.props.form.validateFields(
            (err) => {
                if (!err) {
                    let formData = this.props.form.getFieldsValue();
                    this.setState({
                        isDisabled:true,
                        display:"none"
                    });
                    this.props.saveFirstFormRef(formData);
                }
            },
        );
    };
    render(){
        const {getFieldDecorator} = this.props.form;
        let me=this;
        let isDisabled = this.state.isDisabled;
        return(
            <div>
                <Form.Item
                    label="群组类型"
                    // labelCol={{span: 6}}
                    // wrapperCol={{span: 14}}
                >
                    {getFieldDecorator('groupType',{
                        rules: [{ required: true, message: '请选择群组类型!' }],
                    })(
                        <Select
                            id="select"
                            style={{width: 130}}
                            placeholder="请选择人员"
                            // defaultValue="1"
                            onChange={me.handleSelectChange}
                            disabled={isDisabled}
                        >
                            <Option value="1">钉钉群发组</Option>
                            <Option value="2">钉钉内部群</Option>
                            <Option value="3">运营组</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item
                    id="control-input"
                    label="创建钉钉群名称"
                    // labelCol={{ span: 6 }}
                    // wrapperCol={{ span: 36 }}
                >
                    {getFieldDecorator("groupName",{
                        rules:[{ required: true, message: '请输入内容！' }],
                    })(
                        <Input disabled={isDisabled} placeholder="请输入..." style={{width:"130px"}}/>
                    )}
                </Form.Item>
                <Form.Item
                    id="control-textarea"
                    label="hookUrl"
                    // labelCol={{ span: 6 }}
                    // wrapperCol={{ span: 16 }}
                >
                    {getFieldDecorator("hookUrl",{
                        rules:[{required:true,message:'请输入内容！'}]
                    })(
                        <Input rows="3" disabled={isDisabled} />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary"
                            // style={this.state.display}
                            onClick={(e)=>{this.check(e)}}>
                        提交
                    </Button>
                </Form.Item>
            </div>
        )
    }
}
