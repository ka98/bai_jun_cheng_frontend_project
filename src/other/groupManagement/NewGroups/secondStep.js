import React from "react";
import {Form,Select,message,Button} from "antd";
import jQuery from 'jquery';

export default class SecondStep extends React.Component{
    constructor(props){
        super(props);
        this.state={
            aDate:[],       //总数据
            empIds:[],      //搜索框选中的人员
        }
    }
    //表单信息
    oncheck= () => {
        this.props.form.validateFields(
            (err) => {
                if (!err) {
                    console.info('success');
                    // let formData = this.props.form.getFieldsValue();
                    const formData=this.state.empIds;
                    console.log(formData);

                    this.props.saveSecondFormRef(formData);
                }
            },
        )
    };
    // 获取zong数据
    fetchFn= (url,param) => {
        param = param?param:{};
        param.startPage=this.state.currentPage;
        param.pageSize=this.state.pageSize;
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:jQuery.param(param)
        }).then((res) => {
                return res.json();
        }).then((data) => {
            data = data.pageInfo.list;
            for(let i=0;i<data.length;i++){
                    data[i].key=i;
            }
            this.setState ({
                aDate : data,
            });
        }).catch((e) => {
            message.error("获取员工信息失败！");
            // console.log("e(获取员工信息失败):",e);
        })
    };
    componentDidMount() {
        this.fetchFn("/snail/employees/findAllEmp");
    }
    render(){
        var self=this;
        const {getFieldDecorator} = this.props.form;
        //搜索选择人员：
        const children=[];
        for(let i=0;i<this.state.aDate.length;i++){
            children.push(<Option value={this.state.aDate[i].empId}>{this.state.aDate[i].name}</Option>);
        }
        function handleChange(value) {
            console.log(value);
            self.setState({
                empIds:value
            })
        }
        return(
            <div>
                <Form.Item
                    id="control-input"
                    label="选中的人员"
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                    style={{width:"340px"}}
                >
                    {getFieldDecorator('empIdList')(
                        <Select
                            mode="multiple"
                            style={{ width: '100%' }}
                            onChange={handleChange}
                            defaultValue={['张伟', '朱红燕']}
                        >
                            {children}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary"
                            display={this.state.display}
                            onClick={(e)=>{this.oncheck(e)}}>
                        提交
                    </Button>
                </Form.Item>
            </div>
        )
    }
}
