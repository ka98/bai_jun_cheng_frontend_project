import React from 'react';
import {Link} from 'react-router-dom';
import {Row,Col,Table,Button,message} from 'antd';
import jQuery from 'jquery';
import {Form} from "antd/lib/index";
import HTTPUtil from "../../util/HTTPUtil";
//跳转组件：
import AttendanceAccounting from "../../employee/detail/attendanceAccounting/finalAttendance";
import Person from "../../employee/detail/person";
const columns=[{
    title:'员工姓名',
    dataIndex:'name',
    render:(text,record)=>{
        return <a><Link to={"/person/"+record.empId+"/false"} >{text}</Link></a>;
    }
},{
    title:'手机号',
    dataIndex:'phone'
}];
class groupDetailsRight extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tDate:[],     //总数据
            empDetail:[],
            pageInfo:[],
            loading:false,     //是否正在加载中
            groupName:[],      //群名称
        }
    }
    componentDidMount(){
        console.log(this.props);
        let filter={};
        const groupId = this.props.empDetail.id;
        const groupType=this.props.empDetail.groupType;
        filter.groupId = groupId;
        filter.groupType = groupType;
        this.setState({groupName:this.props.empDetail.groupName,loading:true});
        console.log(filter.groupType)
        if (filter.groupType === 1 || filter.groupType === 3) {
            fetch("/snail/group/findEmpByGroupId", {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: jQuery.param(filter)
            }).then(response => {
                let jsonPromise = response.json();
                return jsonPromise;
            }).then(json => {
                this.setState({
                    pageInfo: json.data.pageInfo.list
                });
                this.setState({loading: false});
            }).catch((e) => {
                // console.log("e:",e);
                message.error("获取信息失败！")
            })
        } else if (filter.groupType === 2) {
            HTTPUtil.post("/snail/group/findEmpListByDingId",jQuery.param(filter),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
                if (data.code == 200) {
                    var per = this;
                    per.setState({
                        pageInfo: data.data
                    });
                }
                else if (data.code !== 200) {
                    message.error("获取数据失败！")
                }
                this.setState({loading: false});
            });
        }
    }

    // checkbox状态(选框)
    onSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({ selectedRowKeys });
        for(var i = 0; i < selectedRows.length; i++) {
            // console.log("selectedRowKeys[i]",selectedRows[i]);
            // console.log("selectedRowKeys[i][empId]",selectedRows[i]["empId"]);
            this.state.empIdList.push(selectedRows[i]["empId"])
        }
    };
    render() {
        //人员信息
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };
        //分页
        const pagination = {
            total: this.state.tDate.length,
            showSizeChanger: true,
            onShowSizeChange(current, pageSize) {
                // console.log("this.total:",this.total);
                // console.log("current:",current)
            },
            onChange(current) {
                // console.log("this.total222:",this.total);
                // console.log("current222:",current)
            }
        };
        return(
            <form onSubmit={this.handleSubmit}>
                <Row type="flex" justify="start" >
                    <Col span={4}>
                        {/*<Button style={{marginRight:'10px'}}>保存</Button>*/}
                        <Button
                            // style={{marginRight:'10px'}}
                        ><Link to={{pathname:'/others/groupManagement'}}>返回</Link></Button>
                        {/*<Button style={{color:'#fff',background:'#d2322d',borderColor:'#ac2925'}}>删除</Button>*/}
                    </Col>
                    <Col span={4}>
                        <div>{this.state.groupName}</div>
                    </Col>
                </Row>
                <div>
                    <Table
                        rowSelection={rowSelection}
                        dataSource={this.state.pageInfo}
                        columns={columns}
                        pagination={pagination}
                        loading={this.state.loading}
                    />
                </div>
            </form>
        )
    }
}
const WrappedgroupDetailsRight=Form.create()(groupDetailsRight);
export default WrappedgroupDetailsRight
