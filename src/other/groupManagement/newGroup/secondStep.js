import React from 'react';
import {Steps,Form,Input,Table,Button,Select,message} from 'antd';
import jQuery from 'jquery';
import classNames from 'classnames';
const Step = Steps.Step;
const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option=Select.Option;

//表格内容
const columns = [
    {
        title: '姓名',
        dataIndex: 'name',
        render:(text)=>{
            return <span>{text}</span>;
        }
    }, {
        title: '工号',
        dataIndex: 'workNum'
    }, {
        title: '手机号码',
        dataIndex: 'phone'
    }];
export default class SecondStep extends React.Component{
    constructor(props){
        super(props);
        this.state={
            modalVisible:[],
            forms:[],
            inputValue:null,     //选框所选内容
            tDate: [],     //获取全部数据
            empIdList:[],    //选中人员的id
            // selecteddata:[],    //选中人员的全部信息
            newName:[],      //传递到父组件中的数据-newName
            empIds:[],      //输入框被选中的人员
        }
    }
    componentWillReceiveProps(nextProps) {
        console.log("next:",nextProps);
        let isLoadUrl = nextProps.loadUrl;
        console.log("empIds(被选中的人员):",this.state.empIds);
        if(isLoadUrl){
            this.handleSubmit(event);
        }
    }
    //表单信息
    handleSubmit=(e)=>{
        e.preventDefault();
        let formData = this.props.form.getFieldsValue();
        console.log("this.props.forms:",this.props.form);
        console.log("表单的datayyyyy:",formData);
        var addressDOList={};
        addressDOList.form=this.state.form;
        // addressDOList.form=this.state.empIds;
        console.log("addressDOList:",addressDOList);
        this.props.getSecondStempFormData(addressDOList);
    };
    //获取表格中数据
    fetchFn= (url,param) => {
        param = param?param:{};
        param.startPage=this.state.currentPage;
        param.pageSize=this.state.pageSize;
        // this.setState({ loading: true });
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:jQuery.param(param)
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                let total = data.pageInfo.total;
                data = data.pageInfo.list;
                for(let i=0;i<data.length;i++){
                    data[i].key=i;
                }
                this.setState({
                    tDate : data,
                    total:total
                });
            })
            .catch((e) => {
                message.error("获取员工信息失败！");
                console.log("e(获取员工信息失败):",e);
            })
    };
    componentDidMount() {
        this.fetchFn("/snail/employees/findAllEmp");
    }
    // //快速搜索
    // handleSearch=(event)=>{
    //     console.log("value",this.state.inputValue);
    //     let param = {};
    //     param.workNum = this.state.inputValue;
    //     param.name=this.state.inputValue;
    //     this.fetchFn("/snail/employees/findAllEmp",param);
    // };
    // handleInputChange=(e)=> {
    //     console.log("e",e.target.value);
    //     this.setState({
    //         inputValue: e.target.value,
    //     });
    // };

    // checkbox状态(选框)
    onSelectChange = (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({
            selecteddata:selectedRows
        });
        console.log("selectedRows:",selectedRows);
        this.setState({ selectedRowKeys });
        let empIdData = [];
        for(var i=0;i<selectedRows.length;i++){
            empIdData.push(selectedRows[i]["empId"]);
        }
        this.setState({
            empIdList:empIdData
        });
    };

    // //确认添加该人员
    // onClickName=()=>{
    //     const newName=this.state.empIdList;
    //     console.log("newName:",newName);     //能获取该人的empId
    //     this.props.getSecondStempFormData(newName);
    // };
    render(){
        var self = this;
        const { selectedRowKeys } = this.state;
        const {getFieldDecorator} = this.props.form;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };
        //快速搜索 start
        const { style, size, placeholder } = this.props;
        const btnCls = classNames({
            'ant-search-btn': true,
        });
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        //搜索选择人员：
        const children=[];
        for(let i=0;i<this.state.tDate.length;i++){
            children.push(<Option value={this.state.tDate[i].empId}>{this.state.tDate[i].name}</Option>);
        }
        function handleChange(value) {
            console.log("value:",value);
            self.setState({
                empIds:value
            })
        }

        //分页
        const pagination = {
            total: this.state.total,
            showSizeChanger: true,
            startPage:[],
            onShowSizeChange(current, pageSize) {
                let param = {};
                param.workNum = self.state.inputValue;
                param.name=self.state.inputValue;
                console.log("current:",current,"pageSize:",pageSize);
                self.setState({
                    currentPage: current,
                    pageSize:pageSize
                },()=>{
                    self.fetchFn("/snail/employees/findAllEmp",param);
                });
            },
            onChange(current) {
                let param = {};
                param.workNum = self.state.inputValue;
                param.name=self.state.inputValue;
                console.log("current:",current);
                self.setState({
                    currentPage:current
                },()=>{
                    self.fetchFn("/snail/employees/findAllEmp",param);
                });
            }
        };
        return(
            <div>
                <Steps direction="horizontal"
                       // size="small"
                       current={1}>
                    <Step title="第一步"  description={
                        <div>
                            <Form horizontal form={this.props.form} onSubmit={this.handleSubmit} >
                                <Form.Item
                                    id="control-input"
                                    label="选中的人员"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 14 }}
                                    style={{width:"340px"}}
                                >
                                    {getFieldDecorator('empIdList')(
                                        <Select
                                            mode="multiple"
                                            style={{ width: '100%' }}
                                            onChange={handleChange}
                                            // defaultValue={['a10', 'c12']}
                                        >
                                            {children}
                                        </Select>
                                    )}

                                </Form.Item>
                            </Form>
                            {/*<Table rowSelection={rowSelection} columns={columns} dataSource={this.state.tDate}  pagination={pagination} style={{width:"420px"}}/>*/}
                        </div>
                    }/>
                    <Step title="第二步"  />
                </Steps>
            </div>
        )
    }
}
