import React from 'react';
import {Steps,Form,Input,Dropdown,Menu,Icon ,Select} from 'antd';
const Step = Steps.Step;
const FormItem = Form.Item;
const Option = Select.Option;

const sendSuperAdminText="超级管理员";
const sendSuperAdmin = 1;
const sendOperationText="运营组";
const sendOperation = 2;
const sendDingText="钉钉群";
const sendDing = 3;
const sendTypeIdx={
    "超级管理员":sendSuperAdmin,
    "运营组":sendOperation,
    "钉钉群":sendDing,
};
export default class FirstStep extends React.Component{
    constructor(props){
        super(props);
        this.state={
            form:[],
            groupType:[]
        }
    }
    componentWillReceiveProps(nextProps) {
        let isLoadUrl = nextProps.loadUrl;
        if(isLoadUrl){
            this.handleSubmit(event);
        }
    }
    //表单数据
    handleSubmit=(e)=>{
        e.preventDefault();
        //群名称form
        // let formDataOld = this.props.form.resetFields();
        let formData = this.props.form.getFieldsValue();
        console.log("表单的data:",formData);
        this.props.getFirstStempFormData(formData);
    };
    userExists(rule, value, callback) {
        if (!value) {
            callback();
        } else {
            setTimeout(() => {
                if (value === 'JasonWood') {
                    callback([new Error('抱歉，该用户名已被占用。')]);
                } else {
                    callback();
                }
            }, 800);
        }
    }
    //定义新建群类型
    handleSelectChange = (value) => {
        const groupType=`${value}`;
        this.setState({
            groupType:`${value}`
        });
        if (value == sendDing) {
            const groupType=`${value}`;
            this.setState({
                groupType:`${value}`
            });
            const name=this.state.groupType;
            let a=name;
            this.setState({
                name:a
            });
            this.props.form.setFieldsValue({
                sendType: sendDingText,
            });
            let d = this.state.form;
            d.push(name);
            this.setState({
                form:d
            });
            this.setState({
                groupType:`${value}`
            });
        }
    };

    render(){
        const {getFieldDecorator} = this.props.form;
        let me=this;
        return(
            <div>
                <Steps direction="horizontal" size="small" current={0}>
                    <Step title="第一步" description={
                        <Form horizontal form={this.props.form} onSubmit={this.handleSubmit} style={{width:"260px"}}>
                            <Form.Item
                                label="群组类型"
                                labelCol={{span: 6}}
                                wrapperCol={{span: 14}}
                            >
                                {getFieldDecorator('groupType')(
                                    <Select
                                        id="select"
                                        style={{width: 110}}
                                        placeholder="请选择人员"
                                        // defaultValue="1"
                                        onChange={me.handleSelectChange}
                                    >
                                        <Option value="1">钉钉群发组</Option>
                                        <Option value="2">钉钉内部群</Option>
                                        <Option value="3">运营组</Option>
                                    </Select>
                                )}
                            </Form.Item>
                            <Form.Item
                                id="control-input"
                                label="创建钉钉群名称"
                                labelCol={{ span: 10 }}
                                wrapperCol={{ span: 36 }}
                                hasFeedback
                            >
                                {getFieldDecorator("groupName")(
                                    <Input id="control-input" placeholder="请输入..." style={{width:"130px"}}/>
                                )}
                            </Form.Item>
                            <Form.Item
                                id="control-textarea"
                                label="hookUrl"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 16 }}
                            >
                                {getFieldDecorator("hookUrl")(
                                    <Input rows="3" />
                                )}
                            </Form.Item>
                        </Form>
                    } />
                    <Step title="第二步" />
                </Steps>
            </div>
        )
    }
}
