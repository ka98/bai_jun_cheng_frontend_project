import React from "react";
import {Modal,Steps,Button,message,Form,Select,Input,Popover} from "antd";
import jQuery from 'jquery';
import './NewGroups.css';
const Step = Steps.Step;

//子组件
import FirstStep from "../groupManagement/NewGroups/firstStep"
import SecondStep from "./NewGroups/secondStep";
const WrappedFirstStep=Form.create()(FirstStep);
const WrappedSecondStep=Form.create()(SecondStep);
export default class NewGroups extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            current: 0,      //当前步骤
            modalVisible: false,      //弹出框隐藏
            PreviewModalVisible:false,
            group:[],     //第一步的数据
            empIds:[],      //第二步的数据
            loading:false,
        };
    }
    //对于弹出框的显示
    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    //确定提交
    successParent=()=>{
        this.setModalVisible(false);
        this.setState({
            PreviewModalVisible:true,
            loading:true,
        });
        this.fetchFn("/snail/group/findGroupTypeList");
        location.reload();
    };
    fetchFn= (url,param) => {
        param = {};
        param.group=this.state.group;
        param.empIds=this.state.empIds;
        console.log("0000:",param);
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:jQuery.param(param)
        }).then((res) => {
            return res.json();
        }).then((data) => {
            console.log("dataSecond:",data);
            this.setState({
                //设置提交后的各数据为空
                group:[],
                empIds:[]
            });
            this.setState({ loading: false });
            message.success("创建成功！")
        }).catch((e) => {
            message.error("创建失败！");
            console.log("eSecond:",e);
            this.setState({ loading: false });
        })
    };

    //接收第一步的数据
    changeFirstProps=(e)=>{
        this.setState({
            group:e
        });
        console.log("第一个子组件传过来的props:",e)
    };
    //接收第二部的数据
    changeSecondProps=(e)=>{
        const per=this;
        console.log("第二个组件传过来的：",e);
        per.setState({
            empIds:e
        })
    };
    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }
    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    render(){
        const { current } = this.state;
        return(
            <div style={{width:'118px', float:'left'}}>
                <Button type="primary" style={{marginRight:'10px'}} onClick={() => this.setModalVisible(true)}>添加钉钉群</Button>
                <Modal
                    title="请完善以下信息："
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}     //对话框是否可见
                    // onOk={this.setModalVisible(false)}
                    // okText="确定"
                    onCancel={() => this.setModalVisible(false)}
                    maskClosable={()=>this.setModalVisible(false)}    //点击蒙层是否允许关闭
                    footer={null}
                >
                    <Steps current={current} progressDot direction="vertical">
                        <Step title="第一步" description={<WrappedFirstStep saveFirstFormRef={(e)=>{this.changeFirstProps(e)}}
                        />} />
                        <Step title="第二步" description={<WrappedSecondStep saveSecondFormRef={(e)=>{this.changeSecondProps(e)}}
                        />}/>
                    </Steps>
                    <div className="next">
                        {
                            current < 1
                            && <Button type="primary" onClick={() => this.next()}>下一步</Button>
                        }
                        {
                            current === 1
                            &&
                            <Button className="yes" type="primary" loading={this.state.loading} onClick={() =>this.successParent()}>确定</Button>
                        }
                        {
                            current > 0
                            && (
                                <Button onClick={() => this.prev()}>上一步</Button>
                            )
                        }
                    </div>
                </Modal>
            </div>
        )
    }
}
