import React from 'react';
import {Button,Modal,Form} from 'antd';
import jQuery from 'jquery'
//子组件
import FirstStep from "./newGroup/firstStep";
import SecondStep from "./newGroup/secondStep";
const WrappedFirstStep = Form.create()(FirstStep);
const WrappedSecondStep=Form.create()(SecondStep);

export default class NewGroup extends React.Component{
    constructor(props){
        super(props);
        this.state={
            modalVisible: false,
            PreviewModalVisible:false,
            modal2Visible:false,
            visible:true,
            isLoad:false,    //是否请求
            formData:[],
            // empIdList:[] ,
            empDetailSecond:[],
            groupName:[],       //从FirstStep中传过来“群组名”等的数据
            newName:[],   //从SecondStep中传过来“群成员empId”的数据
            loading:false,   //是否处于正在加载中
            // destroyOnClose:false,     //关闭弹出框时，其内的数据是否被销毁
        }
    }
    //第一步弹出框-取消
    setModalVisible(modalVisible) {
        this.setState({ modalVisible });

    }
    //第一步弹出框-确定
    showPreviewData=()=>{
        this.setState({
            isLoad:true
        });
        this.setModalVisible(false);
        this.setState({
            PreviewModalVisible:true
        });
        this.setModal2Visible(true);
    };
    //接受第一步数据
    getFirstStempFormData=(data)=>{
        let groupName=data;
        if(data==null){
            return
        }else {
            this.setState({
                groupName:data
            });
        }
        console.log("groupName:",groupName);
    };

    //第二步
    setModal2Visible(modal2Visible){
        this.setState({ modal2Visible });
    };
    showPreview2Data=()=>{

        this.setModal2Visible(false);
        this.setState({
            visible:false
        });
        console.log("this.props.empIdList:",this.props.empIdList);
        console.log("第一步返回的数据：",this.state.groupName);
        console.log("第er步返回的数据：",this.state.newName);
        this.fetchFn("/snail/group/saveGroup");
    };
    getSecondStempFormData=(data)=>{
        console.log("data:",data);
        let newName=data;
        this.setState({
            newName:data
        });
        console.log("newName:",newName);
    };
    fetchFn= (url,param) => {
        param = {};
        param.groupName=this.state.groupName;
        param.empIds=this.state.newName;
        console.log("0000:",param);
        this.setState({ loading: true });
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:jQuery.param(param)
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log("dataSecond:",data);
                this.setState({
                    groupName:[]
                });
                this.setState({ loading: false });
            })
            .catch((e) => {
                console.log("eSecond:",e)
            })
    };
    render(){
        return(
            <div style={{width:'118px', float:'left'}} >
                <Button type="primary" style={{marginRight:'10px'}} onClick={() => this.setModalVisible(true)}>添加钉钉群</Button>
                <Modal
                    title="请完善以下信息："
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}     //对话框是否可见
                    onOk={this.showPreviewData}
                    okText="下一步"
                    onCancel={() => this.setModalVisible(false)}
                    maskClosable={()=>this.setModalVisible(true)}    //点击蒙层是否允许关闭
                >
                    <WrappedFirstStep loadUrl={this.state.isLoad} getFirstStempFormData={this.getFirstStempFormData}/>
                </Modal>
                <Modal
                    title="请填充信息："
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modal2Visible}
                    okText="确定"
                    onOk={this.showPreview2Data}
                    // destroyOnClose={this.state.destroyOnClose(false)}
                    // cancelText="上一步"
                    onCancel={() => this.setModal2Visible(false)}
                    maskClosable={()=>this.setModalVisible(true)}
                    // style={{width:"700px"}}
                    style={{width:"100%"}}
                >
                    <WrappedSecondStep loadUrl={this.state.isLoad} getSecondStempFormData={this.getSecondStempFormData}/>
                </Modal>
            </div>
        )
    }
    // componentWillUnmount=(yy)=>{
    //     console.log("yy:",yy)
    // };
}
