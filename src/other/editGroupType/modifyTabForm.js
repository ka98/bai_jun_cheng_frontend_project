import React, { Component } from 'react'
import { Form, Input, Select, Button } from 'antd'
import { message } from "antd/lib/index";
import CollectionCreateForm from "./CollectionCreateForm"
import { checkObjRequired } from 'util/common.js'
const FormItem = Form.Item
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class Form1 extends Component{
    constructor(props) {
        super(props);
        this.state = {
            visible: false,     //弹出框是否可见
            keepIds: []
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.groupTypeChange = this.groupTypeChange.bind(this)
        this.handleNew = this.handleNew.bind(this)
        this.update = this.update.bind(this)
    }
    handleSubmit(e) {
        e.stopPropagation()
        e.preventDefault()
        let obj = this.props.form.getFieldsValue()
        if (checkObjRequired(obj)) {
            fetch('/snail/group/saveGroupClassification', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(obj)
            }).then(
                res => res.json()
            ).then(data => {
                if(data.code === 200){
                    message.info('修改成功！')
                    this.props.form.setFieldsValue({groupSortName: '',ids: [] })
                    this.props.update()
                }else{
                    message.info(data.message)
                }
            })
        }
    }
    update(newGroupName) {
        this.props.update()
        this.props.form.setFieldsValue({groupSortName: ''})
    }
    groupTypeChange(value) {
        const canSetGroup = this.props.groupType.find((item) => {return item.name === value}).group.map((item) => item.id)
        this.setState({
            keepIds: canSetGroup
        })
        this.props.form.setFieldsValue({ids: canSetGroup})
    }
    handleNew = () => {
        const groupSortName = this.props.form.getFieldsValue().groupSortName
        groupSortName ? this.setState({
            visible:true
        }) : message.error('请先选择要修改名称的群分类！')
    }
    handleCancel = () => {
        this.setState({ visible: false });
    }
    handleCreate = () => {
        console.log(this.formRef.props)
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            console.log('111：', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    }
    handleClose() {
        this.setState({
            visible: false
        })
    }
    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }
    render() {
        const { getFieldsValue, getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem label="群分类" {...formItemLayout}>
                    {getFieldDecorator('groupSortName', {
                    rules: [
                        {require: true, message: '请输入群分类的名称！'}
                    ],
                })(
                    <Select placeholder="选择要修改的群分类" onChange={this.groupTypeChange}>
                        {
                            this.props.groupType && this.props.groupType.length ? this.props.groupType.map((item) => {
                                return (
                                    <Option value={item.name} key={item.name}>{item.name}</Option>
                                )
                            }):null
                        }
                    </Select>
                )}
                </FormItem>
                <FormItem label="修改群组" {...formItemLayout}>
                    {getFieldDecorator('ids')(
                        <Select
                            mode="multiple"
                            placeholder="群组"
                            optionFilterProp="children"
                        >
                            {
                                this.props.groupMap.map((v) => {
                                    return (
                                        <Option value={v.id} key={v.id}>{v.groupName}</Option>
                                    )
                                })
                            }
                        </Select>
                    )}
                </FormItem>
                <FormItem wrapperCol={{span:14, offset: 6}}>
                    <Button type="primary" htmlType="submit" className="baocun">修改</Button>
                    <Button type="primary" onClick={this.handleNew}>修改群分类名称</Button>
                    <CollectionCreateForm
                        groupType={this.props.groupType}
                        wrappedComponentRef={this.saveFormRef}
                        keepIds={this.state.keepIds}
                        visible={this.state.visible}
                        onCancel={this.handleCancel}
                        update={this.update}
                        groupSortName={getFieldsValue().groupSortName}
                        handleClose={this.handleClose.bind(this)}
                    />
                </FormItem>
            </Form>
        )
    }
}
export default Form.create()(Form1)
