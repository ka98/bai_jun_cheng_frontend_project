import React, { Component } from 'react'
import { Form, Input, Select, Button, Checkbox, Transfer } from 'antd'
import { message } from "antd/lib/index";
import { checkObjRequired } from 'util/common.js'
const FormItem = Form.Item
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class Form1 extends Component{
    constructor(props) {
        super(props);
        this.state = {
            groupMap:[],
            targetKeys:[]
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(e) {
        e.preventDefault()
        let obj = this.props.form.getFieldsValue()
        if (checkObjRequired(obj)) {
            fetch('/snail/group/saveGroupClassification', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(obj)
            }).then(
                res => res.json()
            ).then(data => {
                if(data.code === 200){
                    message.info('添加成功！')
                    this.props.form.setFieldsValue({groupSortName: '',ids: [] })
                    this.props.update()
                }else{
                    message.info(data.message)
                }
            })
        }
    }
    // componentDidMount(){
    //     console.log(this.props)
    //     const { groupMap } = this.props
    //     this.setState({
    //         groupMap: groupMap.map( item => (
    //             {
    //                 key: item.id + '',
    //                 title: item.groupName,
    //             }))
    //     }, () => {
    //         console.log(this.state.groupMap)
    //     })
    // }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { groupMap } = this.props
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem label="群分类" {...formItemLayout}>
                    {getFieldDecorator('groupSortName', {
                        rules: [
                            {require: true, message: '请输入群分类的名称！'}
                        ],
                    })(
                        <Input placeholder="选择要添加的群分类"/>
                    )}
                </FormItem>
                <FormItem label="添加群组" {...formItemLayout}>
                    {getFieldDecorator('ids')(
                        // <Transfer
                        //     dataSource={groupMap}
                        //     showSearch
                        //     // filterOption={this.filterOptionChild}
                        //     // targetKeys={this.state.targetKeys}
                        //     // onChange={this.onChangeChild}
                        //     render={item => item.title}
                        // />
                            <Select
                                mode="multiple"
                                placeholder="群组"
                                optionFilterProp="children"
                        >
                                {
                                    groupMap.map((v) => {
                                        return (
                                            <Select.Option value={v.id} key={v.id}>{v.groupName}</Select.Option>
                                        )
                                    })
                                }
                            </Select>
                    )}
                </FormItem>
                <FormItem wrapperCol={{span:14, offset: 6}}>
                    <Button type="primary" htmlType="submit">添加</Button>
                </FormItem>
            </Form>
        )
    }
}
export default Form.create()(Form1)