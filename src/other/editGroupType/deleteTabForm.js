import React, { Component } from 'react'
import { Form, Input, Select, Button } from 'antd'
import { message } from "antd/lib/index";
import jQuery from 'jquery'
const FormItem = Form.Item
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class Form3 extends Component{
    constructor(props) {
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(e) {
        e.preventDefault()
        let obj = this.props.form.getFieldsValue()
        fetch('/snail/group/deleteClassification', {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: jQuery.param(obj)
        }).then(
            res => res.json()
        ).then(data => {
            if(data.code === 200){
                message.info('删除成功！')
                this.props.form.setFieldsValue({classificationName: ''})
                this.props.update()
            }else{
                message.info(data.message)
            }
        })
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem label="群分类" {...formItemLayout}>
                    {getFieldDecorator('classificationName',{
                        rules: [
                            {require: true, message: '请选择群分类！'}
                        ],
                    })(
                        <Select placeholder="选择要删除的群分类">
                            {
                                this.props.groupType && this.props.groupType.length ? this.props.groupType.map((item) => {
                                    return (
                                        <Option value={item.name} key={item.name}>{item.name}</Option>
                                    )
                                }):null
                            }
                        </Select>
                    )}
                </FormItem>
                <FormItem wrapperCol={{span:14, offset: 6}}>
                    <Button type="primary" htmlType="submit">删除</Button>
                </FormItem>
            </Form>
        )
    }
}
export default Form.create()(Form3)