import React from "react";
import {Modal,Form,Input,Select,Button,message} from "antd/lib/index";
const FormItem=Form.Item;
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class CollectionCreateForm extends React.Component{
    constructor(props){
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this)
        this.state = {
            visible: false
        }
        // this.groupTypeChange=this.groupTypeChange.bind(this)
    }
    handleSubmit (e) {
        e.stopPropagation()
        e.preventDefault()
        let obj = this.props.form.getFieldsValue();
        this.props.handleClose()
        console.log("obj:",obj)
        fetch('/snail/group/saveGroupClassification', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({...obj, ids:this.props.keepIds, formerGroupSortName: this.props.groupSortName})
        }).then(
            res => res.json()
        ).then(data => {
            if(data.code === 200){
                message.info('修改成功！')
                this.props.form.setFieldsValue({groupSortName: '',ids: [] })
                // this.props.update()
                this.props.update()
            }else{
                message.info(data.message)
            }
        })
    }
    // componentWillReceiveProps(){
    //     this.props.form.setFieldsValue();
    // }
    // groupTypeChange(value) {
    //     const canSetGroup = this.props.groupType.find((item) => {return item.name === value}).group.map((item) => item.id)
    // }
    render(){
        const { getFieldDecorator } = this.props.form;
        const { visible, onCancel, onCreate, form, groupSortName } = this.props;
        console.log(this.props.groupSortName)
        return(
            <Modal
                visible={visible}
                title="修改的群分类名称"
                okText="确定"
                footer={null}
                onCancel={onCancel}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem label="群分类" {...formItemLayout}>
                        <Input value={this.props.groupSortName} disabled/>
                    </FormItem>
                    <FormItem label="新群分类名称" {...formItemLayout} >
                        {getFieldDecorator('groupSortName')(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem wrapperCol={{span:14, offset: 6}}>
                        <Button type="primary" htmlType="submit">确认修改</Button>
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}
export default Form.create()(CollectionCreateForm)
