import React from 'react'
import {Form, Input, Button,Select, Radio,message} from 'antd';
import HTTPUtil from "../util/HTTPUtil";

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 14},
};

class msgTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exceptionMessagePrompt:[]
        }
    }

    componentDidMount() {
        let me = this;
        //获取异常消息提示
        HTTPUtil.get("/snail/attendenceMongo/findAttendaceMessage").then((data)=>{
            if(data.code==200){
                me.setState({
                    exceptionMessagePrompt:data.data
                })
            }
        })
    }

    //消息类型下拉选改变 设置消息提示
    handleSelectChange=(selectValue)=>{
        console.log("selectValue",selectValue);
        let msgPrompt = this.getMsgPromptByMsgType(selectValue);
        console.log("msgPrompt",msgPrompt);

        this.props.form.setFieldsValue({
            messageType:selectValue,
            content:msgPrompt
        })
    };


    //根据消息类型获取消息提示
    getMsgPromptByMsgType=(msgType)=>{
        let exceptionMessagePrompt = this.state.exceptionMessagePrompt;
        for(let i=0,length=exceptionMessagePrompt.length;i<length;i++){
            if(msgType==exceptionMessagePrompt[i].messageType){
                return exceptionMessagePrompt[i].content;
            }
        }
    };

    //表单提交
    handleSubmit=()=>{
        let param  = [];
        let formData = this.props.form.getFieldsValue();
        param.push(formData);
        HTTPUtil.post("/snail/attendenceMongo/saveAttendaceMessage",JSON.stringify(param),{"Content-Type":"application/json"}).then((data)=>{
            if(data.code==200){
                message.info("保存成功");
            }
        })
    };

    render() {
        const {getFieldProps} = this.props.form;
        let me = this;
        return (
            <div>
                <Form horizontal onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label="消息类型"
                    >
                        <Select id="select" size="large" defaultValue="1" style={{ width: 200 }}  {...getFieldProps('messageType')} onChange={me.handleSelectChange}>
                            <Option value="1">工作小于9小时</Option>
                            <Option value="2">忘记打卡</Option>
                            <Option value="3">事假</Option>
                        </Select>
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="消息提示"
                    >
                        <Input type="textarea" placeholder="编辑" {...getFieldProps('content', {initialValue: ''})} />
                    </FormItem>
                    <FormItem wrapperCol={{span: 16, offset: 6}} style={{marginTop: 24}}>
                        <Button type="primary" htmlType="submit">确定</Button>
                    </FormItem>
                </Form>
            </div>
        );
    }
}

const MessageTemplate = Form.create()(msgTemplate);
export default MessageTemplate;
