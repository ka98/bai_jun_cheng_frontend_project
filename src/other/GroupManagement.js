import React from 'react';
import {Button,Popconfirm,Input,Table,message,Select,LocaleProvider } from 'antd';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import jQuery from 'jquery';
import zhCN from 'antd/lib/locale-provider/zh_CN';

//引入子组件
 import groupDetails from "./GroupManagement/GroupDetails";
import NewGroup from "./GroupManagement/newGroup";
import NewGroups from "./groupManagement/NewGroups";
import HTTPUtil from "../util/HTTPUtil";
const InputGroup=Input.Group;
const Option = Select.Option;
const Search = Input.Search;
const activeKey="2";
const columns=[{
    title:'序号',
    dataIndex:'id',
    render:(text,record,index)=>{
        return <span>{index+1}</span>
    }
},{
    title:'群名称',
    dataIndex:'groupName',
    render:(text,record)=>{
        const egId=record.groupType;
        return (<a><Link to={'/groupDetails/'+ record.id+"/false"}>{text}</Link></a>)
    }
},{
    title:'hookUrl',
    dataIndex:'hookUrl'
},{
    title:'操作',
    dataIndex:'',
    render:(text,record,index)=>{
        console.log("activeKey:",activeKey);
        return(<div>
            <a className="baocun">
                <Link to={{pathname:"/groupDetails/"+ record.id+"/false"}}>群员管理</Link>
            </a>
            <a style={{display: text.groupType==2 ? "none" : "left"}}>
                <Link to={{pathname:"/groupDetails"+ record.id+"/false"}}>修改</Link>
            </a>
        </div>)
    }
}];

export default class GroupManagement extends React.Component{
    constructor(props){
        super(props);
        this.state={
            ttDate:[],
            empIdList:[],
            id:[],
            loading:false,    //是否正在加载中
            groupName:[] ,  //搜索框测试(群名称)
            groupType:2,    //搜索框测试(群组分类的类型)
            selectedRows:[]     //选项框选中的人员
        }
    }
    //table数据
    fetchFn=(url,param)=>{
        fetch(url,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method:'post',
            body:JSON.stringify(param)
        }).then((response) => {
            loading:true;
            if(response.status!==200){
                message.error("获取数据失败！");
                return;
            }
            //检查响应文本
            response.json().then((data) => {
                this.setState({
                    ttDate : data.data.pageInfo.list
                });
                this.setState({loading:false});
            })
        }).catch((e) => {
            message.error("获取数据失败！");
            console.log("e:",e)
        })
    };
    componentDidMount() {
        console.log("this.state.groupType:",this.state.groupType);
        const param={};
        param.groupType = this.state.groupType;
        console.log("param:",param);
        this.setState({loading:true});
        this.fetchFn("/snail/group/findAllGroup",jQuery.param(param));
    }

    // checkbox状态(选框)
    onSelectChange = (selectedRowKeys, selectedRows) => {
        this.setState({
            selectedRows:selectedRows
        });
        for (let i of selectedRows) {
            const arr=[i];
            console.log("123:",arr)
        }
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({ selectedRowKeys });
        for(var i=0;i<selectedRows.length;i++){
            console.log("selectedRowKeys[i]",selectedRows[i]);
            console.log("selectedRowKeys[i][empId]",selectedRows[i]["empId"]);
            const aa=this.state.empIdList.push(selectedRows[i]["empId"]);
            console.log("aa:",aa)
        }
    };

    //删除
    deleteGroup=()=>{
        let me = this;
        let paramArr = [];
        let dataArr = this.state.selectedRows;
        for(var i=0;i<dataArr.length;i++){
            paramArr.push(dataArr[i].id);
        }
        HTTPUtil.post("/snail/group/deleteGroups",JSON.stringify(paramArr),{'Content-Type': 'application/json'}).then((data)=>{
            message.success("删除成功！");
            me.fetchFn("/snail/group/findAllGroup",{ });
        }).catch((e)=>{
            message.error("删除失败！");
        });
    };

    //快速搜索
    handleSearch=(value)=>{
        let param={};
        param.groupType = this.state.groupType;
        param.groupName=value;
        console.log("1111:",param);
        HTTPUtil.post("/snail/group/findAllGroup",jQuery.param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data)=>{
            this.setState({
                ttDate : data.data.pageInfo.list
            });
            loading:false;
        })
    };

    //下拉选改变
    handleSelectChange=(value)=>{
        let me = this;
        me.setState({
            groupType:value
        });
        HTTPUtil.post("/snail/group/findAllGroup",jQuery.param({groupType:value}),{'Content-Type':'application/x-www-form-urlencoded'}).then((data)=>{
            me.setState({
                ttDate: data.data.pageInfo.list
            })
        })
    };

    render(){
        var self=this;
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };

        //分页
        const pagination = {
            total: this.state.ttDate.length,
            showSizeChanger: true,
            onShowSizeChange(current, pageSize) {
            },
            onChange(current) {
            }
        };
        const total=pagination.total;
        return(
            <div>
                <div style={{width:"100%",height:"32px"}}>
                    {/*<NewGroup/>*/}
                    <NewGroups/>
                    <Popconfirm title="确定要删除吗？" onConfirm={self.deleteGroup}>
                        <Button type="danger" style={{float:"left"}}>删除</Button>
                    </Popconfirm>
                    <div
                        style={{float:"left",marginLeft:"16px"}}
                    >群组分类：
                        <Select
                            style={{width:120}}
                            defaultValue="2"
                            // placeholder="请选择.."
                            showArrow
                            onChange={self.handleSelectChange}>
                            <Option value="1">钉钉群发组</Option>
                            <Option value="2">钉钉内部群</Option>
                            <Option value="3">公司运营组</Option>
                        </Select>
                    </div>
                    <div className="search">
                        <Search
                            placeholder="请输入群名称"
                            onSearch={this.handleSearch}
                            className="search"
                        />
                    </div>
                </div>
                <LocaleProvider locale={zhCN}>
                    <Table
                        rowSelection={rowSelection}
                        dataSource={this.state.ttDate}
                        columns={columns}
                        pagination={pagination}
                        loading={this.state.loading}
                    />
                </LocaleProvider>
                <div>共{total}条数据</div>
            </div>
        )
    }
}
