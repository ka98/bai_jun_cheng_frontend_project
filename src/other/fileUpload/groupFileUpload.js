import React from "react";
import {Upload,Icon,Button,message} from 'antd';

export default class GroupMessageFileUpload extends React.Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }
    componentDidMount(){

    }
    render(){
        const props = {
            name: 'groupMessageSendFile',
            action: '/snail/attendence/groupMessageFileUpload',
            headers: {
                authorization: 'authorization-text',
            },
            onChange(info) {
                if (info.file.status !== 'uploading') {
                    console.log(info.file, info.fileList);
                }
                if (info.file.status === 'done') {
                    message.success(`${info.file.name} 上传成功。`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} 上传失败。`);
                }
            },
        };
        return(
            <Upload {...props}>
                <Button type="ghost">
                    <Icon type="upload" /> 点击上传
                </Button>
            </Upload>
        )
    }
}
