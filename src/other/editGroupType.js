import React, { Component } from 'react'
import { Tabs } from 'antd'
import Form1 from './editGroupType/addTabForm.js'
import Form2 from './editGroupType/modifyTabForm.js'
import Form3 from './editGroupType/deleteTabForm.js'
// const FormItem = Form.Item
const TabPane = Tabs.TabPane;
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class editGroupType extends Component{
    constructor(props) {
        super(props)
        this.state = {
            groupMap: [],
            groupType: []
        }
        this.getGroupClass = this.getGroupClass.bind(this)
        this.findAllGroup = this.findAllGroup.bind(this)
    }
    handleSubmit() {
        e.preventDefault()
    }
    componentDidMount() {
        // this.findAllGroup()
        this.getGroupClass()
    }
    findAllGroup() {
        fetch('/snail/group/findAllGroup', {
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(
            res => {
                return res.json()
            }
        ).then(data => {
            const groupMap = data.data.pageInfo.list
            this.setState({
                groupMap
            },console.log(this.state.groupMap))
        })
    }
    getGroupClass() {
        fetch('/snail/group/findGroupClassification', {
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(
            res => res.json()
        ).then(data => {
            let groupType = [], list = data.data
            for(let key in list){
                groupType.push({group: list[key], name: key})
            }
            this.setState({
                groupType
            })
        })
    }
    render() {
        const { groupType } = this.state
        const { groupMap } = this.props
        return (
            <Tabs defaultActiveKey="1">
                <TabPane tab="新增" key="1">
                    <Form1 groupMap={groupMap} update={this.getGroupClass}/>
                </TabPane>
                <TabPane tab="修改" key="2">
                    <Form2 groupMap={groupMap} groupType={groupType} update={this.getGroupClass}/>
                </TabPane>
                <TabPane tab="删除" key="3">
                    <Form3 groupType={groupType}  groupMap={groupMap} update={this.getGroupClass}/>
                </TabPane>
            </Tabs>
        )
    }
}
export default editGroupType
