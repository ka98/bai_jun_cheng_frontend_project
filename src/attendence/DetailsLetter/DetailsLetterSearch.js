import React from "react";
import { Form, Row, Col, Button, Input, DatePicker ,Select} from "antd";
import Param from "util/transParam";
import HTTPUtil from "util/HTTPUtil";
const FormItem=Form.Item;
const Option = Select.Option;
const { MonthPicker} = DatePicker;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    }
};
let timeout;
let currentValue;
export default class DetailsLetterSearch extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSearch:[],  //获取部门的数据
            empId:null,    //部门id
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }
    handleSearch = (e) => {
        e.preventDefault();
        this.props.form.getFieldValue((values) => {
            console.log('Received values of form(DetailsLetterSearch): ', values);
            const param = {
                ...values,
                'deptId': values['deptId'] ? values['deptId'] : 0,
                "ymDate" : values['ymDate'] ? values['ymDate'].format('YYYY-MM') : ''
            };
            this.handleLetterFn("/snail/....",param)
        });
    };
    handleLetterFn (url,param) {
        fetch(url,{
            headers:{
                'Content-Type':'application/x-www-form-urlencoded'
            },
            method:'post',
            body:JSON.stringify(param),
        }).then(response => {
            return response.json()
        }).then(data => {
            console.log(data)
        }).catch(error => {
            console.log(error)
        })
    }
    //重置
    handleReset = () => {
        this.props.form.resetFields();
    };
    handleOnSearch = (state) => {
        console.log(state);
        const param = {};
        param.deptName = event;
        this.fetchsSearch(param);
    };
    fetchsSearch = (param) => {
        const me = this;
        if(timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        currentValue = param;
        function fake() {
            HTTPUtil.post("/snail/wxLeave/DetailsLetter",Param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
                console.log(data);
                me.setState({
                    dataSearch:data.data
                })
            }).catch(error => { console.log(error)})
        }
        timeout = setTimeout(fake, 1000);
    };
    handleOnChange = () => {
        this.setState({empId:data})
    };
    render(){
        const { getFieldDecorator } = this.props.form;
        const options = this.state.dataSearch.map(d => <Option key={d.id}>{d.name}</Option>);
        return(
            <Form onSubmit={this.handleSearch} className="form">
                <Row gutter={16} className="eyebrows">
                    <Col span={24}>
                        <Button type="primary" htmlType="submit">搜索</Button>
                        <Button className="cz" onClick={this.handleReset}>重置</Button>
                    </Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-between">
                    <Col span={8}>
                        <FormItem label="部门" {...formItemLayout}>
                            {getFieldDecorator("departMent")(
                                <Select
                                    showSearch
                                    showArrow={false}
                                    filterOption={false}
                                    defaultActiveFirstOption={false}
                                    onSearch = {this.handleOnSearch}
                                    onChange = {this.handleOnChange}
                                    notFoundContent={null}
                                >{options}</Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem label="员工姓名" {...formItemLayout}>
                            {getFieldDecorator("employeeName")(
                                <Input />
                            )}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem label="请假日期" {...formItemLayout}>
                            {getFieldDecorator("leaveDate")(
                                <MonthPicker
                                    placeholder="选择月份"
                                />
                            )}
                        </FormItem>
                    </Col>
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="岗位" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("comId")(*/}
                                {/*<Input />*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                </Row>
                {/*<Row gutter={16}>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="考勤日期" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("beginDate")(*/}
                                {/*<MonthPicker onChange={this.onChangeRangePicker} placeholder="选择月份" />*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="111" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("aaa")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="222" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("bbb")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="333" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("ccc")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                {/*</Row>*/}
            </Form>
        )
    }
}
