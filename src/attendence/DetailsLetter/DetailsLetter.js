import React from "react";
import { Table, Form, Input } from "antd";
import DetailsLetterSearch from "./DetailsLetterSearch";
import Param from "util/transParam";
import HTTPUtil from "util/HTTPUtil";
const Search = Input.Search;
const WrappedDetailsLetterSearch = Form.create()(DetailsLetterSearch);
const columns = [{
    title: '序号',
    render: (text, record, index) => {
        return <span>{index + 1}</span>
    }
},{
    title: '姓名',
    dataIndex: 'employeeName',
    key: 'employeeName',
},{
    title:'外包工号',
    dataIndex: 'aliNumber',
    key: 'aliNumber'
},{
    title:'部门',
    dataIndex:'department',
    key: 'department'
}, {
    title: '请假类型',
    dataIndex: 'leaveType',
    key: 'leaveType'
}, {
    title: '请假日期',
    dataIndex: 'leaveDate',
    key: 'leaveDate'
}, {
    title: '实际小时数（h）',
    dataIndex: 'realHour',
    key: 'realHour'
}, {
    title: '状态',
    dataIndex: 'status',
    key: 'status'
}, {
    title: '备注',
    dataIndex: 'comment',
    key: 'comment'
}];
export default class DetailsLetter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],     //获取的数据
            loading:false,
            isDisabled: true,   //高级搜索按钮切换
        }
    }
    componentDidMount () {
        this.setState({loading:true});
        this.LetterFen("/snail/wxLeave/DetailsLetter")
    }
    LetterFen = (url,param) => {
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body:JSON.stringify(param)
        }).then( (response) => {
            return response.json()
        }).then( (state) => {
            // console.log(state)
            this.setState({dataSource:state.data,loading:false})
        }).catch( (error) => {
             message.error("查询失败！");
            console.log(error)
        })
    };
    aClick = () => {
        const { expand } = this.state;
        this.setState({ expand: !expand });
        this.setState({
            isDisabled: false
        });
        // this.fetchClick("/snail/")
    };
    // fetchClick (url,param) {
    //     fetch(url,{
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         },
    //         method:'post'
    //     }).then(response => {
    //         return response.json()
    //     }).then(data => {
    //         console.log(data)
    //     }).catch(error => {
    //         console.log(error)
    //     })
    // }
    aaClick = (state) => {
        console.log("state:",state);
        this.setState({isDisabled:true})
    };
    handleSearch = (state) => {
        console.log(state);
        let me = this;
        let param = {};
        param.employeeName = state;
        me.setState({
            loading:true
        });
        console.log(param);
        HTTPUtil.post("/snail/wxLeave/DetailsLetter",Param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
            me.setState({
                dataSource:data.data,
                loading:false
            })
        }).catch(error => {
            message.error("获取数据失败！");
            console.log(error)
        })
    };
    render(){
        return(
            <div>
                <div
                    style={{display: this.state.isDisabled ? "none" : "block"}}
                     className="wrapped"
                >
                    <WrappedDetailsLetterSearch wrappedComponentRef={(form) => this.form = form}/>
                </div>
                <a onClick={this.aClick} style={{display: !this.state.isDisabled ? "none" : "block",float:"left"}}>高级搜索</a>
                <a onClick={this.aaClick} style={{display: this.state.isDisabled ? "none" : "block",float:"left"}}>隐藏搜索</a>
                <div className="search">
                    <Search
                        placeholder="请输入群名称"
                        onSearch={this.handleSearch}
                        className="search"
                        style={{display: !this.state.isDisabled ? "none" : "block"}}
                    />
                </div>
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    loading={this.state.loading}
                    className="clear"
                />
            </div>
        )
    }
}
