import React from 'react';
import {Row, Col, Table, Input, Button, DatePicker, LocaleProvider, Collapse} from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import 'moment/src/locale/zh-cn';
import {Link} from 'react-router-dom';
import HTTPUtil from "../../util/HTTPUtil";
import jQuery from 'jquery';
import {message} from "antd/lib/index";
const Panel = Collapse.Panel;
const {MonthPicker, RangePicker, WeekPicker} = DatePicker;

const columns = [{
    title: '序号',
    key: 'number',
    width: 100,
    align: "center",
    render: (text, record, index) => {
        return index + 1;
    }
}, {
    title: '当月在岗周期',
    align: "center",
    dataIndex: 'jobPeriod',
    width: 100,
}, {
    title: 'PO编号',
    align: "center",
    width: 100,
}, {
    title: '当月法定工作日',
    dataIndex: 'legalWorkingDay',
    key: 'legalWorkingDay',
    align: "center",
    width: 100,
}, {
    title: '实际出勤天数',
    dataIndex: 'attendanceAays',
    align: "center",
    key: 'attendanceAays',
    width: 100,
}, {
    title: '加班(小时)',
    children: [{
        title: '工作日',
        dataIndex: 'weekOverHour',
        align: "center",
        key: 'weekOverHour',
        width: 100,
    }, {
        title: '周末',
        dataIndex: 'weekendOverHour',
        align: "center",
        key: 'weekendOverHour',
        width: 100,
    }, {
        title: '法定节假日',
        dataIndex: 'vocationOverHour',
        align: "center",
        key: 'vocationOverHour',
        width: 100,
    },],
}, {
    title: '调休(小时)',
    dataIndex: 'overHour',
    align: "center",
    key: 'overHour',
    width: 100,
},{
    title: '微信事假(小时)',
    dataIndex: 'wxLeaveHour',
    align: "center",
    key: 'wxLeaveHour',
    width: 100,
},  {
    title: '不可调休的加班(小时)',
    children: [{
        title: '工作日',
        dataIndex: 'weekOverHourApproval',
        align: "center",
        key: 'weekOverHour',
        width: 100
    }, {
        title: '周末',
        dataIndex: 'weekendOverHourApproval',
        align: "center",
        key: 'weekendOverHour',
        width: 100
    }, {
        title: '法定节假日',
        dataIndex: 'vocationOverHourApproval',
        align: "center",
        key: 'vocationOverHour',
        width: 100
    }]
}];

const column_detail = [
    {
        title: '序号',
        key: 'number',
        width: 100,
        align: "center",
        render: (text, record, index) => {
            return index + 1;
        }
    },
    {
        title: '日期',
        align: "center",
        dataIndex: "attendanceDate",
        key: 'attendanceDate',
        width: 100
    },
    {
        title: '上班打卡时间',
        align: "center",
        dataIndex: "punchTime1",
        key: 'punchTime1',
        width: 100
    },
    {
        title: '下班打卡时间',
        align: "center",
        dataIndex: "punchTime2",
        key: 'punchTime2',
        width: 100
    },
    {
        title: '时长',
        align: "center",
        dataIndex: "duration",
        key: 'duration',
        width: 100
    },
    {
        title: '状态',
        align: "center",
        dataIndex: "state",
        key: 'approval',
        width: 100
    }
];
export default class AccountingAttendance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [{}],
            data_detail: [{}],
            loading: false,   //正在加载中
            detailLoading: false //查询日期的时候明细考勤加载效果
        }
    }

    componentDidMount() {
        let me = this;
        this.setState({
            loading: true,
            detailLoading: true
        });
        console.log(this.props)
        let empId = this.props.empId ? this.props.empId : parseInt(this.props.match.params.empId);
        HTTPUtil.post('/snail/attendence/findAttendanceCheck', jQuery.param({empId: empId}), {'Content-Type': 'application/x-www-form-urlencoded'}).then((data) => {
            if (data.code == 200) {
                me.setState({
                    data: data.data,
                    loading: false
                }, () => {
                    console.log(this.state)
                });
                message.info(data.message);
            } else {
                message.error("考勤核算查询失败:" + data.code);
            }
        });
        HTTPUtil.post('/snail/attendence/findAttendanceDetail', jQuery.param({empId: empId}), {'Content-Type': 'application/x-www-form-urlencoded'}).then((data) => {
            if (data.code == 200) {
                me.setState({
                    data_detail: data.data,
                    detailLoading: false
                })
            } else {
                message.error("明细考勤查询失败:" + data.code);
            }
        })
    }

    onPickerChange = (date, dateString) => {
        let me = this;
        let param = {};
        let empId = this.props.empId ? this.props.empId : parseInt(this.props.match.params.empId);
        param.empId = empId;
        param.beginDate = dateString[0];
        param.endDate = dateString[1];
        console.log(date, dateString);
        me.setState({
            detailLoading: true
        });
        HTTPUtil.post('/snail/attendence/findAttendanceDetail', jQuery.param(param), {'Content-Type': 'application/x-www-form-urlencoded'}).then((data) => {
            if (data.code == 200) {
                me.setState({
                    data_detail: data.data,
                    detailLoading: false
                })
            }
        })

    };

    render() {
        let dataObj = this.state.data[0];
        return (
            <div>
                <div>
                    <Button type="primary" className="Accounting"><Link to="/attendance/AccountingAttendance/">返回</Link></Button>
                    <div className="Accounting">姓名:{this.props.empName ? this.props.empName : dataObj.employeeName}</div>
                    <div>工号:{this.props.empId ? this.props.empId : dataObj.aliNumber}</div>
                </div>
                <div className="clear"></div>
                <Collapse defaultActiveKey={['1']}>
                    <Panel header={
                        <Row>
                            <Col span={24}>
                                <span>考勤信息概括:</span>
                            </Col>
                        </Row>
                    } key="1">
                        <Table
                            columns={columns}
                            dataSource={this.state.data}
                            loading={this.state.loading}
                            bordered
                            size="middle"
                        />
                    </Panel>
                    <Panel header={
                        <Row>
                            <Col span={12}>
                                <span>明细考勤:</span>
                            </Col>
                            <Col span={12}>
                                <LocaleProvider  locale={zh_CN}>
                                    <RangePicker onChange={this.onPickerChange} locale="zh-cn"/>
                                </LocaleProvider>
                            </Col>
                        </Row>
                    } key="2">
                        <Table
                            columns={column_detail}
                            dataSource={this.state.data_detail}
                            loading={this.state.detailLoading}
                            bordered
                            size="middle"
                        />
                    </Panel>
                </Collapse>
            </div>
        )
    }
}
