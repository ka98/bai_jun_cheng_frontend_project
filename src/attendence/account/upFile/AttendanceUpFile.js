import React from 'react';
import {Upload,Button,Icon,message} from 'antd';
import '../css/AccountingAttendance.css';
export default class AttendanceUpFile extends React.Component {
    constructor(props) {
        super(props);
        this.state={

        }
    }
    componentDidMount() {

    }
    render() {
        const me = this;
        const props = {
            name: 'file',
            action: '/snail/attendence/prevAttendance',
            headers: {
                authorization: 'authorization-text',
                "Access-Control-Allow-Origin":"*"
            },
            data:{},
            supportServerRender:true,
            showUploadList:false,
            onChange (info) {
                me.props.setLoading(true);
                if (info.file.status !== 'uploading') {

                }
                if (info.file.status === 'done') {
                    let prevData = info.file.response.data;  //返回值
                    // me.props.setLoading(false);
                    // me.props.showModel(prevData);
                    me.props.findAttendanceCheck();
                } else if (info.file.status === 'error') {
                     message.error(`${info.file.name} file upload failed.`);
                    me.props.setLoading(false);
                }
            },
        };
        return (
            <div className="Accounting">
                <Upload {...props}>
                    <Button type="primary"><Icon type="upload" />导入</Button>
                </Upload>
            </div>
        )
    }
}
