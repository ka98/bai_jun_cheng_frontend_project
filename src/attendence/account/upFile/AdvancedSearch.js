import React from "react";
import {Form,Button,Row,Col,Input,DatePicker,Select } from "antd";
import HTTPUtil from "../../../util/HTTPUtil";
import Param from "util/transParam";
import 'moment/locale/zh-cn';
import {message} from "antd/lib/index";
const FormItem = Form.Item;
const Option = Select.Option;
const { MonthPicker } = DatePicker;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};
let timeout;
let currentValue;

export default class AdvancedSearch extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSearch:[],   //获取部门的数据
            empId:null,     //部门的id
        };
        this.handleSearch=this.handleSearch.bind(this);
        this.handleResetSearch=this.handleResetSearch.bind(this);
    }
    componentDidMount () {
        this.props.DataSearch(this)
    }
    //搜索（提交）
    handleSearch = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldsValue) => {
            console.log('Received values of form: ',err, fieldsValue);
            const yy = {
                ...fieldsValue,
                'deptId': fieldsValue['deptId'] ? fieldsValue['deptId'] :0,
                'ymDate': fieldsValue['ymDate'] ? fieldsValue['ymDate'].format('YYYY-MM') : ''
            };
            this.handleResetFn("/snail/attendence/findAttendanceCheck",yy);
        });
    };
    handleResetFn = (url,aa) => {
        const me=this;
        console.log(aa);
        HTTPUtil.post(url,Param(aa),{'Content-Type':'application/x-www-form-urlencoded'}).then( (data) => {
            // console.log(data);
            const emp = data.data;
            me.props.DataSearch(emp);   //将获取的数据传递到父组件中
            me.setState({
                loading:false
            })
        }).catch((e) => {
            console.log(e);
        })
    };
    //重置
    handleResetSearch = () => {
        this.props.form.resetFields();
    };
    //部门获取值
    handleOnSearch = (event) => {
        const param = {};
        param.deptName = event;
        this.fetchSearch(param);
    };
    fetchSearch(value) {
        const me = this;
        if(timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        currentValue = value;
        function fake() {
            HTTPUtil.post("/snail/department/exhibitDeptInfo",Param(value),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
                console.log(data);
                // data.data == '' ? '':
                me.setState({
                    dataSearch:data.data
                });
            }).catch(error => { console.log(error)})
        }
        timeout = setTimeout(fake, 1000);
    }
    //选取部门-id
    handleOnChange = (data) => {
        this.setState({empId:data})
    };
    render(){
        const { getFieldDecorator } = this.props.form;
        const options = this.state.dataSearch.map(d => <Option key={d.id}>{d.name}</Option>);
        return(
            <Form onSubmit={this.handleSearch} className="form">
                <Row gutter={16} className="eyebrows">
                    <Col span={24}>
                        <Button type="primary" htmlType="submit">搜索</Button>
                        <Button className="cz" onClick={this.handleResetSearch}>重置</Button>
                    </Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-between">
                    <Col span={6}>
                        <FormItem label="部门" {...formItemLayout}>
                            {getFieldDecorator("deptId")(
                                <Select
                                    showSearch
                                    showArrow={false}
                                    filterOption={false}
                                    defaultActiveFirstOption={false}
                                    onSearch = {this.handleOnSearch}
                                    onChange = {this.handleOnChange}
                                    notFoundContent={null}
                                >{options}</Select>
                            )}
                        </FormItem>
                    </Col>
                    {/*<Col xs={2}></Col>*/}
                    <Col span={6}>
                        <FormItem label="员工姓名" {...formItemLayout}>
                            {getFieldDecorator("employeeName")(
                                <Input />
                            )}
                        </FormItem>
                    </Col>
                    {/*<Col xs={2}></Col>*/}
                    <Col span={6}>
                        <FormItem label="考勤日期" {...formItemLayout}>
                            {getFieldDecorator("ymDate")(
                                <MonthPicker
                                    placeholder="选择月份"
                                />
                            )}
                        </FormItem>
                    </Col>
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="岗位" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("comId")(*/}
                                {/*<Input />*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                </Row>
                {/*<Row gutter={16}>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="证件号码" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("number")(*/}
                                {/*<Input />*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="111" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("aaa")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="222" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("bbb")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                    {/*<Col span={6}>*/}
                        {/*<FormItem label="333" {...formItemLayout}>*/}
                            {/*{getFieldDecorator("ccc")(*/}
                                {/*<Input/>*/}
                            {/*)}*/}
                        {/*</FormItem>*/}
                    {/*</Col>*/}
                {/*</Row>*/}
            </Form>
        )
    }
}
