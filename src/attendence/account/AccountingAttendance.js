import React from 'react';
import {Table,Input,Button,Icon,Row,Col,Modal,message,Form} from 'antd';
import {Link} from 'react-router-dom';
import HTTPUtil from "../../util/HTTPUtil";
import Param from "../../util/transParam";
import './css/AccountingAttendance.css';
import classNames from 'classnames';
import AttendanceUpFile from "./upFile/AttendanceUpFile";
import AdvancedSearch from "./upFile/AdvancedSearch"
const InputGroup = Input.Group;
const Search = Input.Search;
const WrappedAdvancedSearch =  Form.create()(AdvancedSearch);
export default class AccountingAttendance extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            daSource:[],
            previewSource:[], //预览数据
            modalVisible:false,
            loading:false,     //正在加载中
            expand: false,     //高级搜索框隐藏
            isDisabled: true,   //高级搜索按钮切换
            isHeight: "0px",
            search:[],     //设置输入框中得内容
        };
        this.aClick=this.aClick.bind(this);
        this.aaClick=this.aaClick.bind(this);
        // this.DataSearch=this.DataSearch.bind(this)
    }

    componentDidMount() {
      this.findAttendanceCheck();
    }

    //设置工作日数据
    weekVocation=(event,index)=>{
        console.log("event:",event);
        console.log("index",index);
    };

    //设置周末数据
    weekendVocation=(index)=>{

    };

    //设置法定节假日数据
    changeVocation=(index)=>{

    };

    //保存
    handleSave=()=>{
        let paramList = [];
        let data = this.state.data;
        for(let i=0;i<data.length;i++){
            var item = data[i];
            var obj = {};
            obj.empId = item.empId;
            obj.startCountTime = new Date((item.jobPeriod.split("~"))[0]);
            obj.endCountTime = new Date((item.jobPeriod.split("~"))[1]);
            obj.weekOverHour = item.weekOverHour;
            obj.weekendOverHour = item.weekendOverHour;
            obj.vocationOverHour = item.vocationOverHour;
            obj.weekOverHourApproval = item.weekOverHourApproval;
            obj.weekendOverHourApproval = item.weekendOverHourApproval;
            obj.vocationOverHourApproval = item.vocationOverHourApproval;
            paramList.push(obj);
        }
        console.log("data:",this.state.data);
        fetch("/snail/attendence/saveAttendanceCheck",{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(paramList)
        }).then((res) => {
            return res.json();
            //this.setState({ loading: true });    //正在加载中
        }).then((data) => {
            message.info(data.message);
        }).catch((e) => {

        })
    };
    //搜索功能
    handleSearch=(event)=>{
        console.log(event);
        let me = this;
        // me.setState({search:event});
        let param = {};
        param.employeeName = event;
        me.setState({
            loading:true
        });
        console.log(param);
        HTTPUtil.post("/snail/attendence/findAttendanceCheck",Param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
            me.setState({
                daSource:data.data,
                loading:false
            })
        }).catch(error => {console.log(error)})
    };

    findAttendanceCheck=()=>{
        let me = this;
        this.setState({
            loading:true
        });
        HTTPUtil.get("/snail/attendence/findAttendanceCheck").then((data)=>{
            console.log(data.data)
            me.setState({
                daSource:data.data,
                loading:false
            });
            // message.info(data.message);
        }).catch(error => {
            console.log(error);
            message.error("获取数据失败！")
        })
    };

    setLoading=(b)=>{
        this.setState({
            loading:b
        })
    };
    //弹出框确定按钮
    handleModalOk=()=>{
        const me = this;
        this.setState({
            modalVisible:false,
            loading:true
        });
        HTTPUtil.post("/snail/attendence/saveAttendance",JSON.stringify(me.state.previewSource),{'Content-Type': 'application/json'}).then((data)=>{
            // message.info(data.message);
            me.findAttendanceCheck();
        }).catch(error => {
            console.log(error);
            message.error("获取数据失败！")
        })
    };

    //弹出框取消按钮
    handleModalCancel=()=>{
        this.setState({
            modalVisible:false
        })
    };

    showModel=(data)=>{
        this.setState({
            modalVisible:true,
            previewSource:data
        })
    }

    // table 的样式
    rowClass=(record, index)=>{
        let rowClass = "";
        if(record.state==1){
            rowClass =  "abnormalAttendanceAdd"
        }else if(record.state==2){
            rowClass =  "noAttendanceDate"
        }
        return rowClass;
    };
    //a高级搜索
    aClick=(state)=>{
        console.log("state:",state);
        // this.setState({display:"block"})
        const { expand } = this.state;
        console.log("expand:",expand);
        this.setState({ expand: !expand });
        this.setState({
            isDisabled: false,
            isHeight:false
        })
        // this.fetchClick("/snail/department/exhibitDeptInfo")
    };
    // fetchClick = (url) => {
    //     fetch(url,{
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json'
    //         },
    //         method:'post'
    //     }).then((res) => {
    //         console.log("res:",res)
    //         return res.json();
    //         //this.setState({ loading: true });    //正在加载中
    //     }).then((data) => {
    //         console.log("data:",data)
    //         // message.info(data.message);
    //     }).catch((e) => {
    //         console.log("e:",e)
    //     })
    // };
    aaClick = (state) => {
        console.log("11:",state);
        this.setState({
            isDisabled: true,
            isHeight:true,
            // daSource:[]
        });
        console.log(this);
        console.log(this.refs.aa);
        console.log(this.child);
        this.child.handleResetSearch();
    };
    //高级搜索数据
    DataSearch = (data) => {
        this.setState({ loading:true });
        const aa = [];
        for(var i=0;i<data.length;i++){
            aa.push(data[i]);
        }
        this.setState({
            daSource:aa,
            loading:false
        });
        this.child = data;     //父组件调用子组件得方法（第二种）
        const per = this;
        if(this.state.search !== null){
            per.setState({search:[]})
        }
    };
    render() {
        let me = this;
        const btnCls = classNames({
            'ant-search-btn': true,
        });
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });
        const columns = [
            {
            title: '序号',
            dataIndex: 'name',
            key: 'name',
            width: 100,
            align:"center",
            render:(text, record, index)=> {
                return index+1;
            }
        },{
            title: '工号',
            dataIndex: 'aliNumber',
            key: 'aliNumber',
            align:"center",
            width: 100,
        }, {
            title: '姓名',
            dataIndex: 'employeeName',
            key: 'employeeName',
            align:"center",
            width: 100,
            render:(text, record, index)=> {
                return <a><Link to={"/attendanceDetail/"+record.empId} >{text}</Link></a>;
            }
        },{
            title: '部门',
            dataIndex: 'belongsDept',
            key: 'belongsDept',
            align: "center",
            width: 100,
        }, {
            title: '在岗周期',
            align:"center",
            dataIndex:'jobPeriod',
            width: 100,
        }, {
            title: '当月法定工作日',
            dataIndex:'legalWorkingDay',
            key:'legalWorkingDay',
            align:"center",
            width: 100,
        },{
            title: '实际出勤天数',
            dataIndex: 'attendanceAays',
            align:"center",
            key: 'attendanceAays',
            width: 100,
        },{
            title: '加班',
            children: [{
                title: '工作日',
                dataIndex: 'weekOverHour',
                align:"center",
                key: 'weekOverHour',
                width: 100,
            },{
                title: '周末',
                dataIndex: 'weekendOverHour',
                align:"center",
                key: 'weekendOverHour',
                width: 100,
            },{
                title: '法定节假日',
                dataIndex: 'vocationOverHour',
                align:"center",
                key: 'vocationOverHour',
                width: 100,
            },],
        },{
            title: '调休',
            dataIndex: 'overHour',
            align:"center",
            key: 'overHour',
            width: 100,
        },{
            title: '微信事假',
            dataIndex: 'wxLeaveHour',
            align:"center",
            key: 'wxLeaveHour',
            width: 100,
        },{
            title: '不可调休的加班',
            children: [{
                title: '工作日',
                dataIndex: 'weekOverHourApproval',
                align:"center",
                key: 'weekOverHourApproval',
                width: 100,
                render: (text, recode,index) => <Input  value={text} onChange={e => {
                    let tableData = me.state.data;
                    // update state
                    // 通过recode.id 与dataSource List中的id匹配, 匹配到后，直接修改，然后setState.
                    console.log(e);
                    tableData[index].weekOverHourApproval = e.target.value;
                    me.setState({
                        data:tableData
                    })

                }}/>
            },{
                title: '周末',
                dataIndex: 'weekendOverHourApproval',
                align:"center",
                key: 'weekendOverHourApproval',
                width: 100,
                render: (text, recode,index) => <Input value={text}  onChange={e => {
                    let tableData = me.state.data;
                    console.log(e);
                    // update state
                    // 通过recode.id 与dataSource List中的id匹配, 匹配到后，直接修改，然后setState.
                    tableData[index].weekendOverHourApproval = e.target.value;
                    me.setState({
                        data:tableData
                    })
                }}/>
            },{
                title: '法定节假日',
                dataIndex: 'vocationOverHourApproval',
                align:"center",
                key: 'vocationOverHourApproval',
                width: 100,
                render: (text, recode,index) => <Input  value={text} onChange={e => {
                    let tableData = me.state.data;
                    // update state
                    // 通过recode.id 与dataSource List中的id匹配, 匹配到后，直接修改，然后setState.
                    tableData[index].vocationOverHourApproval = e.target.value;
                    me.setState({
                        data:tableData
                    })
                }}/>
            }]
        },{
            title: '操作',
            align:"center",
            width: 100,
            render:(text, record, index)=> {
                return <Link to={"/attendanceDetail/"+record.empId} >考勤明细</Link>;
            }
        }];

        // const columns_preview = [
        //     {
        //         title: '序号',
        //         render: (text, record, index) => {
        //             return <span>{index + 1}</span>
        //         }
        //     }, {
        //         title: '姓名',
        //         dataIndex: 'employeeName',
        //     },{
        //         title: '外包工号',
        //         dataIndex: 'aliNumber',
        //     }, {
        //         title: '考勤日期',
        //         dataIndex: 'dateStr',
        //     },
        //     {
        //         title: '班次',
        //         dataIndex: 'order',
        //     }, {
        //         title: '上班打卡时间',
        //         dataIndex: 'punchTime1',
        //     },{
        //         title: '上班打卡结果',
        //         dataIndex: 'punchComment1',
        //     },{
        //         title: '下班打卡时间',
        //         dataIndex: 'punchTime2',
        //     },{
        //         title: '下班打卡结果',
        //         dataIndex: 'punchComment2',
        //     },{
        //         title: '考勤审批',
        //         dataIndex: 'approval',
        //     }];


        return (
            <div>
                <div style={{
                     display: this.state.isDisabled ? "none" : "block",
                     // height: this.state.isHeight ? "0px " : "100%",
                     // animation: "mymove 5s",
                    }}
                     className="wrapped"
                >
                    <WrappedAdvancedSearch
                        ref="aa"   //父组件调用子组件得方法（第一种）
                        wrappedComponentRef={(form) => this.form = form} DataSearch={this.DataSearch}/>
                </div>
                <div>
                    {/*<AttendanceUpFile findAttendanceCheck={this.findAttendanceCheck} setLoading={this.setLoading} showModel={this.showModel}/>*/}
                    <Button className="Accounting" type="primary" onClick={this.handleSave}>保存</Button>
                    <div>
                        <a onClick={this.aClick} style={{display: !this.state.isDisabled ? "none" : "block",float:"left"}}>高级搜索</a>
                        <a onClick={this.aaClick}  style={{display: this.state.isDisabled ? "none" : "block",float:"left"}}>隐藏搜索</a>
                    </div>
                    <div className="search">
                        <Search
                            placeholder="请输入员工姓名"
                            onSearch={this.handleSearch}
                            className="search"
                            style={{display: !this.state.isDisabled ? "none" : "block"}}
                        />
                    </div>
                </div>
                <Table
                    columns={columns}
                    dataSource={this.state.daSource}
                    loading={this.state.loading}
                    // bordered
                    size="middle"
                    className="clear"
                />
                {/*<Modal title="异常考勤" visible={this.state.modalVisible}*/}
                       {/*onOk={this.handleModalOk} onCancel={this.handleModalCancel} width='100%'*/}
                {/*>*/}
                    {/*<Table*/}
                        {/*dataSource={this.state.previewSource}*/}
                        {/*bordered*/}
                        {/*columns={columns_preview}*/}
                        {/*rowClassName={this.rowClass}*/}
                        {/*loading={this.state.loading}*/}
                    {/*/>*/}
                    {/*<div><span style={{display:"inline-block",width:"10px",height:"10px",backgroundColor:"rgba(0,255,0,.3)"}}></span>新增考勤</div>*/}
                    {/*<div><span style={{display:"inline-block",width:"10px",height:"10px",backgroundColor:"rgba(255,0,0,.3)"}}></span>无考勤日期</div>*/}
                {/*</Modal>*/}
            </div>
        )
    }
}
