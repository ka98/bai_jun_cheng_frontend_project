import React from "react";
import {Table} from "antd";
const columns = [{
    title: '序号',
    render: (text, record, index) => {
        return <span>{index + 1}</span>
    }
},{
    title: '姓名',
    dataIndex: 'employeeName',
},{
    title:'外包工号',
    dataIndex: 'aliNumber',
},{
    title:'部门',
    dataIndex:'department',
}, {
    title: '请假类型',
    dataIndex: 'leaveType',
}, {
    title: '请假日期',
    dataIndex: 'approval',
}, {
    title: '实际小时数（h）',
    dataIndex: 'realHour',
}, {
    title: '状态',
    dataIndex: 'status',
}, {
    title: '备注',
    dataIndex: 'comment',
}];
export default class OvertimeRecords extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],
            loading:false,
        }
    }
    componentDidMount () {
        this.OvertimeRecordsFen("/snail/");
    }
    OvertimeRecordsFen = (url,param) => {
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body:JSON.stringify(param)
        }).then((response) => {
            return response.json()
        }).then((state) => {
            console.log(state)
        }).catch((e) => {
            console.log(e)
        })
    };
    render(){
        return(
            <Table
                columns={columns}
                dataSource={this.state.dataSource}
                loading={this.state.loading}
            />
        )
    }
}
