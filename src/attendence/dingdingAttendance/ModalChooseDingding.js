import React from "react";
import {Modal} from "antd";
import {Link} from "react-router-dom";
export default class ModalChooseDingding extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const { visible, onCancel, onCreate} = this.props;
        return(
            <Modal
                title="选择查看位置"
                visible={visible}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <p>钉钉详情：<Link to="/attendance/DetailsDingding">钉钉详情</Link></p>
                <p>核算考勤：<Link to="/attendance/AccountingAttendance">核算考勤</Link></p>
            </Modal>
        )
    }
}
