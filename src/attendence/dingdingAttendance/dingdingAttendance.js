import React from "react";
import {Upload,Button,Icon,Table,message} from 'antd';
import {Link} from "react-router-dom";
import DetailsDingding from "../DetailsDingding/DetailsDingding";
import ModalUpFileDingding from "./ModalUpFileDingding";
import ModalChooseDingding from "./ModalChooseDingding"
const columns = [
    {
        title: '序号',
        render: (text, record, index) => {
            return <span>{index + 1}</span>
        }
    },{
        title: '文件名称',
        dataIndex: '',
        render:() => {
            return(
                <Link to="/attendance/DetailsDingding"/>
            )
        }
    },{
        title: '上传时间',
        dataIndex: ''
    }
];
export default class DingdingAttendance extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],     //全部数据
            loading:false,     //是否正在加载中
            visible:false,
            visibleChoose:false,
            dataData:[]
        }
        this.handleOnCancel = this.handleOnCancel.bind(this);
        this.handleOnCreate = this.handleOnCreate.bind(this);
        this.handleOnCancelChoose = this.handleOnCancelChoose.bind(this);
        this.handleOnCreateChoose = this.handleOnCreateChoose.bind(this);
    }
    handleOnCancel = () => {
        this.setState({visible:false})
    };
    handleOnCreate = () => {
        console.log(this.state.dataData);
        this.setState({visible:false,visibleChoose:true})
        fetch("/snail/attendence/saveAttendance",{
            headers: {
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(this.state.dataData)
        }).then(response => {
            return response.json()
        }).then(state => {
            if( state.code === 200){
                message.info("保存成功！");
                console.log("111:",state)
                // this.setState({dataSource:state})
            }
            console.log(state)
        }).catch(error => {
            console.log(error)
        });
    };

    //选择弹出框
    handleOnCancelChoose = () => {
        this.setState({visibleChoose:false})
    };
    handleOnCreateChoose = (b) => {
        console.log(b);
        this.setState({visibleChoose:false})
    };
    render(){
        const me=this;
        const props = {
            name: 'file',
            action: '/snail/attendence/prevAttendance',
            headers: {
                authorization: 'authorization-text',
            },
            onChange(info) {
                if (info.file.status !== 'uploading') {
                    console.log(info.file, info.fileList);
                    if (info.file.response.code === 200) {
                        me.setState({
                            dataData : info.file.response.data
                        })
                    }
                }
                if (info.file.status === 'done') {
                    me.setState({visible:true});
                    me.refs.aa.ModalDingdingFentch();
                    message.success(`${info.file.name} file uploaded successfully`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} file upload failed.`);
                }
            }
        };
        return(
            <div>
                <Upload {...props}>
                    <Button type="primary"><Icon type="upload"/>导入</Button>
                </Upload>
                <ModalUpFileDingding
                    visible={this.state.visible}
                    onCancel={this.handleOnCancel}
                    onCreate={this.handleOnCreate}
                    dataData={this.state.dataData}
                    ref="aa"
                />
                <ModalChooseDingding
                    visible={this.state.visibleChoose}
                    onCancel={this.handleOnCancelChoose}
                    onCreate={this.handleOnCreateChoose}
                />
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    loading={this.state.loading}
                />
            </div>
        )
    }
}
