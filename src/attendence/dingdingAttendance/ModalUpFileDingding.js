import React from "react";
import {Modal,Table,message} from "antd";
import HTTPUtil from "util/HTTPUtil";
const columns=[{
    title: '序号',
    render: (text, record, index) => {
        return <span>{index + 1}</span>
    }
},{
    title: '姓名',
    dataIndex: 'employeeName',
},{
    title:'外包工号',
    dataIndex: 'aliNumber',
},{
    title:'部门',
    dataIndex:'department',
},{
    title: '班次',
    dataIndex: 'order',
}, {
    title: '请假日期(明细)',
    dataIndex: 'approval',
}, {
    title: '工作时长（分钟）',
    dataIndex: 'attendenceMin',
},{
    title: '出勤天数',
    dataIndex: 'attendanceDay',
},{
    title: '休息天数',
    dataIndex: 'freeDay'
}, {
    title: '状态',
    dataIndex: 'status',
}];
export default class ModalUpFileDingding extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],    //总数据
            loading:false,     //正在加载
            pagination:[],     //分页
        }
    }
    ModalDingdingFentch = () => {
        console.log(this.props);
        this.setState({dataSource:this.props.dataData});
        const param = [];
        param.dataData=this.props.dataData;
        console.log(param);
        // fetch("/snail/attendence/saveAttendance",{
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     method:'post',
        //     body:JSON.stringify(this.props.dataData)
        // }).then(response => {
        //     return response.json()
        // }).then(state => {
        //     if( state.code === 200){
        //         console.log("111:",state)
        //         // this.setState({dataSource:state})
        //     }
        //     console.log(state)
        // }).catch(error => {
        //     console.log(error)
        // });
    };
    render(){
        const {visible,onCancel,onCreate} = this.props;
        return(
            <Modal
                title="预览"
                visible={visible}
                onCancel={onCancel}
                onOk={onCreate}
                // wrapClassName="vertical-center-modal"
                width='100%'
            >
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    loading={this.state.loading}
                    pagination={this.state.pagination}
                />
            </Modal>
        )
    }
}
