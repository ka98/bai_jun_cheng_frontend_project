import React from 'react';
import {Upload, Button, Icon, Table, message,Spin} from 'antd';
import {Link} from "react-router-dom";
import jQuery from "jquery";
import HTTPUtil from "../../util/HTTPUtil";
import DetailsLetter from "../DetailsLetter/DetailsLetter";
//子组件
import ModalUpFile from "./ModalUpFile";
import ModalChoose from "./ModalChoose";
export default class Affair extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],   //显示table的数据
            pagination:[],   //分页
            abnormalMap:{},  //发送给后台的map
            sendBtnVisi:false, //发送按钮是否显示
            prevData:[],
            aDisplay:[],    //矫正考勤按钮是否显示
            jDisplay:[],     //按钮切换
            loading:false,    //正在加载中
            upFile:true,      //上传成功后出现预览页面
            dataUpFile:[],     //传到预览表中的
            visible:false,     //弹出框的显示与否
            visibleChoose:false,   //xuanzetanchukuang
        }
        this.handleOnCancel = this.handleOnCancel.bind(this);
        this.handleOnCreate = this.handleOnCreate.bind(this);
    }
    componentDidMount () {
        HTTPUtil.post("/snail/wxLeave/findWXFile",JSON.stringify(this.state.pagination),{'Content-Type': 'application/json'}).then((data)=>{
            if(data.code == 200) {
                // console.log(data);
                this.setState({dataSource:data.data})
            }
        })
    }
    correctionNormalAttendance = (r) => {
        console.log(r);
        const isCorrect=this.record;
        this.setState({
            aDisplay:true
        });
        this.setState({
            jDisplay:false
        })
    };

    //钉钉群消息发送：
    handleSend = () => {
        let data = this.state.dataSource;
        for(var i=0;i<data.length;i++){
            data[i].abnormalityOfAttendanceDescribe=data[i].approval
        }
        HTTPUtil.post("/snail/attendence/sendMessage",JSON.stringify(data),{"Content-Type":"application/json"}).then((data)=>{
            if(data.code == 200) {
                message.info(data.message);
            }
        })
    };

    //下载Excel
    fetchFnGroup = (url,fileName) => {
        console.log(fileName);
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body:jQuery.param(fileName)
        }).then((response) => {
            return response.blob();
        }).then((blob) => {
            console.log(blob);
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = url;
            a.download = "filename.xlsx";
            a.click();
            // console.log("download222:",this.a.download);
        }).catch((e) => {
            console.log("下载失败的原因:",e);
        })
    };
    handleDownLoad = (fileName,aa) => {
        console.log(fileName,aa);
        const parama = {};
        parama.fileName = fileName ;
        console.log(parama);
        this.fetchFnGroup("/snail/wxLeave/attendanceDownLoad ",parama);
    };

    //弹出框取消
    handleOnCancel = (s) => {
        this.setState({visible:false})
    };
    //弹出框确定
    handleOnCreate = (a) => {
        console.log(a);
        const param = [];
        this.setState({visible:false,visibleChoose:true})
        fetch("/snail/wxLeave/saveAttendenceWX",{
            headers: {
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(this.state.dataUpFile)
        }).then(response => {
            return response.json()
        }).then(state => {
            if( state.code === 200){
                console.log("111:",state)
                // this.setState({dataSource:state})
            }
            console.log(state)
        }).catch(error => {
            console.log(error)
        });
    };

    //选择弹出框
    handleOnCancelChoose = () => {
        this.setState({visibleChoose:false})
    };
    handleOnCreateChoose = (b) => {
        console.log(b);
        this.setState({visibleChoose:false})
    };
    render(){
        let me = this;
        //需要点击矫正按钮传参
        const columns = [
            {
                title: '序号',
                render: (text, record, index) => {
                    return <span>{index + 1}</span>
                },
                key: 'num'
            },{
                title: '文件名称',
                dataIndex: 'fileName',
                render:(text, record, index) => {
                    // console.log(text, record, index)
                    return(
                        <a onClick={me.handleDownLoad.bind(me,text)}>{text}</a>
                    )
                },
                key: 'fileName'
            },{
                title: '上传时间',
                dataIndex: 'upTime',
                key: 'upTime',
            }];
        //上传考勤excel
        //需要设置数据
        const props = {
            name: 'file',
            action: '/snail/wxLeave/previewAttendenceWX ',
            headers: {
                authorization: 'authorization-text',
                "Access-Control-Allow-Origin": "*"
            },
            showUploadList:false,
            // data:JSON.stringify(data),
            supportServerRender: "true",
            onChange(info) {
                console.log(info);
                if (info.file.status !== 'uploading') {
                    let prevData = info.file.response.data;  //返回值
                    me.setState({
                        dataSource : prevData,
                        abnormalMap:prevData,
                        dataUpFile:prevData,
                        loading:true
                    });
                }
                if (info.file.status === 'done') {
                    message.success("上传成功！",10);
                    me.refs.af.ModalUpFileFetch();
                    me.setState({
                        loading:false,
                        sendBtnVisi:true,
                        visible:true
                    });
                } else if (info.file.status === 'error') {
                    message.error("上传失败！",10);
                    me.setState({
                        loading:false,
                        visible:false
                    });
                }
            },
        };
        return(
            <div>
                <Spin spinning={this.state.loading}></Spin>
                <ModalUpFile
                    visible={this.state.visible}
                    onCancel={this.handleOnCancel}
                    onCreate={this.handleOnCreate}
                    dataUpFile={this.state.dataUpFile}
                    ref="af"
                />
                <ModalChoose
                    visible={this.state.visibleChoose}
                    onCancel={this.handleOnCancelChoose}
                    onCreate={this.handleOnCreateChoose}
                />
                <Upload {...props} style={{marginRight:"16px"}}>
                    <Button type="primary"><Icon type="upload"/>导入</Button>
                </Upload>
                {/*<Button style={{display:this.state.sendBtnVisi?"block":"none"}} onClick={this.handleSend}>发送</Button>*/}
                {/*<Button onClick={this.handleDownLoad}>下载</Button>*/}
                <Table dataSource={this.state.dataSource} columns={columns} loading={this.state.loading} pagination={this.state.pagination}/>
            </div>
        )
    }
}
