import React from "react";
import {Modal,Table} from "antd";
const columns=[{
    title: '序号',
    render: (text, record, index) => {
        return <span>{index + 1}</span>
    }
},{
    title: '姓名',
    dataIndex: 'employeeName',
},{
    title:'外包工号',
    dataIndex: 'aliNumber',
},{
    title:'部门',
    dataIndex:'department',
}, {
    title: '请假类型',
    dataIndex: 'leaveType',
}, {
    title: '请假日期',
    dataIndex: 'approval',
}, {
    title: '实际小时数（h）',
    dataIndex: 'realHour',
}, {
    title: '状态',
    dataIndex: 'status',
}, {
    title: '备注',
    dataIndex: 'comment',
}]
export default class ModalUpFile extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],    //总数据
            loading:false,     //正在加载
        }
    }
    ModalUpFileFetch () {
        console.log(this.props.dataUpFile);
        this.setState({dataSource:this.props.dataUpFile});
        // const param = [];
        // param.dataUpFile=this.props.dataUpFile;
        // console.log(param);
        // fetch("/snail/wxLeave/saveAttendenceWX",{
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     method:'post',
        //     body:JSON.stringify(this.props.dataUpFile)
        // }).then(response => {
        //     return response.json()
        // }).then(state => {
        //     if( state.code === 200){
        //         console.log("111:",state)
        //         // this.setState({dataSource:state})
        //     }
        //     console.log(state)
        // }).catch(error => {
        //     console.log(error)
        // });
    }
    // componentDidMount(){
    //     console.log(this.props);
    //     // dataSource:this.props
    // }
    render(){
        const {visible,onCancel,onCreate} = this.props;
        return(
            <Modal
                title="预览"
                visible={visible}
                onCancel={onCancel}
                onOk={onCreate}
                // wrapClassName="vertical-center-modal"
                width='100%'
            >
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    loading={this.state.loading}
                />
            </Modal>
        )
    }
}
