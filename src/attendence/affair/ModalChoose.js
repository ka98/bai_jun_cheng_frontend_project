import React from "react";
import {Modal} from "antd";
import {Link} from "react-router-dom"
export default class ModalChoose extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const { visible, onCancel, onCreate} = this.props;
        return(
            <Modal
                title="选择查看位置"
                visible={visible}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <p>微信详情：<Link to="/attendance/DetailsLetter">微信详情</Link></p>
            </Modal>
        )
    }
}
