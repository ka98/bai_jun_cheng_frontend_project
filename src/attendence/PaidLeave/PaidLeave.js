import React from "react";
import { Table, Form, Input } from "antd";
import PaidLeaveSearch from "./PaidLeaveSearch";
const WrappedPaidLeaveSearch = Form.create()(PaidLeaveSearch);
const Search = Input.Search;
const columns = [{
    title: '序号',
    render: (text, record, index) => {
        return <span>{index + 1}</span>
    }
},{
    title: '姓名',
    dataIndex: 'employeeName',
},{
    title:'外包工号',
    dataIndex: 'aliNumber',
},{
    title:'部门',
    dataIndex:'department',
}, {
    title: '请假类型',
    dataIndex: 'leaveType',
}, {
    title: '请假日期',
    dataIndex: 'approval',
}, {
    title: '实际小时数（h）',
    dataIndex: 'realHour',
}, {
    title: '状态',
    dataIndex: 'status',
}, {
    title: '备注',
    dataIndex: 'comment',
}];
export default class PaidLeave extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSource:[],
            loading:[],
            isDisabled:true,
        };
        this.handleSearch = this.handleSearch.bind(this);
    }
    componentDidMount () {
        this.PaidLeaveFen("/snail/.no")
    }
    PaidLeaveFen = (url,param) => {
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body:JSON.stringify(param)
        }).then((response) => {
            return response.json()
        }).then((state) => {
            console.log(state)
        }).catch((e) => {
            console.log(e)
        })
    };
    handleSearch = (state) => {
        console.log(state);
    };
    aClick = () => {
        const { expand } = this.state;
        this.setState({ expand: !expand });
        this.setState({
            isDisabled: false
        });
    };
    aaClick = (state) => {
        console.log("state:",state);
        this.setState({isDisabled:true})
    };
    render(){
        return(
            <div>
                <div
                    style={{display: this.state.isDisabled ? "none" : "block"}}
                    className="wrapped"
                >
                    <WrappedPaidLeaveSearch wrappedComponentRef={(form) => this.form = form}/>
                </div>
                <a onClick={this.aClick} style={{display: !this.state.isDisabled ? "none" : "block",float:"left"}}>高级搜索</a>
                <a onClick={this.aaClick} style={{display: this.state.isDisabled ? "none" : "block",float:"left"}}>隐藏搜索</a>
                <div className="search">
                    <Search
                        placeholder="请输入群名称"
                        onSearch={this.handleSearch}
                        className="search"
                        style={{display: !this.state.isDisabled ? "none" : "block"}}
                    />
                </div>
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    loading={this.state.loading}
                    className="clear"
                />
            </div>
        )
    }
}
