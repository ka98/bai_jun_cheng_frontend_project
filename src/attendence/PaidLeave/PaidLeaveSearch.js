import React from "react";
import { Form, Row, Col, Button, Select, Input, DatePicker } from "antd";
import HTTPUtil from "util/HTTPUtil";
import Param from "util/transParam";
const FormItem = Form.Item;
const Option = Select.Option;
const { MonthPicker} = DatePicker;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    }
};
let timeout;
let currentValue;
export default class PaidLeaveSearch extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            dataSearch:[],
        }
        this.handleReset = this.handleReset.bind(this);
        this.handleOnSearch = this.handleOnSearch.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
    }
    handleSearch = (e) => {
        e.preventDefault();
        this.props.form.getFieldValue((values) => {
            console.log('Received values of form(DetailsLetterSearch): ', values);
            const param = {
                ...values,
                'deptId': values['deptId'] ? values['deptId'] : 0,
                "ymDate" : values['ymDate'] ? values['ymDate'].format('YYYY-MM') : ''
            };
            this.handleLetterFn("/snail/....",param)
        });
    }
    handleLetterFn (url ,param) {
        fetch(url,{
            headers:{
                'Content-Type':'application/x-www-form-urlencoded'
            },
            method:'post',
            body:JSON.stringify(param),
        }).then(response => {
            return response.json()
        }).then(data => {
            console.log(data)
        }).catch(error => {
            console.log(error)
        })
    }
    //重置
    handleReset = () => {
        this.props.form.resetFields();
    };
    handleOnSearch = (state) => {
        console.log(state);
        const param = {};
        param.deptName = event;
        this.fetchsSearch(param);
    }
    fetchsSearch (param) {
        const me = this;
        if(timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        currentValue = param;
        function fake() {
            HTTPUtil.post("/snail/...",Param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data) => {
                console.log(data);
                me.setState({
                    dataSearch:data.data
                })
            }).catch(error => { console.log(error)})
        }
        timeout = setTimeout(fake, 1000);
    }
    handleOnChange = () => {
        this.setState({ empId:data })
    }
    render(){
        const { getFieldDecorator } = this.props.form;
        const options = this.state.dataSearch.map(d => <Option key={d.id}>{d.name}</Option>);
        return(
            <Form onSubmit={this.handleSearch}>
                <Row gutter={16} className="eyebrows">
                    <Col span={24}>
                        <Button type="primary" htmlType="submit">搜索</Button>
                        <Button className="cz" onClick={this.handleReset}>重置</Button>
                    </Col>
                </Row>
                <Row gutter={16} type="flex" justify="space-between">
                    <Col span={8}>
                        <FormItem label="部门" {...formItemLayout}>
                            {getFieldDecorator("deptId")(
                                <Select
                                    showSearch
                                    showArrow={false}
                                    filterOption={false}
                                    defaultActiveFirstOption={false}
                                    onSearch = {this.handleOnSearch}
                                    onChange = {this.handleOnChange}
                                    notFoundContent={null}
                                >{options}</Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem label="员工姓名" {...formItemLayout}>
                            {getFieldDecorator("employeeName")(
                                <Input />
                            )}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem label="考勤日期" {...formItemLayout}>
                            {getFieldDecorator("ymDate")(
                                <MonthPicker
                                    placeholder="选择月份"
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        )
    }
}
