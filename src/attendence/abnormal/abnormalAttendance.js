import React from 'react';
import {Upload, Button, Icon, Table, message,Row,Col,Dropdown,Menu,Modal,Tabs,Collapse } from 'antd';
import HTTPUtil from "../../util/HTTPUtil";
import jQuery from "jquery";
const TabPane = Tabs.TabPane;
const Panel = Collapse.Panel;

const abnormalityOfAttendanceType={
    1:"上班忘记打卡",
    2:"下班未打卡",
    3:"严重迟到",
    4:"工作时长小于8小时",
    5:"调休+上班时间小于8小时",
    6:"周末加班时长与审批单不符",
    7:"加班加上上班时间小于8小时",
    8:"事假加上上班时间小于8小时",
    9:"外出审批异常",
    12:"上下班均未打卡",
    // 13:"请假后上下班缺卡",
};

export default class AbnormalAttendance extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            previewSource:[],   //显示预览table的数据
            dbdiffList:[],       //保存数据库数据
            viewSource:[],      //显示异常考勤数据
            abnormalMap:{},     //发送给后台的map
            sendBtnVisi:false, //发送按钮是否显示
            prevData:[],
            modalVisible:false,
            loading:false   //正在加载中
        }
    }
    //矫正考勤异常：
    correctionNormalAttendance =(record)=>{
        console.log("record:",record);
        this.fetchFnNormal("/snail/attendenceMongo/editNormal")
    };
    fetchFnNormal=(url,param)=>{
        fetch(url,{

        })
    };

    //消息发送：
    handleSend=()=>{
        HTTPUtil.post("/snail/attendence/sendMessage",JSON.stringify(this.state.viewSource),{"Content-Type":"application/json"}).then((res) => {
            var per = this;
            return res.json();
        }).then((data)=>{
            console.log("11:",data);
            const optionalParams = this;
            message.success('发送成功!')
        }).catch((e) => {
            message.error('发送失败!');
            console.log("e",e)
        })
    };

    //下载Excel
    fetchFnGroup=(url,param)=>{
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body:JSON.stringify(param)
        }).then((response)=>{
            return response.blob();
        }).then((blob)=>{
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement('a');
            a.href = url;
            a.download = "表格.xlsx";
            a.click();
            message.success("下载成功！")
        }).catch((e)=>{
            message.error("下载失败！")
        })
    };
    handleDownLoad=()=>{
        this.fetchFnGroup("/snail/attendence/attendanceDownLoad");
    };

    //下拉选框
    handleButtonClick=(e)=>{
    };
    handleMenuClick=(e)=>{
    };

    //弹出框确定按钮
    handleModalOk=()=>{
        this.setState({
            modalVisible:false
        });
        this.setState({
            loading:true    //加载中的效果
        });
        this.saveAttendence();
        this.compareAttendence();
    };

    //将异常考勤保存数据库
    saveAttendence=()=>{
        fetch('/snail/attendence/addAttendenceDB', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.dbdiffList)
        }).then(function (response) {
            response.json().then(function (data) {
                message.success("考勤数据保存成功");
            })
        }).catch((e)=>{
            message.error("保存失败！")
        });
    };

    //返回异常考勤数据
    compareAttendence=()=>{
        let me = this;
        me.setState({
            loading:true
        });
        HTTPUtil.post(
            "/snail/attendence/createAbnormalList",
            JSON.stringify({aliWorkList:this.state.previewSource}),
            {'Accept': 'application/json','Content-Type': 'application/json'}
        ).then(function (response) {
            message.success("返回数据成功！");
            me.setState({
                loading:true
            });
            if(response.code == 200){
                me.setState({
                    viewSource:response.data
                });
                message.success("返回成功！");
                me.setState({
                    loading:false
                })
            }else if(response.code !==200){
                message.error("返回数据错误！");
            }
        })
    };
    //弹出框取消按钮
    handleModalCancel=()=>{
        this.setState({
            modalVisible:false
        })
    };
    // table 的样式
    rowClass=(record, index)=>{
        let rowClass = "";
        if(record.dbstatus==1){
            rowClass =  "abnormalAttendanceAdd"
        }else if(record.dbstatus==2){
            rowClass =  "abnormalAttendanceEdit"
        }
        return rowClass;
    };
    render() {
        let me = this;
       //需要点击矫正按钮传参
        const columns_preview = [
            {
                title: '序号',
                render: (text, record, index) => {
                    return <span>{index + 1}</span>
                }
            }, {
                title: '姓名',
                dataIndex: 'employeeName',
            }, {
                title: '钉钉群组',
                dataIndex: 'groupName',
            }, {
                title: '外包工号',
                dataIndex: 'aliNumber',
            }, {
                title: '考勤日期',
                dataIndex: 'dateStr',
            }, {
                title: '上班打卡时间',
                dataIndex: 'punchTime1',
            }, {
                title: '下班打卡时间',
                dataIndex: 'punchTime2',
            }, {
                title: '考勤审批',
                dataIndex: 'approval',
            }, {
                title: '工作时长',
                dataIndex: 'attendenceMin',
            }];
        const columns_view = [
            {
                title: '序号',
                render: (text, record, index) => {
                    return <span>{index + 1}</span>
                }
            }, {
                title: '姓名',
                dataIndex: 'employeeName',
            }, {
                title: '钉钉群组',
                dataIndex: 'groupName',
            }, {
                title: '外包工号',
                dataIndex: 'aliNumber',
            }, {
                title: '考勤日期',
                dataIndex: 'dateStr',
            }, {
                title: '上班打卡时间',
                dataIndex: 'punchTime1',
            }, {
                title: '下班打卡时间',
                dataIndex: 'punchTime2',
            }, {
                title: '考勤审批',
                dataIndex: 'approval',
            }, {
                title: '工作时长',
                dataIndex: 'attendenceMin',
            }, {
                title: '考勤异常原因',
                dataIndex: 'abnormalityOfAttendance',
                render:(text,record)=>{
                    return <span>{abnormalityOfAttendanceType[text]}</span>
                }
            }, {
                title: '操作',
                dataIndex: 'address',
                render: (text,record) => {
                    return (
                        <div>
                            <a onClick={this.correctionNormalAttendance.bind(this,record.isCorrect)}>矫正考勤</a>
                        </div>
                    )
                }
            }];

        //上传考勤excel
        //需要设置数据
        const props = {
            name: 'file',
            action: '/snail/attendence/createAbnormalList',
            headers: {
                authorization: 'authorization-text',
                "Access-Control-Allow-Origin": "*"
            },
            showUploadList:false,
            supportServerRender: true,
            onChange(info) {
                me.setState({
                    loading:true
                });
                if (info.file.status !== 'uploading') {
                }
                if (info.file.status === 'done') {
                    message.success("上传成功！");
                    let prevData = info.file.response.data;  //返回值
                    console.log("prevData:",prevData);
                    me.setState({
                        viewSource : prevData,
                        loading:false,
                        sendBtnVisi:true
                    });

                } else if (info.file.status === 'error') {
                    message.error("上传失败！");
                }
            },
        };
        //下拉选框
        const menu = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key="1">1111</Menu.Item>
                <Menu.Item key="2">2222</Menu.Item>
                <Menu.Item key="3">3333</Menu.Item>
            </Menu>
        );
         const style={
             borderBottom:"1px solid #eee",
             marginBottom:"10px",
             paddingBottom:"9px",
             width:"100%",
             height:"42px",
         };


        //  //新加的选项卡效果的：
        // function callback(key) {
        //     console.log(key);
        // }
        return (
            <div>
                {/*<Tabs onChange={callback} type="card">*/}
                    {/*<TabPane tab="异常考勤" key="1">*/}
                        {/*<Collapse accordion>*/}
                            {/*<Panel header="实事异常考勤分析" key="1">*/}
                                {/**/}
                            {/*</Panel>*/}
                            {/*<Panel header="历史异常考勤记录(单位/月)" key="2">*/}
                                {/**/}
                            {/*</Panel>*/}
                        {/*</Collapse>*/}
                    {/*</TabPane>*/}
                    {/*<TabPane tab="事假考勤" key="2">*/}

                    {/*</TabPane>*/}
                {/*</Tabs>*/}

                <div style={{width:"100%",height:"32px"}}>
                    <div style={{width:"82px",float:"left"}}>
                        <Upload {...props} >
                            <Button type="primary"><Icon type="upload"/>导入</Button>
                        </Upload>
                    </div>
                    <Button style={{float:"left",margin:"0 16px",display:this.state.sendBtnVisi?"block":"none"}} onClick={this.handleSend}>发送</Button>
                    <Button onClick={this.handleDownLoad}  style={{float:"left",display:this.state.sendBtnVisi?"block":"none"}}>下载</Button>
                    {/*<Dropdown.Button*/}
                        {/*overlay={menu}*/}
                        {/*onClick={this.handleButtonClick}*/}
                        {/*type="ghost"*/}
                        {/*style={{float:"right"}}*/}
                    {/*>*/}
                        {/*筛选*/}
                    {/*</Dropdown.Button>*/}
                </div>

                {/*<Modal title="异常考勤" visible={this.state.modalVisible}*/}
                       {/*onOk={this.handleModalOk} onCancel={this.handleModalCancel} width='100%'*/}
                {/*>*/}
                    {/*<Table*/}
                        {/*dataSource={this.state.previewSource}*/}
                        {/*bordered*/}
                        {/*columns={columns_preview}*/}
                        {/*rowClassName={this.rowClass}*/}
                        {/*loading={this.state.loading}*/}
                    {/*/>*/}
                    {/*<div><span style={{display:this.state.previewSource.length==0?"none":"inline-block",width:"10px",height:"10px",backgroundColor:"green"}}></span>新增考勤</div>*/}
                    {/*<div><span style={{display:this.state.previewSource.length==0?"none":"inline-block",width:"10px",height:"10px",backgroundColor:"red"}}></span>修改考勤</div>*/}
                    {/*<span style={{display:this.state.previewSource.length == 0 ?"block":"none"}}>暂无数据表示与上次数据一致,点击确定后直接进入异常考勤列表页面</span>*/}
                {/*</Modal>*/}
                <Table
                    dataSource={me.state.viewSource}
                    // bordered
                    columns={columns_view}
                    loading={this.state.loading}
                />
            </div>
        )
    }
}

