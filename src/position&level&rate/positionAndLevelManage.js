import React, { Component } from 'react';
import { Table } from 'antd';
class positionAndLevelManage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            dataSource:[],
            quotationDO_v2s:[],
            disabled:true, //控制操作按钮
        };
        this.theEditorChange = this.theEditorChange.bind(this);
        this.theOkChange = this.theOkChange.bind(this);
    }
    componentDidMount () {
        fetch ("/snail/quotation_v2/findAllQuotation",{
            method: "post",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(
            res => res.json()
        ).then((data) => {
            console.log(data);
            let ss=[];
            data.data.map( item => {
                ss.push(item.quotationDO_v2s)
            });
            console.log('ss:',ss);
            this.setState({
                dataSource: data.data,
                quotationDO_v2s: ss,
            })
        }).catch(e => {console.log(e)})
    }
    theEditorChange = (e) => {
        console.log(e);
        this.setState({ disabled: false })
    };
    theOkChange = (e) => {
        console.log(e);
        this.setState({ disabled: true })
    };
    expandedRowRender = (record, index, indent, expanded) => {
        console.log("00:",record, index, indent, expanded)
        // console.log(this.props);
        const columns = [{
            title: '层级',
            dataIndex: 'hierarchyName',
            key: 'hierarchyName'
        }, {
            title: '报价',
            dataIndex: 'rate',
            key: 'rate'
        },{
            title: '操作',
            render:(text,record,index) => {
                return(
                    <a>编辑</a>
                )
            }
        }];
        console.log(this.state.quotationDO_v2s);
        return(
            <Table columns={columns} dataSource={this.state.quotationDO_v2s} bordered/>
        )
    };
    render() {
        const columns = [{
            title: '序号',
            dataIndex: 'number',
            key: 'number',
            render:(text, record, index)=> {
                return index+1;
            }
        }, {
            title: '岗位',
            dataIndex: 'jobName',
            key: 'jobName',
            // render:(val,row,index) => {
            //     console.log(val,row,index);
            //     const obj = {
            //         children:val,
            //         props:{}
            //     }
            //     if(row.jobName>0){
            //         console.log(obj.props);
            //         console.log(row.jobName);
            //         obj.props.rowSpan = row.jobName
            //     }else{
            //         console.log("111:",obj.props.rowSpan);
            //         obj.props.rowSpan = 0
            //     }
            //     console.log(obj.children);
            //     return (
            //         <span>{obj}</span>
            //     )
            //
            //     // return(
            //     //     {/*<span>{text.jobId===text.jobId}</span>*/}
            //     // )
            // }
        }, {
            title: '操作',
            render:(text,record,index) => {
                return(
                    <a
                        // onClick={this.expandedRowRender}
                    >详情</a>
                )
            }
        }];
        // console.log(this.state.dataSource,this.state.quotationDO_v2s)
        return (
            <div>
                <Table
                    columns={columns}
                    dataSource={this.state.dataSource}
                    // bordered
                    expandedRowRender={this.expandedRowRender}
                />
            </div>
        )
    }
}
export default positionAndLevelManage