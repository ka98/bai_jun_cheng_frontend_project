import React, { Component } from 'react'
import { Form, Input, Select, Button, message, Modal, Tabs,  } from 'antd'
import SelectList from 'util/selectList'
const FormItem = Form.Item
const TabPane = Tabs.TabPane
const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
}
class addPosition extends Component{
    constructor(props) {
        super(props)
        this.state = {
            hierarchyNameSet: []
        }
        this.findHierarchyByJob = this.findHierarchyByJob.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    findHierarchyByJob() {
        const { jobName } = this.props.form.getFieldsValue()
        jobName && fetch('/snail/jobsHierarchy/findAllHierachyByJobName', {
            method: "post",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ jobName })
        }).then(
            res => res.json()
        ).then((data) => {
            const hierarchyNameSet = data.data
            this.props.form.setFieldsValue({
                hierarchyNameSet
            })
            // this.setState({
            //     hierarchyNameSet
            // }, () => {
            //     console.log(this.state.hierarchyNameSet)
            // })
        })
    }
    handleSubmit(e){
        // console.log(this.props.form.getFieldsValue())
        e.preventDefault()
        fetch('/snail/jobsHierarchy/saveJobsHierachy', {
            method: "post",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.form.getFieldsValue())
        }).then(
            res => res.json()
        ).then((data) => {
            console.log(data)
            if (data.code === 200) {
                message.info(data.message)
            }else{
                message.err(data.message)
            }
        })
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <FormItem label="岗位" {...formItemLayout}>
                        {getFieldDecorator('jobName', {
                            rules: [
                                {require: true, message: '请输入岗位名称！'}
                            ],
                        })(
                            <Input placeholder="选择要添加的岗位" onBlur={this.findHierarchyByJob}/>
                        )}
                    </FormItem>
                    <FormItem label="层级" {...formItemLayout}>
                        {getFieldDecorator('hierarchyNameSet', {
                            initialValue: []
                        })(
                            <SelectList addName="岗位" rest={["项目经理", "专家"]}/>
                        )}
                    </FormItem>
                    <FormItem wrapperCol={{span:14, offset: 6}}>
                        <Button type="primary" htmlType="submit">确定新增</Button>
                        <Button type="primary" >新增岗位或层级</Button>
                    </FormItem>
                </Form>
                <Modal
                    title="新增岗位或层级"
                    visible={this.state.visible}
                    footer={null}
                >
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Tab 1" key="1">Content of Tab Pane 1</TabPane>
                        <TabPane tab="Tab 2" key="2">Content of Tab Pane 2</TabPane>
                    </Tabs>
                </Modal>
            </div>
        )
    }
}
export default Form.create()(addPosition)