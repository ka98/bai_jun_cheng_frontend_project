import {message} from "antd/lib/index";
//校验obj是否有key为undifined，如果有弹框error
export function checkObjRequired(obj, error) {
    let msg = error ? error : '请输入所有必要的信息！'
    for(let key in obj){
        let val = obj[key]
        if (!val && typeof val !== "object") {
            message.error(msg)
            return false
        } else if (Array.isArray(val) && val.length === 0) {
            message.error(msg)
            return false
        } else if (typeof val === "object" && !Array.isArray(val) && Object.keys(val).length === 0) {
            message.error(msg)
            return false
        }
    }
    return true
}
//一个数组的拓展方法，数组减传进去的参数
export function extendArrayMinus(){
    Array.prototype.minus = function(a){
        return this.filter((item) => {
            return item!== a
        })
    }
}
//批量绑定this
export function batchBindThis(context) {
    Array.prototype.shift.apply( arguments )
    Array.prototype.forEach.call(arguments, (item) => {
        context[item] = context[item].bind(context)
    })
}
