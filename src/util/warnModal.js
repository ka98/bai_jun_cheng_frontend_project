import React, { Component } from 'react'
import { Modal, Icon } from 'antd'
class WarnModal extends Component{
    constructor(props) {
        super(props)
    }
    render() {
        const props = {
            title: <div>
                <Icon type="exclamation" style={{color: 'red'}}/>
                <span>警告！</span>
            </div>,
            onOk: this.props.onOk,
            onCancel: this.props.onCancel,
            visible: this.props.visible
        }
        return (
            <Modal {...props}>
                <p>您确定要删除{this.props.deleteName}的数据吗？</p>
            </Modal>
        )
    }
}
export default WarnModal