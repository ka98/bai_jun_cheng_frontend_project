import React, { Component } from 'react'
import { Icon, Select, Row, Col, Button, message } from 'antd'
class SelectList extends Component{
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            selectValue: ''
        }
        this.addOne = this.addOne.bind(this)
    }
    componentWillReceiveProps(nextProps) {
        const { value } = nextProps
        if (this.props.value.length !== value.length) {
            this.setState({
                list: value
            })
        }
    }
    addOne() {
        const { list, selectValue } = this.state
        if (selectValue) {
            this.props.rest.filter((item) => {
                return item !== this.state.selectValue
            })
            this.setState({
                list: list.concat(selectValue),
                selectValue: ''
            }, () => {
                this.props.onChange(this.state.list)
            })
        } else {
            message.error('请先输入要添加的' + this.props.addName)
        }
    }
    render() {
        const spanStyle = {padding: '0 15px', margin: 2, color:'#fff', backgroundColor: '#31b0d5', display: 'inline-block', borderRadius: 4, borderColor: '#269abc', }
        return (
            <div>
                <div>
                    {
                        this.state.list.length > 0 && this.state.list.map((item) => {
                            return (<div style={spanStyle} key={item}>{item}</div>)
                        })
                    }
                </div>
                <div style={{display: 'flex', marginTop: 10}}>
                    {/*<Input ref={this.inputRef} style={{flex: 1}}/>*/}
                    <Select style={{flex: 1}} value={this.state.selectValue} onChange={(value) => {this.setState({selectValue: value})}}>
                        {
                            this.props.rest.map((item) => {
                                return (
                                    <Option value={item} key={item}>{item}</Option>
                                )
                            })
                        }
                    </Select>
                    <Button onClick={this.addOne}><Icon type="plus-circle-o"/></Button>
                </div>
            </div>
        )
    }
}
export default SelectList