import React, { Component } from 'react'
import { Icon, Input, Row, Col, Button, message } from 'antd'
class InputList extends Component{
    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
        console.log(props)
        this.inputRef = React.createRef()
        this.addOne = this.addOne.bind(this)
    }
    componentWillReceiveProps(nextProps) {
        const { value } = nextProps
        if(this.props.value.length !== value.length){
            this.setState({
                list: value
            })
        }
    }
    addOne() {
        let input = this.inputRef.current.input
        const { list } = this.state

        input.value ? this.setState({
            list: list.concat(input.value)
        }, () => {
            input.value = '';
            console.log(this.state.list)
            this.props.onChange(this.state.list)
        }) : message.error('请先输入要添加的' + this.props.addName)
    }
    render() {
        const spanStyle = {padding: '0 15px', margin: 2, color:'#fff', backgroundColor: '#31b0d5', display: 'inline-block', borderRadius: 4, borderColor: '#269abc', }
        return (
            <div>
                <div>
                    {
                        this.state.list.length > 0 && this.state.list.map((item) => {
                            return (<div style={spanStyle} key={item}>{item}</div>)
                        })
                    }
                </div>
                <div style={{display: 'flex', marginTop: 10}}>
                    <Input ref={this.inputRef} style={{flex: 1}}/>
                    <Button onClick={this.addOne}><Icon type="plus-circle-o" /></Button>
                </div>
            </div>
        )
    }
}
export default InputList