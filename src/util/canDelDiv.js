import React, { Component } from 'react'
import { Icon, message } from 'antd'
import './css/canDelDiv.css'
class canDelDiv extends Component{
    constructor(props) {
        super(props)
    }
    render() {
        const { data, canDel } = this.props
        return (
            !canDel ?
            data.map((item) => {
                return (<div className="normal-div" key={item}><span>{item}</span></div>)
                }) :
            data.map((item, index) => {
                return (<div className="normal-div del-div" key={item} onClick={() => {data.length > 1 ? this.props.onChange(index) : message.error('只剩一个不可以删除呀！')}}><span>{item}</span><Icon type="close"/></div>)
        })
        )
    }
}
export default canDelDiv