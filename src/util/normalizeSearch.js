const normalizeSearch = function(obj) {
    console.log(obj)
    let str = '?';
    if(!Object.keys(obj).length){
        return ''
    }
    for (let key in obj) {
        str += `${key}=${obj[key]}&&`
    }
    str = str.slice(0, -2)
    return str
}
export default normalizeSearch