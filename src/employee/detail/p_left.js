import React from "react";

export default class Perleft extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            empDetail:{
                name:"",
                phone:""
            }  //明细的数据对象
        }
    }


    componentWillReceiveProps(nextProps) {
        let empDetail = nextProps.empDetail==null?{name:""}:nextProps.empDetail;
        this.setState({
            empDetail:empDetail
        });

    }

    render(){
        return(
            <div className='per1'>
                <img src={require('../../../img/employee/person.jpg')} alt="头像"  className="per1-img" />
                <div className="per1-header">
                    <h3 className="per1-name">{this.state.empDetail.name}</h3>
                </div>
                <ul className="per1-ul">
                    <li>
                        <a>{this.state.empDetail.phone}</a>
                        <span className="per1-span">拨打电话</span>
                    </li>
                    <li>
                        <a>{this.state.empDetail.email}</a>
                        <span className="per1-span">发送邮件</span>
                    </li>
                </ul>
            </div>
        )
    }
}
