import React from "react";
import {Form, Collapse, Tabs, Row, Col, Input,Table,DatePicker,Button} from 'antd';
import jQuery from 'jquery';
import HTTPUtil from "../../../util/HTTPUtil";

const FormItem = Form.Item;
const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;

const columns = [
    {
    title: '开始核算时间',
    dataIndex: 'startCountTime',
    key: 'startTime',
}, {
    title: '结算截止时间',
    dataIndex: 'endCountTime',
    key: 'endTime',
}, {
    title: '工作日加班',
    dataIndex: 'weekOverHour',
    key: 'weekOverHour',
}, {
    title: '周末加班',
    dataIndex: 'weekendOverHour',
    key: 'weekendOverHour',
}, {
    title: '节假日加班',
    dataIndex: 'vocationOverHour',
    key: 'vocationOverHour',
}, {
    title: '请假天数',
    dataIndex: 'leaveHour',
    key: 'leaveHour',
}, {
    title: '调休天数',
    dataIndex: 'approvalDay',
    key: 'approvalDay',
}, {
    title: '微信事假累计天数',
    dataIndex: 'wxLeaveDay',
    key: 'wxLeaveDay',
}];

class AttendanceAccountingForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            AttHistoryList:[],
            dataSource:[]
        }
    }

    componentDidMount() {
        let me = this;
        console.log("let empId = parseInt( this.props.match.params.empId);",this.props.empDetail);
        let empId = this.props.empDetail.empId;
        //考勤核算历史记录
        HTTPUtil.post("/snail/attendenceCheck/findAttHistoryList",jQuery.param({empId:empId}),{"Content-Type":"application/x-www-form-urlencoded"}).then((data)=>{
            if(data.code==200){
                me.setState({
                    AttHistoryList:data.data
                })
            }
        });

        //剩余调休天数
        HTTPUtil.post("/snail/attendenceCheck/countAttendance",jQuery.param({empId:empId}),{"Content-Type":"application/x-www-form-urlencoded"}).then((data)=>{
            if(data.code==200){
                me.props.form.setFieldsValue(data.data);
            }
        })
    }
    handleSubmit = (e) => {
        let formData = this.props.form.getFieldsValue();
        console.log("formData",formData);
        formData.startCountTime = formData.startTime;
        formData.endCountTime = formData.endTime;
        delete  formData.startTime;
        delete formData.endTime;

        let empId = this.props.empDetail.empId;
        formData.empId=empId;
        HTTPUtil.post("/snail/attendenceCheck/countAndClearAtt",jQuery.param(formData),{"Content-Type":"application/x-www-form-urlencoded"}).then((data)=>{
            console.log("data:",data)
        })
    };

    handleEditClick =(e)=>{

    };

    handleSaveClick =(e)=>{

    };

    render() {

        let isDisabled = this.state.isDisabled;
        const { getFieldDecorator } = this.props.form;

        return (
            <Form horizontal  onSubmit={this.handleSubmit} id="advForm">
                <Collapse style={{border: 0}}
                          defaultActiveKey={['2','3']}
                          // activeKey={['1']}
                >
                    <Panel header="核算历史记录" key="2">
                        <Table dataSource={this.state.AttHistoryList} columns={columns} />
                    </Panel>
                    <Panel header="剩余调休天数" key="3">
                        <Row gutter={16}>
                            <Col sm={8}>
                                <Form.Item  label="工作日加班"  labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('weekOverHour', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col sm={8}>
                                <Form.Item label="周末加班" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('weekendOverHour', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col sm={8}>
                                <Form.Item  label="节假日加班"  labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('vocationOverHour', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col sm={8}>
                                <Form.Item label="请假天数" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('leaveHour', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col sm={8}>
                                <Form.Item  label="调休天数"  labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('approvalDay', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col sm={8}>
                                <Form.Item label="微信事假累计天数" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('wxLeaveDay', {})(
                                        <Input disabled={isDisabled}/>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col sm={8}>
                                <Form.Item  label="开始核算时间"  labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('startTime', {})(
                                        <Input disabled={isDisabled} />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col sm={8}>
                                <Form.Item label="结算截止时间" labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    {getFieldDecorator('endTime', {})(
                                        <Input disabled={isDisabled} />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col sm={8}>
                                <Form.Item  label=""  labelCol={{ span: 10 }} wrapperCol={{ span: 14 }}>
                                    <Button type="primary" htmlType="submit">核算</Button>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Panel>
                </Collapse>
            </Form>
        );
    }
}
const AttendanceAccounting = Form.create()(AttendanceAccountingForm);
export default AttendanceAccounting
