import React from "react";
import {Form, Collapse, Tabs, Row, Col, Input, Button, Select, DatePicker, message} from 'antd';
import {Link} from "react-router-dom";
import moment from 'moment';
import jQuery from 'jquery';
import Param from "../../../util/transParam"
import HTTPUtil from "../../../util/HTTPUtil";
import employees from "../../employee";

const FormItem = Form.Item;
const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;

export default class HorizontalLoginForm extends React.Component {
    constructor() {
        super();
        this.state = {
            initData: true,
            isDisabled: true,
            isRender: false,
            empDetail: {}
        }
    }

    initLoad = (empDetail) => {
        this.setState({
            empDetail: empDetail
        });
    };


    handleSubmit = (e) => {
        e.preventDefault();
        let me = this;
        //请求返回的数据
        let reqData = this.state.empDetail;
        let formData = {};
        //表单的数据
        this.props.form.validateFields((err, values) => {
            formData = values;
        });
        console.log("formData", formData);
        formData.empId = reqData.empId;
        formData.viceId = reqData.viceId;
        if(formData.entryDate) {
            formData.entryDate = formData.entryDate._d;
        }
        if(formData.departureDate){
            formData.departureDate = formData.departureDate._d;
        }
        for (var k in formData) {
            if (!formData[k]) {
                delete  formData[k]
            }
        }
        this.fetchFn("/snail/employees/updateEmp", formData);
    };
    fetchFn = (url, formData) => {
        let Da = formData;
        HTTPUtil.post(url, jQuery.param(Da), {"Content-Type": "application/x-www-form-urlencoded"}).then((data) => {
            if (data.code == 200) {
                message.success("保存成功");
            }
        }).catch((error)=>{
            console.log("error:",error);
            message.error("保存失败！")
        })
    };
    handleEditClick = (e) => {
        this.setState({
            isDisabled: false
        })
    };
    handleSaveClick = (e) => {
        this.setState({
            isDisabled: true
        })
    };

    render() {
        let empDetail = this.state.empDetail;
        let isDisabled = this.state.isDisabled;
        const {getFieldDecorator} = this.props.form;
        const dateFormat = 'YYYY-MM-DD';
        return (
            <div>
                <Form horizontal onSubmit={this.handleSubmit} id="advForm">
                    <Row gutter={2} style={{marginBottom: 10,width:"155px"}}>
                        <Col sm={2}>
                            <div style={{width:"155px"}}>
                                <Button onClick={this.handleEditClick}
                                        style={{display: !this.state.isDisabled ? "none" : "block",float:"left"}}>编辑</Button>
                                <Button htmlType="submit" onClick={this.handleSaveClick}
                                        style={{display: this.state.isDisabled ? "none" : "block",float:"left"}}>保存</Button>
                                <Button style={{float:"right"}}><Link to={{pathname: '/employees/employeesInfo'}}>返回</Link></Button>
                            </div>
                        </Col>
                    </Row>
                    <Collapse
                        // accordion
                        style={{border: 0}}
                        defaultActiveKey={['1']}
                    >
                        <Panel header="基本信息" key="1">
                            <Row gutter={16}>
                                <Col sm={8}>

                                    <Form.Item label="姓名" key='name' labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('name', {
                                            initialValue: empDetail.name
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="邮箱" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('email', {
                                            initialValue: empDetail.email
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="工号" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('workNum', {
                                            initialValue: empDetail.workNum
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="用工类型" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('typeOfLabor', {
                                            initialValue: empDetail.typeOfLabor
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="外包工号" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('outNum', {
                                            initialValue: empDetail.outNum
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="用工状态" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('workingState', {
                                            initialValue: empDetail.workingState
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="学历" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('education', {
                                            initialValue: empDetail.education
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="性别" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('sex', {
                                            initialValue: empDetail.sex
                                        })(
                                            <Select style={{width: 200}} disabled={isDisabled}>
                                                <Option value="男">男</Option>
                                                <Option value="女">女</Option>
                                            </Select>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="年龄" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('age', {
                                            initialValue: empDetail.age
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="手机号" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('phone', {
                                            initialValue: empDetail.phone
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Panel>
                        <Panel header="合同信息" key="2">
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="职务" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('job', {
                                            initialValue: empDetail.job,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="在职状态" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('onTheJobStatus', {
                                            initialValue: empDetail.onTheJobStatus
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="入职日期" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('entryDate', {
                                            initialValue: empDetail.entryDate?moment(empDetail.entryDate, dateFormat):''
                                        })(
                                            <DatePicker disabled={isDisabled}
                                                        defaultValue={empDetail.entryDate?moment(empDetail.entryDate, dateFormat):''}
                                                        placeholder="请选择日期"/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="效率" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('rate', {
                                            initialValue: empDetail.rate
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="离职日期" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('departureDate', {
                                            initialValue: empDetail.departureDate?moment(empDetail.departureDate, dateFormat):''
                                        })(
                                            <DatePicker disabled={isDisabled}
                                                        defaultValue={empDetail.departureDate?moment(empDetail.departureDate, dateFormat):''}
                                                        placeholder="请选择日期"/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="客户类型" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('customerType', {
                                            initialValue: empDetail.customerType,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="办公城市" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('officeCity', {
                                            initialValue: empDetail.officeCity,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Panel>
                        <Panel header="其他" key="3">
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="技能" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('skill', {
                                            initialValue: empDetail.skill,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="级别" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('level', {
                                            initialValue: empDetail.level,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="详细状态" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('stateDetail', {
                                            initialValue: empDetail.stateDetail,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="最新薪资" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('latestSalary', {
                                            initialValue: empDetail.latestSalary,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="部门" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('department', {
                                            initialValue: empDetail.department,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col sm={8}>
                                    <Form.Item label="转正薪资" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('positiveSalary', {
                                            initialValue: empDetail.positiveSalary,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col sm={8}>
                                    <Form.Item label="是否交付" labelCol={{span: 10}} wrapperCol={{span: 14}}>
                                        {getFieldDecorator('delivered', {
                                            initialValue: empDetail.delivered,
                                        })(
                                            <Input disabled={isDisabled}/>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Panel>
                    </Collapse>
                </Form>
            </div>
        );
    }
}
