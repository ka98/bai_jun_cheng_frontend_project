import React from "react";
import { Tabs,Collapse} from 'antd';
import {Form} from "antd/lib/index";

//子组件
import HorizontalLoginForm from "./basicInformation/advSearchForm";
import AttendanceAccounting from "./attendanceAccounting/finalAttendance";
import AccountingAttendance from '../../attendence/account/AttendanceDetail'

const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;
const WrappedHorizontalLoginForm = Form.create()(HorizontalLoginForm);

export default class Perright extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            empDetail:{},  //明细的数据对象

        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            empDetail: nextProps.empDetail,
            edit:nextProps.edit
        }, () => {
            console.log(this.state.empDetail)
        });
        console.log("pright",nextProps.empDetail);
        this.formRef.initLoad(nextProps.empDetail);
    }
    /*getEmpMsg(msg) {

    }*/
    render(){
        let empDetail = this.props.empDetail;
        let edit = this.state.edit;
        let empMsg = this.props.empMsg
        return(
            <div className='per2'>
                <Tabs defaultActiveKey="1" >
                    <TabPane tab="基本信息" key="1">
                        <WrappedHorizontalLoginForm wrappedComponentRef={(inst) => this.formRef = inst} edit={edit}/>
                    </TabPane>
                    <TabPane tab="考勤核算" key="2">
                        <AccountingAttendance empId={this.props.empId} empName={this.state.empDetail.name}/>
                        {/*<AttendanceAccounting  empDetail={empDetail}/>*/}
                    </TabPane>
                    <TabPane tab="劳动合同" key="3">开发中。。。。</TabPane>
                </Tabs>
            </div>
        )
    }
}
