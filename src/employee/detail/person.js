import React from "react";
import {message} from "antd";
//引入样式
import '../css/person.css';

//引入子组件内容
import Perleft from "./p_left";
import Perright from "./p_right";

export default class Person extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            empDetail:{},  //明细的数据对象
            edit:false,     //主页操作
            isDisabled:false,   //加载效果隐藏与否
            loading:false,      //正在加载
        }
    }

    componentDidMount(){
        console.log(this.props)
        let empId = parseInt( this.props.match.params.empId);
        let edit = this.props.match.params.edit;
        this.setState({
            edit:edit,
            loading:true
        });
        this.fetchFn(empId);
    }

    //获取数据
    fetchFn = (empId) => {
        let me = this;
        fetch('/snail/employees/findEmpById',{
            method: 'POST',
            headers: {
                "Content-Type":"application/x-www-form-urlencoded",
                'Accept': 'application/json',
            },
            body:"empId="+empId,
        }).then(res => {
            me.setState({
                isDisabled: true
            });
            res.json().then(function(data){
                    me.setState({
                        empDetail : data.data
                    });
                    me.setState({
                        isDisabled: false,
                        loading:false
                    })
            });
        }).catch((e) => {
            message.error("加载失败！");
            console.log("加载失败的原因：",e);
        })
    };

    render(){
        let empDetail = this.state.empDetail;
        let empId = parseInt( this.props.match.params.empId);
        let edit = this.state.edit;
        // console.log("empDetail",empDetail);
        // console.log("edit",edit);
        console.log(this.props.match.params.empId)
        return(
            <div id="person">
                <Perleft empDetail={empDetail}/>
                <Perright empId={empId} edit={edit}  empDetail={empDetail}/>
                <div style={{clear:"both"}}></div>
            </div>
        )
    }
}
