import React from 'react';
import {Table,Button,Input, message,Popconfirm,LocaleProvider} from 'antd';
import jQuery from 'jquery';
import {Link} from 'react-router-dom';
import zhCN from 'antd/lib/locale-provider/zh_CN';
const Search = Input.Search;

// 引入React-Router模块
import classNames from 'classnames';

//引入子组件
import Person from "../employee/detail/person";
import New from "./emplo/emplo-preview";
import HTTPUtil from "../util/HTTPUtil";

const InputGroup = Input.Group;

//表格内容
const columns = [
    {
        title:'序号',
        dataIndex:'pageInfo.data[0]',
        render:(text,record,index)=>{
            return <span>{index+1}</span>
        }
    },{
        title: '姓名',
        dataIndex: 'name',
        render:(text,record)=>{
            return <a><Link to={"/person/"+record.empId+"/false"} >{text}</Link></a>;
        }
    }, {
        title: '年龄',
        dataIndex: 'age'
    }, {
        title: '工号',
        dataIndex: 'workNum'
    }, {
        title: '外包工号',
        dataIndex: 'outNum'
    }, {
        title: '手机号码',
        dataIndex: 'phone'
    }, {
        title: '学历',
        dataIndex: 'education'
    }, {
        title: '生日',
        dataIndex: '/'
    }, {
        title: '身份证信息',
        dataIndex: '/'
    }, {
        title: '入职时间',
        dataIndex: 'entryDate'
    }, {
        title: '离职时间',
        dataIndex: 'departureDate'
    }, {
        title: '个人等级',
        dataIndex: 'level'
    }, {
        title: '个人薪资',
        dataIndex: 'latestSalary'
    },{
        title: '个人设备',
        dataIndex: '/'
    },{
        title: '状态',
        dataIndex: 'onTheJobStatus'
    },{
        title:'操作',
        render:(text,record,index)=>{
            return <a ><Link to={"/person/"+record.empId+"/false"} >编辑</Link></a>
        }
    }];

// 表格设置
export default class Employee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage : 1,  //当前页
            pageSize : 10,    //一页显示几条数据
            total:0,        //一共多少条数据
            tDate: [],     //获取全部数据
            selectedRowKeys: [],
            lists: [],
            name:"name",
            inputValue:null,     //选框所选内容
            loading:false,      //正在加载
            empIdList:[]
        }
    };

    //获取表格中数据
    fetchFn= (url,param) => {
        param = param?param:{};
        param.startPage=this.state.currentPage;
        param.pageSize=this.state.pageSize;
        this.setState({ loading: true });
        fetch(url,{
            // headers: {
            //     'Content-Type': 'application/x-www-form-urlencoded'
            // },
            method:'post',
            body:jQuery.param(param)
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log("data(old):",data);
                let total = data.pageInfo.total;
                data = data.pageInfo.list;
                for(let i=0;i<data.length;i++){
                    data[i].key=i;
                }
                this.setState({
                    tDate : data,
                    total:total,
                    loading:false
                });
            })
            .catch((e) => {
                message.error("获取数据失败！")
            })
    };
    componentDidMount() {
        this.fetchFn("/snail/employees/findAllEmp");
    }

    // checkbox状态(选框)
    onSelectChange = (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        this.setState({ selectedRowKeys });
        // this.state.tDate({empId});
        let empIdData = [];
        for(var i=0;i<selectedRows.length;i++){
            empIdData.push(selectedRows[i]["empId"]);
        }
        this.setState({
            empIdList:empIdData
        });
    };

    //快速搜索
    handleSearch=(value)=>{
        console.log("value(搜索):",value);
        let param = {};
        param.workNum = this.state.inputValue;
        param.name=this.state.inputValue;
        this.fetchFn("/snail/employees/findAllEmp",param);
    //     HTTPUtil.post("/snail/employees/findAllEmp",jQuery.param(param),{'Content-Type':'application/x-www-form-urlencoded'}).then((data)=>{
    //         console.log(data);
    //         this.setState({
    //             tDate : data.pageInfo.list
    //         });
    //         loading:false;
    //     })
    };
    handleInputChange=(e)=> {
        this.setState({
            inputValue: e.target.value,
        });
    };

    //删除
    fetchFnParam=(url,param)=>{
        let me = this;
        fetch(url,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method:'post',
            body:JSON.stringify(param)
        })
            .then((res) => {
                return res.json();
                //this.setState({ loading: true });    //正在加载中
            })
            .then((data) => {
                message.info("删除成功");
                me.fetchFn("/snail/employees/findAllEmp",{});
                me.setState({
                    selectedRowKeys: []
                })
            })
            .catch((e) => {  })
    };
    deleteEmp=()=>{
        this.fetchFnParam("/snail/employees/deleteEmp",this.state.empIdList);
    };

    render() {
        var self = this;
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };

        //分页
        const pagination = {
            total: this.state.total,
            // defaultCurrent: this.state.tDate.pages,
            // pageSize: this.state.tDate.pageSize,
            showSizeChanger: true,
            startPage:[],
            onShowSizeChange(current, pageSize) {
                let param = {};
                param.workNum = self.state.inputValue;
                param.name=self.state.inputValue;
                console.log("current:",current,"pageSize:",pageSize);
                self.setState({
                    currentPage: current,
                    pageSize:pageSize
                },()=>{
                    self.fetchFn("/snail/employees/findAllEmp",param);
                });

            },
            onChange(current) {
                let param = {};
                param.workNum = self.state.inputValue;
                param.name=self.state.inputValue;
                console.log("current:",current);
                self.setState({
                    currentPage:current
                },()=>{
                    self.fetchFn("/snail/employees/findAllEmp",param);
                });
            }
        };
        //快速搜索 start
        const { style, size, placeholder } = this.props;
        const btnCls = classNames({
            'ant-search-btn': true,
        });
        const searchCls = classNames({
            'ant-search-input': true,
            'ant-search-input-focus': this.state.focus,
        });

        return (
            <div>
                <div>
                    <New />
                    <Popconfirm title="确定要删除吗？" onConfirm={self.deleteEmp}>
                        <Button type="danger">删除</Button>
                    </Popconfirm>
                    {/*<div className="search">*/}
                        {/*<Search*/}
                            {/*placeholder="请输入群名称"*/}
                            {/*onSearch={this.handleSearch}*/}
                            {/*style={{ width: 200 }}*/}
                        {/*/>*/}
                    {/*</div>*/}
                    <InputGroup className={searchCls} style={{width:'200px',float:'right'}}>
                        <Input placeholder={placeholder}  value={this.state.inputValue} onChange={this.handleInputChange}/>
                        <div className="ant-input-group-wrap">
                            <Button icon="search" className={btnCls} size={size} onClick={this.handleSearch} style={{borderRadius: '0 3px 3px 0'}}/>
                        </div>
                    </InputGroup>
                </div>
                <LocaleProvider locale={zhCN}>
                    <Table
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={this.state.tDate}
                        pagination={pagination}
                        loading={this.state.loading}
                    />
                </LocaleProvider>
                <div>共<span>{this.state.total}</span>条数据</div>
            </div>
        )
    }
}


