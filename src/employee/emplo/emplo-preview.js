import React from 'react';
import { Modal, Button, message,Select,Table,Icon} from 'antd';
import Uploa from "./upload";
import jQuery from 'jquery';
const Option = Select.Option;

//state 0: 不变  1:新增  2:修改
var green={
    backgroundColor:'#0f0'
};
var red={
    backgroundColor:'#f00'
};

//excel表的类型
const base = "base";
const detail="detail";
const phone ="phone";
const column_phone = [
    {
        title:'序号',
        dataIndex:'pageInfo.data[0]',
        render:(text,record,index)=>{
            return <span>{index+1}</span>
        }
    },{
        title: '姓名',
        dataIndex: 'name',
        render:(text,record)=>{
            let state = record.state;
        return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '工号',
        dataIndex: 'workNum',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '手机号码',
        dataIndex: 'phone',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }];

const column_base = [
    {
        title:'序号',
        dataIndex:'pageInfo.data[0]',
        render:(text,record,index)=>{
            return <span>{index+1}</span>
        }
    },{
        title: '姓名',
        dataIndex: 'name',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '年龄',
        dataIndex: 'age',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '性别',
        dataIndex: 'sex',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '详细状态',
        dataIndex: 'stateDetail',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '工号',
        dataIndex: 'workNum',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },{
        title: '学历',
        dataIndex: 'education',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '入职时间',
        dataIndex: 'entryDate',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '离职时间',
        dataIndex: 'departureDate',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '最新薪资',
        dataIndex: 'latestSalary',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '转正薪资',
        dataIndex: 'positiveSalary',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    }, {
        title: '使用薪资',
        dataIndex: 'trialSalary',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },{
        title: '技能',
        dataIndex: 'skill',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },
    {
        title: '等级',
        dataIndex: 'level',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },
    {
        title: '一级项目',
        dataIndex: 'oneLevelPro',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },
    {
        title: '上项日期',
        dataIndex: 'projectOnlineDate',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },
    {
        title: '下项日期',
        dataIndex: 'projectOfflineDate',
        render:(text,record)=>{
            let state = record.state;
            return <a style={state==1?green:state==2?red:null}>{text}</a>;
        }
    },];

const column_detail=[
    {
        title:'序号',
        dataIndex:'pageInfo.data[0]',
        render:(text,record,index)=>{
            return <span>{index+1}</span>
        }
    },{
        title: '姓名',
        dataIndex: 'name',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '工号',
        dataIndex: 'workNum',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '是否交付',
        dataIndex: 'delivered',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '部门',
        dataIndex: 'department',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '邮箱',
        dataIndex: 'email',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '职务',
        dataIndex: 'job',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '办公城市',
        dataIndex: 'officeCity',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '在职状态',
        dataIndex: 'onTheJobStatus',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '用工类型',
        dataIndex: 'typeOfLabor',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },{
        title: '用工状态',
        dataIndex: 'workingState',
        render:(text,record)=>{
            let state = record.state;
            return <div style={state==1?green:state==2?red:null}>{text}</div>;
        }
    },
];


export default class New extends React.Component {
    constructor(props){
        super(props);
        this.state={
            PrevData:[],
            empType:"phone",
            modalVisible: false,
        }
    }
    state = {
        modalVisible: false,
        PreviewModalVisible:false
    };

    //弹出框-确定
    setModalVisible(modalVisible) {
        this.setState({ modalVisible:modalVisible });
    }
    showPreviewData=()=>{
        this.setModalVisible(false);
        this.setState({
            PreviewModalVisible:true
        })
    };

    //从子组件中传过来的全部数据
    handleEmail=(data)=>{
        this.setState({
            PrevData :data
        });
    };


    //预览-确定
    setModal2Visible(isOk){
        this.setState({PreviewModalVisible:false});
        if(isOk) {
            this.fetchFn();
        }
    }
    fetchFn= (params = {}) => {
        // this.setState({ loading: true });

        fetch('/snail/employees/saveEmpList_v2', {
            method: "POST",
            headers:{
                'Accept':'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({"empList":this.state.PrevData,"empType":this.state.empType})}).then(function(response) {
                message.info("保存成功");
            },function(error){
                message.info("保存失败");
                console.log("error:",error);
        })
    };

    showPreview2Data=()=>{
        this.setModal2Visible(false);
        this.setState({
            PreviewModalVisible:true
        });
    };

    event = {
        data:[],
        selectedRowKeys: [],
        list: [],
        name:"name",
        inputValue:null,     //选框所选内容
        loading:false,      //正在加载

    };

    //checkbox状态(选框)
    onSelectChange = (selectedRowKeys) => {
        this.setState({ selectedRowKeys })
    };
    searchInput=(event)=>{
        this.setState({
            inputValue:event.target.value
        });
    };

    render() {
        let me = this;
        let empType = this.state.empType;
        //下拉选框
        function handleChange(value) {
            me.setState({
                empType:value
            })
        }

        //预览表
        var self = this;
        const { selectedRowKeys } = this.event;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        };
        //分页
        const pagination = {
            total: this.event.length,
            showSizeChanger: true,
            onShowSizeChange(current, pageSize) {
            },
            onChange(current) {
            }
        };

        return (
            <div style={{marginRight:"16px", float:'left'}} >
                <Button type="primary" onClick={() => this.setModalVisible(true)}><Icon type="upload" />导入</Button>
                <Modal
                    title="请选择文件："
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}
                    onOk={this.showPreviewData}
                    onCancel={() => this.setModalVisible(false)}
                    maskClosable={()=>this.setModalVisible(true)}
                >
                    <Select defaultValue="phone" style={{ width: '120',marginBottom:'5'}} onChange={handleChange}>
                        <Option value="phone">通讯录表</Option>
                        <Option value="base">基础表</Option>
                        <Option value="detail">明细表</Option>
                    </Select>
                    <br/>
                    <Uploa handleEmail={this.handleEmail.bind(this)} empType={this.state.empType}/>
                </Modal>
                <div onChange={this.props.PrevData}>
                    <Modal
                        title="预览："
                        wrapClassName="vertical-center-modal"
                        visible={this.state.PreviewModalVisible}
                        maskClosable={()=>this.setModal2Visible(true)}
                        width='100%'
                        onOk={()=>this.setModal2Visible(true)}
                        onCancel={() => this.setModal2Visible(false)}

                    >
                        <Table rowSelection={rowSelection}
                               columns={empType==base?column_base:empType==detail?column_detail:column_phone}     //列
                               rowKey={record => record.registered}    //
                               dataSource={this.state.PrevData}    //数据
                               bordered                    //边框
                               pagination={pagination}     //分页 1
                               />
                    </Modal>
                </div>
            </div>
        );
    }
}

