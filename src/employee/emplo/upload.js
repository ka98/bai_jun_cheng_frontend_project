import React from 'react';
import {Upload,Button,Icon,message} from 'antd';
import '../../main.css'

export default class Uploa extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            prevData:[]
        }
    }

    render(){
        let me = this;
        const props = {
            name: 'file',
            action: '/snail/employees/previewEmpList_v2',
            headers: {
                authorization: 'authorization-text',
                "Access-Control-Allow-Origin":"*"
            },
            data:{
                empType:this.props.empType
            },
            supportServerRender:"true",
            onChange(info) {
                if (info.file.status !== 'uploading') {
                    let prevData = info.file.response.data;  //返回值
                    me.setState({
                        prevData:prevData
                    });
                    me.props.handleEmail(prevData);
                }
                if (info.file.status === 'done') {

                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} file upload failed.`);
                    // console.log("123:",message)
                }
            },
        };

        return(
            <div onChange={this.props.handleEmail}>
                <Upload {...props} className="inline">
                    <Button className="daoru"><Icon type="upload" />导入</Button>
                </Upload>
            </div>
        )
    }
}

