import React, { Component } from 'react'
import { Menu, Layout, Icon } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import { pathToNameMapping as map } from './pathToNameMapping'
const { Sider } = Layout
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
@withRouter
class MenuList extends Component{
    constructor(props) {
        super(props)
    }
    render() {
        const pathname = this.props.location.pathname
        const openKeys = [('/' + pathname.split('/')[1])]
        return (
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
            >
                <div className="logo"><img src={require("../../img/logo.png")} alt="logo" id="logo" /></div>
                <Menu theme="dark"
                      style={{width: 200,marginTop:60}}
                      defaultOpenKeys={ openKeys }
                    selectedKeys={[pathname]}
                      mode="inline">
                    <Menu.Item key="/Home">
                        <Link to="/Home">
                            <Icon type="home" />
                            <span>首页</span>
                        </Link>
                    </Menu.Item>
                    <SubMenu key="/employees" title={<span><Icon type="team"/><span>员工</span></span>}>
                        <Menu.Item key="/employees/employeesInfo"><Link to="/employees/employeesInfo">{map['/employees/employeesInfo']}</Link></Menu.Item>
                        {/*<Menu.Item key="3"><Link to="/PersonalDevices">个人设备</Link></Menu.Item>*/}
                        {/*<Menu.Item key="4"><Link to="/Security">社保信息</Link></Menu.Item>*/}
                        {/*<Menu.Item key="5"><Link to="/Standby">备用</Link></Menu.Item>*/}
                    </SubMenu>
                    <SubMenu key="/departmentManage" title={<span><Icon type="usergroup-add" /><span>部门管理</span></span>}>
                        <Menu.Item key="/departmentManage/DepartmentList"><Link to="/departmentManage/DepartmentList">{map['/departmentManage/DepartmentList']}</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu key="/attendance" title={<span><Icon type="dashboard"/><span>考勤</span></span>}>
                        <MenuItemGroup title="导入">
                            <Menu.Item key="/attendance/Affair"><Link to="/attendance/Affair">{map['/attendance/Affair']}</Link></Menu.Item>
                            <Menu.Item key="/attendance/dingdingAttendance"><Link to="/attendance/dingdingAttendance">{map['/attendance/dingdingAttendance']}</Link></Menu.Item>
                        </MenuItemGroup>
                        <MenuItemGroup title="考勤明细">
                            <Menu.Item key="/attendance/DetailsLetter"><Link to="/attendance/DetailsLetter">{map['/attendance/DetailsLetter']}</Link></Menu.Item>
                            <Menu.Item key="/attendance/DetailsDingding"><Link to="/attendance/DetailsDingding">{map['/attendance/DetailsDingding']}</Link></Menu.Item>
                            <Menu.Item key="/attendance/OvertimeRecords"><Link to="/attendance/OvertimeRecords">{map['/attendance/OvertimeRecords']}</Link></Menu.Item>
                            <Menu.Item key="/attendance/PaidLeave"><Link to="/attendance/PaidLeave">{map['/attendance/PaidLeave']}</Link></Menu.Item>
                        </MenuItemGroup>
                        <MenuItemGroup title="报表">
                            <Menu.Item key="/attendance/AccountingAttendance"><Link to="/attendance/AccountingAttendance">{map['/attendance/AccountingAttendance']}</Link></Menu.Item>
                            <Menu.Item key="/attendance/AbnormalAttendance"><Link to="/attendance/AbnormalAttendance">{map['/attendance/AbnormalAttendance']}</Link></Menu.Item>
                        </MenuItemGroup>
                        {/*<Menu.Item key="7"><Link to="/Rest">休息信息</Link></Menu.Item>*/}
                        {/*<Menu.Item key="8"><Link to="/workOvertime">加班信息</Link></Menu.Item>*/}
                    </SubMenu>
                    <SubMenu key="/organization" title={<span><Icon type="fork"/><span>组织</span></span>}>
                        {/*<Menu.Item key="10"><Link to="/Department">部门（部门架构图）</Link></Menu.Item>*/}
                    </SubMenu>
                    <SubMenu key="/order" title={<span><Icon type="pay-circle"/><span>订单（PO）</span></span>}>
                        <Menu.Item key="/order/Po"><Link to="/order/Po">{map['/order/Po']}</Link></Menu.Item>
                        <Menu.Item key="/order/UpDown"><Link to="/order/UpDown">{map['/order/UpDown']}</Link></Menu.Item>
                        <Menu.Item key="/order/Price"><Link to="/order/Price">{map['/order/Price']}</Link></Menu.Item>
                        <Menu.Item key="/order/JobManagement"><Link to="/order/JobManagement">{map['/order/JobManagement']}</Link></Menu.Item>
                        <Menu.Item key="/order/Quotation"><Link to="/order/Quotation">{map['/order/Quotation']}</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu key="/equipment" title={<span><Icon type="tool"/><span>设备</span></span>}>
                        <Menu.Item key="/equipment/"><Link to="/">备用</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu key="/others" title={<span><Icon type="appstore" /><span>其他</span></span>}>
                        <Menu.Item key="/others/GroupManagement"><Link to="/others/GroupManagement">{map['/others/GroupManagement']}</Link></Menu.Item>
                        <Menu.Item key="/others/GroupMessage"><Link to="/others/GroupMessage">{map['/others/GroupMessage']}</Link></Menu.Item>
                        <Menu.Item key="/others/MessageTemplate"><Link to="/others/MessageTemplate">{map['/others/MessageTemplate']}</Link></Menu.Item>
                        {/*<Menu.Item key="18"><Link to="/MyCard">备用</Link></Menu.Item>*/}
                        {/*<Menu.Item key="19"><Link to="/MySetting">备用</Link></Menu.Item>*/}
                    </SubMenu>
                    <SubMenu key="/system" title={<span><Icon type="setting" /><span>系统管理</span></span>}>
                        <Menu.Item key="/system/TimerManagement"><Link to="/system/TimerManagement">{map['/system/TimerManagement']}</Link></Menu.Item>
                    </SubMenu>
                    <SubMenu key="/positionAndLevelAndRate" title={<span><Icon type="share-alt" /><span>岗位层级单价</span></span>}>
                        <Menu.Item key="/positionAndLevelAndRate/positionAndLevel"><Link to="/positionAndLevelAndRate/positionAndLevel">{map['/positionAndLevelAndRate/positionAndLevel']}</Link></Menu.Item>
                        <Menu.Item key="/positionAndLevelAndRate/addPositionAndLevel"><Link to="/positionAndLevelAndRate/addPositionAndLevel">{map['/positionAndLevelAndRate/addPositionAndLevel']}</Link></Menu.Item>
                        <Menu.Item key="/positionAndLevelAndRate/positionAndLevelManage"><Link to="/positionAndLevelAndRate/positionAndLevelManage">{map['/positionAndLevelAndRate/positionAndLevelManage']}</Link></Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
        )
    }
}
export default MenuList
