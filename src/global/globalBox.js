import React from 'react';
import {Layout, Menu, Icon} from 'antd';
import {Route, Link, Switch,HashRouter} from 'react-router-dom';
//子组件
import Employee from "../employee/employee";
import Person from "../employee/detail/person";
import Home from "../home/home";
import PersonalDevices from "../employee/Personal-devices";
import Security from "../employee/security";
import Standby from "../employee/standby";
import Affair from "../attendence/affair/affair";
import dingdingAttendance from "../attendence/dingdingAttendance/dingdingAttendance"
import DetailsLetter from "../attendence/DetailsLetter/DetailsLetter";
import DetailsDingding from "../attendence/DetailsDingding/DetailsDingding";
import OvertimeRecords from "../attendence/OvertimeRecords/OvertimeRecords";
import PaidLeave from "../attendence/PaidLeave/PaidLeave";
import AbnormalAttendance from "../attendence/abnormal/abnormalAttendance";      //外出
import AccountingAttendance from "../attendence/account/AccountingAttendance"
import AttendanceDetail from "../attendence/account/AttendanceDetail"
import Department from "../organization/department"     /*组织-部门*/
import Po from "../the-order/po/po";
import UpDown from "../the-order/updown";
import Price from "../the-order/price";
import JobManagement from "../the-order/JobManagement/JobManagement";          /*订单-岗位层级管理*/
import Quotation from "../the-order/Quotation/Quotation";          /*订单-报价*/
import GroupManagement from "../other/GroupManagement";
import GroupDetails from "../other/GroupManagement/GroupDetails";      /*其他-群管理-群详情*/
import GroupMessage from "../other/groupMsg";
import MessageTemplate from "../other/messageTemplate";
import MyCard from "../other/fetch";
import MySetting from "../other/setting";
import BreadcrumbCustom from "./breadcrumbCustom";
import TimerManagement from "../SystemSettings/TimerManagement";
import editGroupType from "../other/editGroupType"
import MenuList from './menulist'
import DepartmentList from '../department/departmentList'
import positionAndLevel from "../position&level&rate/positionAndLevel"
import XiaTest from 'test/linkage'
import addPositionAndLevel from "../position&level&rate/addPositionAndLevel"
import positionAndLevelManage from '../position&level&rate/positionAndLevelManage'
const {Header, Content, Footer, Sider} = Layout;

const SubMenu = Menu.SubMenu;
/**
 *  此类是全局布局类 主要包括左侧导航 右上面包屑,右下内容区域
 */

class GlobalBox extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <HashRouter>
                    <Layout>
                        <MenuList/>
                        <Layout   style={{ height: '100vh', overflow: 'auto' }}>
                            <Header style={{background: '#fff', padding: 0,margin:"24px 20px 0"}}>
                                <BreadcrumbCustom/>
                            </Header>
                            <Content style={{margin: '16px 20px 0', background: '#fff', padding: 24}} >
                                {/*<div style={{padding: 24, background: '#fff'}}>*/}
                                    {/*路由器*/}
                                    <Switch>
                                        <Route exact path="/home" component={Home} />  /*首页（默认）*/
                                        <Route path="/employees/employeesInfo" component={Employee}/>   /*员工-员工基本信息*/
                                        <Route path="/person/:empId/:edit" component={Person}/>    /*员工-个人详细信息*/
                                        <Route path="/PersonalDevices" component={PersonalDevices}/>  /*员工-个人设备*/
                                        /*<Route path="/security" component={Security}/>  /*员工-社保信息*/*/
                                        /*<Route path="/standby" component={Standby}/>   /*员工-备用*/*/
                                        <Route path="/attendance/Affair" component={Affair}/>   /*考勤-事假考勤*/
                                        <Route path="/attendance/dingdingAttendance" component={dingdingAttendance}/>   /*考勤-钉钉考勤*/
                                        <Route path="/attendance/DetailsLetter" component={DetailsLetter}/>  /*考勤-微信详情*/
                                        <Route path="/attendance/DetailsDingding" component={DetailsDingding}/>  /*考勤-钉钉详情*/
                                        <Route path="/attendance/OvertimeRecords" component={OvertimeRecords}/>  /*考勤-加班记录*/
                                        <Route path="/attendance/PaidLeave" component={PaidLeave}/>  /*考勤-请假调休*/
                                        <Route path="/attendance/abnormalAttendance" component={AbnormalAttendance}/>   /*异常考勤*/
                                        /*<Route path="/Department" component={Department}/>   /*组织-部门*/*/
                                        <Route path="/order/Po" component={Po}/>     /*订单-PO信息*/
                                        <Route path="/order/UpDown" component={UpDown}/>     /*订单-上下项*/
                                        <Route path="/order/Price" component={Price}/>     /*订单-单价*/
                                        <Route path="/order/JobManagement" component={JobManagement}/>       /*订单-岗位层级管理*/
                                        <Route path="/order/Quotation" component={Quotation}/>      /*订单-报价*/
                                        <Route path="/others/GroupManagement" component={GroupManagement}/>    /*其他-群管理*/
                                        <Route path="/groupDetails/:id/:groupType" component={GroupDetails}/>    /*其他-群管理-群详情*/
                                        <Route path="/others/GroupMessage" component={GroupMessage} />     /*其他-群消息发送*/
                                        <Route path="/GroupClassModify" component={editGroupType} /> /*群分类编辑*/
                                        <Route path="/others/messageTemplate" component={MessageTemplate}/>/*异常消息模板*/
                                        <Route path="/attendance/AccountingAttendance" component={AccountingAttendance}/> /*核算考勤*/
                                        <Route path="/attendanceDetail/:empId" component={AttendanceDetail}/>         /*考勤明细*/
                                        <Route path="/departmentManage/DepartmentList" component={DepartmentList}/>         /*部门列表*/
                                        /*<Route path="/myCard" component={MyCard} />    /*其他-备用*/*/
                                        /*<Route path="/mySetting" component={MySetting} />   /*其他-备用*/*/
                                        <Route path="/system/TimerManagement" component={TimerManagement}/>
                                        <Route path="/positionAndLevelAndRate/positionAndLevel" component={positionAndLevel}/>
                                        <Route path="/positionAndLevelAndRate/addPositionAndLevel" component={addPositionAndLevel}/>
                                        <Route path="/positionAndLevelAndRate/positionAndLevelManage" component={positionAndLevelManage}/>
                                        <Route path="/test/xia" component={XiaTest}></Route>
                                    </Switch>
                                {/*</div>*/}
                            </Content>
                            <Footer style={{textAlign: 'center'}}>
                                佰钧成©2018 Created by Ant UED
                            </Footer>
                        </Layout>
                    </Layout>
            </HashRouter>
        );
    }
}
export default GlobalBox
