
const pathToNameMapping = {
    '/Home': '首页',
    '/employees/employeesInfo': '员工基本信息',
    '/attendance/Affair': '微信考勤',
    '/attendance/dingdingAttendance':'钉钉考勤',
    '/attendance/AccountingAttendance': '核算考勤',
    '/attendance/AbnormalAttendance': '异常考勤',
    '/attendance/DetailsLetter':'微信详情',
    '/attendance/DetailsDingding':'钉钉详情',
    '/attendance/OvertimeRecords':'加班记录',
    '/attendance/PaidLeave':'请假调休',
    '/order/Po': 'PO信息',
    '/order/UpDown': '上下项',
    '/order/Price': '单价',
    '/order/JobManagement':'岗位层级管理',
    '/order/Quotation':'报价',
    '/others/GroupManagement': '群管理',
    '/others/GroupMessage': '群消息发送',
    '/others/MessageTemplate': '异常消息模板',
    '/system/TimerManagement': '定时器管理',
    '/departmentManage/DepartmentList': '部门列表',
    '/positionAndLevelAndRate/positionAndLevel': '岗位层级',
    '/positionAndLevelAndRate/addPositionAndLevel': '新增岗位层级',
    '/positionAndLevelAndRate/positionAndLevelManage': '岗位层级管理'
}
// 路由menu中配置的页面
const skipPageMapping = {
    '/attendanceDetail': '详细考勤信息',
    '/GroupClassModify': '群分类管理'
}
// 跳转的页面，不在路由配置中
export { pathToNameMapping, skipPageMapping }
