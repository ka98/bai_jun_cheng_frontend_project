import React, { Component } from 'react';
import { Breadcrumb, Switch, Icon } from 'antd';
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { pathToNameMapping, skipPageMapping } from './pathToNameMapping'
//
// const breadcrumbNameMap = {
//     '/employeesInfo': '人员基本信息',
//     '/person':'人员基本信息/个人详细信息',
//     '/abnormalAttendance':'异常考勤',
//     '/GroupMessage':'群消息发送',
//     '/GroupManagement':'群管理',
//     '/Affair':'事假考勤',
//     '/AccountingAttendance':'核算考勤',
//     '/attendanceDetail': '详细考勤信息',
//     '/GroupClassModify': '群分类管理'
// };
class BreadcrumbCustom extends Component {
    //利用PropTypes記住所跳轉每個頁面的位置
    static contextTypes = {
        router: PropTypes.object
    }
    constructor(props, context) {
        super(props, context);
        this.state = {
            // pathSnippets: null,
            // extraBreadcrumbItems: null
            title: ''
        }
    }
    getPath() {
        //對路徑進行切分，存放到this.state.pathSnippets中
        const pathname = this.context.router.history.location.pathname
        //將切分的路徑讀出來，形成面包屑，存放到this.state.extraBreadcrumbItems
        // this.state.extraBreadcrumbItems = arr.map((_, index) => {
        //     const url = `/${arr.slice(0, index + 1).join('/')}`;
        //     console.log('url', url, breadcrumbNameMap[url])
        //     return (
        //         <Breadcrumb.Item key={url}>
        //             <Link to={url}>
        //                 <h2 style={{marginLeft:'20px'}}>{breadcrumbNameMap[url]}</h2>
        //             </Link>
        //         </Breadcrumb.Item>
        //     );
        // });
        // console.log(this.state.extraBreadcrumbItems)
        this.setState({
            title: pathToNameMapping[pathname] ?  pathToNameMapping[pathname] : skipPageMapping[`/${pathname.split('/')[1]}`]
        })
        // 先判断是不是menu里注册的页面如果不是就是跳转过去的
    }


    componentDidMount() {
        //首次加載的時候調用，形成面包屑
        this.getPath();
    }
    componentWillReceiveProps(){
        //任何子頁面發生改變，均可調用，完成路徑切分以及形成面包屑
        this.getPath();
    }
    render() {
        return (

            <span>
                {/*<Breadcrumb style={{ margin: '12px 0' }}>*/}
                <h2 style={{marginLeft:'20px'}}>{this.state.title}</h2>
                {/*</Breadcrumb>*/}
            </span>
        )
    }
}
export default BreadcrumbCustom;